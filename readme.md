# zrapr

zrapr is a written-from-scratch* experimental 2D platformer game made with C++20 and OpenGL 3.x designed for player-made levels and a high amount of flexibility.

<small>*Some third-party libraries are used. Details are at the bottom of this document.</small>

A screenshot:

![Screenshot](docs/screenshot.png)

## Making Levels

Levels are made with [Tiled](https://www.mapeditor.org/). Start by creating a project file by clicking `Game > generate tiled-project file` from the main menu bar within zrapr. This creates `zrapr.tiled-project` in the same directory as the executable. Opening this in Tiled will make all the assets and object types available for use. Player levels can either go in the `res/` directory next to the executable, or the "pref dir," opened with `Game > open pref dir` from zrapr's main menu bar. (The path of the pref dir varies per system.) Levels cannot reference assets between or outside of the two locations. ALL of a level's assets must either be in `res/` or the pref dir.

## Building

### CMake Options

Option | Description
--- | ---
`-DCMAKE_BUILD_TYPE=Debug` | Produce a debug build (default)
`-DCMAKE_BUILD_TYPE=Release` | Produce an optimized release build
`-DCOPY_RESOURCES=copy` | Method of copying resources into the build directory. Values are: "copy" or "symlink"
`-DUSE_CCACHE=1` | Use ccache to speed up repeat builds, if available (default)
`-DMINGW_STATIC_RUNTIME=1` | Static link MinGW runtime libs for better portability (default)
`-DFORCE_LOCAL_LIBS=1` | Force use of local, bundled libs. Do not attempt to locate any system libraries (except SDL2)
`-DUSE_PKG_CONFIG=1` | Use pkg-config to resolve dependencies, if available (default)
`-DUSE_PKG_CONFIG_STATIC=1` | Use static libraries from pkg-config (deafult on windows)
`-DUSE_CMAKE_FIND=1` | Use CMake's find_package to resolve dependencies, if available (default)
`-DUSE_CMAKE_SDL2_STATIC=1` | Use SDL2::SDL2-static from SDL2's CMake package (deafult on windows)

### Building on GNU/Linux

Install packages:

```sh
# arch linux
pacman -S cmake sdl2
# debian/ubuntu
apt install build-essential cmake libsdl2-dev
# gentoo
emerge cmake libsdl2
```

Configure for build:

```sh
# add any additional CMake options to the end of this command line
cmake -B build
```

Build:

```sh
cmake --build build
```

### Building with MSYS2/MinGW on Microsoft Windows

Use anything but the MSYS shell. MinGW, Clang, and UCRT shells all work.

Install packages:

```sh
pacman -S ${MINGW_PACKAGE_PREFIX}-{toolchain,cmake,SDL2}
```

Configure for build:

```sh
# add any additional CMake options to the end of this command line
cmake -B build -G"Unix Makefiles"
```

If MinGW CMake whines about missing libraries, try running this:

```sh
cp $MINGW_PREFIX/bin/libuv*.dll .
cp $MINGW_PREFIX/bin/librhash.dll .
cp $MINGW_PREFIX/bin/libjsoncpp*.dll .
```

Build:

```sh
cmake --build build
```

### Building on Mac OS X

Functionality on Mac OS X is not guaranteed.

Install packages with Homebrew:

```sh
brew install cmake pkg-config sdl2
```

Configure for build:

```sh
# add any additional CMake options to the end of this command line
cmake -B build
```

Build:

```sh
cmake --build build
```

### Cross-building with MinGW on GNU/Linux

Installing MinGW and its packages varies per distro and most distros don't even have it on their main repos. Installing MinGW on GNU/Linux is out of the scope of this readme. More info: https://www.mingw-w64.org/downloads/

Configure for build:

```sh
# add any additional CMake options to the end of these command lines
# targeting mingw 32-bit
cmake -B buildmingw -DCMAKE_TOOLCHAIN_FILE=/path/to/your/toolchain-i686-w64-mingw32.cmake -DPKG_CONFIG_EXECUTABLE=/usr/bin/x86_64-w64-mingw32-pkg-config
# targeting mingw 64-bit
cmake -B buildmingw -DCMAKE_TOOLCHAIN_FILE=/path/to/your/toolchain-x86_64-w64-mingw32.cmake -DPKG_CONFIG_EXECUTABLE=/usr/bin/x86_64-w64-mingw32-pkg-config
```

Build:

```sh
cmake --build buildmingw
```

## Attribution

### Assets

* Cave Story sprites from Lethrys: https://github.com/Lethrys/Cave-Story-Lethrys-Upscale <small>("open community use")</small>
* Penguin sprites from GrafxKid: https://opengameart.org/content/super-random-sprites <small>(CC0-1.0)</small>
* Gem sprites from Clint Bellanger: https://opengameart.org/content/gem-icons <small>(CC-BY-SA-3.0)</small>
* Folder icons from Tango Desktop Project: http://tango.freedesktop.org <small>(Public Domain)</small>

All of the rest of the assets in the demo levels are made by me.

### Libraries

* Dear ImGui https://github.com/ocornut/imgui <small>(MIT)</small>
* EnTT https://github.com/skypjack/entt <small>(MIT)</small>
* glad https://github.com/Dav1dde/glad
* inih https://github.com/benhoyt/inih <small>(BSD 3-clause)</small>
* PhysicsFS https://icculus.org/physfs/ <small>(zlib)</small>
* SDL2 https://www.libsdl.org/ <small>(zlib)</small>
* stb_image https://github.com/nothings/stb <small>(Unlicense)</small>
* TinyXML2 https://github.com/leethomason/tinyxml2 <small>(zlib)</small>
* zlib https://zlib.net/ <small>(zlib)</small>
* Zstandard https://facebook.github.io/zstd/ <small>(BSD 3-clause)</small>
