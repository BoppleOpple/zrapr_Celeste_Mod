<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="demo_tiles" tilewidth="32" tileheight="32" tilecount="128" columns="8">
 <image source="demo_tiles.png" width="256" height="512"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="-0.125" y="0">
    <polyline points="0,0 32,0"/>
   </object>
   <object id="2" type="static_collision/leftwall" x="0" y="0">
    <polyline points="0,0 0,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="26.5" y="41">
    <polyline points="-26.5,-41 5.5,-41"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="33.75" y="30">
    <polyline points="-33.75,-30 -1.75,-30"/>
   </object>
   <object id="2" type="static_collision/rightwall" x="32" y="0">
    <polyline points="0,0 0,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="32">
    <polyline points="0,0 32,-32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/leftwall" x="0" y="0">
    <polyline points="0,0 0,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/rightwall" x="32" y="0">
    <polyline points="0,0 0,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/ceiling" x="0" y="0">
    <polyline points="0,0 32,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/ceiling" x="0" y="32">
    <polyline points="0,0 32,-32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/leftwall" x="0" y="0">
    <polyline points="0,0 0,32"/>
   </object>
   <object id="2" type="static_collision/ceiling" x="0" y="32">
    <polyline points="0,0 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/ceiling" x="0" y="32">
    <polyline points="0,0 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/rightwall" x="32" y="0">
    <polyline points="0,0 0,32"/>
   </object>
   <object id="2" type="static_collision/ceiling" x="0" y="32">
    <polyline points="0,0 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="32">
    <polyline points="0,0 32,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="16">
    <polyline points="0,0 32,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="22">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="23">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="16">
    <polyline points="0,0 32,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="24" type="entity/coin">
  <objectgroup draworder="index" id="2">
   <object id="1" type="body_collision/non_solid" x="8" y="8" width="16" height="16"/>
  </objectgroup>
  <animation>
   <frame tileid="24" duration="150"/>
   <frame tileid="25" duration="150"/>
   <frame tileid="26" duration="150"/>
   <frame tileid="27" duration="150"/>
  </animation>
 </tile>
 <tile id="28">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/ceiling" x="0" y="0">
    <polyline points="0,0 32,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="29">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/ceiling" x="0" y="16">
    <polyline points="0,0 32,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="30">
  <objectgroup draworder="index" id="2">
   <object id="2" type="static_collision/ceiling" x="0" y="16">
    <polyline points="0,16 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="31">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/ceiling" x="0" y="0">
    <polyline points="0,16 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="56" type="entity/warp">
  <objectgroup draworder="index" id="2">
   <object id="1" type="body_collision/non_solid" x="14" y="28" width="4" height="4"/>
  </objectgroup>
  <animation>
   <frame tileid="56" duration="150"/>
   <frame tileid="57" duration="150"/>
   <frame tileid="58" duration="150"/>
   <frame tileid="59" duration="150"/>
  </animation>
 </tile>
 <tile id="64">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="65">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="66">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="70">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="32">
    <polyline points="0,0 32,-32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="71">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="84">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="32">
    <polyline points="0,0 32,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="85">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="16">
    <polyline points="0,0 32,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="86">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="0">
    <polyline points="0,0 32,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="87">
  <objectgroup draworder="index" id="2">
   <object id="1" type="static_collision/floor" x="0" y="16">
    <polyline points="0,0 32,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="96" type="entity/key">
  <properties>
   <property name="key_name" value="sapphire"/>
  </properties>
  <objectgroup draworder="index" id="3">
   <object id="2" type="body_collision/non_solid" x="8" y="8" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="97" type="entity/key">
  <properties>
   <property name="key_name" value="emerald"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" type="body_collision/non_solid" x="8" y="8" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="98" type="entity/key">
  <properties>
   <property name="key_name" value="ruby"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" type="body_collision/non_solid" x="8" y="8" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="99" type="entity/key">
  <properties>
   <property name="key_name" value="diamond"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" type="body_collision/non_solid" x="8" y="8" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="100" type="entity/warp">
  <properties>
   <property name="warp_unlock_tile_id" type="int" value="56"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" type="body_collision/non_solid" x="14" y="28" width="4" height="4"/>
  </objectgroup>
  <animation>
   <frame tileid="100" duration="150"/>
   <frame tileid="101" duration="150"/>
   <frame tileid="102" duration="150"/>
   <frame tileid="103" duration="150"/>
  </animation>
 </tile>
</tileset>
