// zrapr.cc

// sdl is epic

#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

#include <entt/entt.hpp>
#include <imgui/imgui_impl_opengl3.h>
#include <imgui/imgui_impl_sdl.h>
#include <imgui/imgui.h>
#include <SDL.h>

#include "comp.hh"
#include "configfile.hh"
#include "context.hh"
#include "entities.hh"
#include "fs.hh"
#include "geom.hh"
#include "gfx_tile.hh"
#include "gfx.hh"
#include "input.hh"
#include "intersects.hh"
#include "map.hh"
#include "playerchar.hh"
#include "ui.hh"
#include "util/input_constants.hh"
#include "util/sleep.hh"
#include "window.hh"

ClientContext g_client_context;
GameContext g_game_context;

static inline int clamp(int x, int min, int max) { return x > max ? max : (x < min ? min : x); }
static inline float minf(float a, float b) { return a < b ? a : b; }
static inline float maxf(float a, float b) { return a > b ? a : b; }
static inline float clampf(float x, float min, float max) { return x > max ? max : (x < min ? min : x); }
static inline float lerpf(float v0, float v1, float t) { return t * (v1 - v0) + v0; }

static inline float detract(float value, float target, float period) {
	if (value >= target) {
		return value - period <= target ? target : value - period;
	} else {
		return value + period >= target ? target : value + period;
	}
}

void init_contexts() {
	// window settings
	g_client_context.window_width = 1280;
	g_client_context.window_height = 720;
	g_client_context.window_fullscreen = false;
	g_client_context.window_maximized = false;
	g_client_context.window_vsync = false;
	g_client_context.view_x = 0.0f;
	g_client_context.view_y = 0.0f;
	g_client_context.view_scale = 2;

	// input settings
#ifndef __INTELLISENSE__
	g_client_context.input_control.frame_advance = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_F                      }
	} };
	g_client_context.input_control.pause = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_G                      }
	} };
	g_client_context.input_control.reload_level = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_T                      }
	} };
	g_client_context.input_control.zoom_in = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_EQUALS                 }
	} };
	g_client_context.input_control.zoom_out = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_MINUS                  }
	} };

	InputProfile &profile0 = g_client_context.input_control.profiles.emplace_back();
	profile0.left = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_LEFT                   },
		{ .type = InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE, .axis     = SDL_CONTROLLER_AXIS_LEFTX           },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_DPAD_LEFT     }
	} };
	profile0.right = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_RIGHT                  },
		{ .type = InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE, .axis     = SDL_CONTROLLER_AXIS_LEFTX           },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_DPAD_RIGHT    }
	} };
	profile0.up = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_UP                     },
		{ .type = InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE, .axis     = SDL_CONTROLLER_AXIS_LEFTY           },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_DPAD_UP       }
	} };
	profile0.down = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_DOWN                   },
		{ .type = InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE, .axis     = SDL_CONTROLLER_AXIS_LEFTY           },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_DPAD_DOWN     }
	} };
	profile0.jump = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_X                      },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_A             }
	} };
	profile0.aircontrol = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_C                      },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_X             }
	} };
	profile0.freemove = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_V                      },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_Y             }
	} };
	profile0.respawn = { .entries = {
		{ .type = InputSpecEntry::SCANCODE,                      .scancode = SDL_SCANCODE_R                      },
		{ .type = InputSpecEntry::GAME_CONTROLLER_BUTTON,        .button   = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER }
	} };
#endif

	// ui settings
	g_client_context.show_imgui_demo = false;
	g_client_context.show_ecs_view = false;
	g_client_context.show_collision_view_control = false;
	g_client_context.show_input_control = false;
	g_client_context.show_level_select = true;
	g_client_context.caption_entity_ids = true;

	// collision view control
	g_client_context.collision_view_control.show_static_floor           = false;
	g_client_context.collision_view_control.show_static_wall_left       = false;
	g_client_context.collision_view_control.show_static_wall_right      = false;
	g_client_context.collision_view_control.show_static_ceiling         = false;
	g_client_context.collision_view_control.show_body_main              = false;
	g_client_context.collision_view_control.show_body_floor_snap_up     = false;
	g_client_context.collision_view_control.show_body_floor_snap_down   = false;
	g_client_context.collision_view_control.show_body_wall_snap_left    = false;
	g_client_context.collision_view_control.show_body_wall_snap_right   = false;
	g_client_context.collision_view_control.show_body_ceiling_snap_down = false;
	g_client_context.collision_view_control.setting_extra_body_collision = false;

	// collision view colors
	g_client_context.collision_view_colors.static_floor              = { 0x6f, 0x5f, 0xff, 0xff };
	g_client_context.collision_view_colors.static_wall_left          = { 0xaf, 0xff, 0x5f, 0xff };
	g_client_context.collision_view_colors.static_wall_right         = { 0x5f, 0xff, 0xcf, 0xff };
	g_client_context.collision_view_colors.static_ceiling            = { 0xff, 0x5f, 0x6f, 0xff };
	g_client_context.collision_view_colors.body_main                 = { 0xdf, 0xdf, 0xdf, 0xff };
	g_client_context.collision_view_colors.body_main_bg              = { 0xdf, 0xdf, 0xdf, 0x13 };
	g_client_context.collision_view_colors.body_floor_snap_up        = { 0xff, 0x5f, 0xff, 0xff };
	g_client_context.collision_view_colors.body_floor_snap_up_bg     = { 0xff, 0x5f, 0xff, 0x2f };
	g_client_context.collision_view_colors.body_floor_snap_down      = { 0x5f, 0xff, 0x5f, 0xff };
	g_client_context.collision_view_colors.body_floor_snap_down_bg   = { 0x5f, 0xff, 0x5f, 0x2f };
	g_client_context.collision_view_colors.body_wall_snap_left       = { 0x7f, 0xaf, 0xff, 0xff };
	g_client_context.collision_view_colors.body_wall_snap_left_bg    = { 0x7f, 0xaf, 0xff, 0x2f };
	g_client_context.collision_view_colors.body_wall_snap_right      = { 0xff, 0xbf, 0x7f, 0xff };
	g_client_context.collision_view_colors.body_wall_snap_right_bg   = { 0xff, 0xbf, 0x7f, 0x2f };
	g_client_context.collision_view_colors.body_ceiling_snap_down    = { 0xff, 0xff, 0x8f, 0xff };
	g_client_context.collision_view_colors.body_ceiling_snap_down_bg = { 0xff, 0xff, 0x8f, 0x2f };

	// logs
	g_client_context.log_max_entries = 100;
	g_client_context.log_to_stdout = true;

	// game settings
	g_game_context.target_frames_per_second = 60.0f;
	g_game_context.time_factor_multiplier = 1.0f;
	g_game_context.coin_count = 0;
	g_game_context.is_game_frame = true;
	g_game_context.paused = false;
	g_game_context.level_path = "demo_level/demo_level.tmx";
	g_game_context.playerchar_path = "playerchar/penguin_grafxkid.png";
	g_game_context.playerchar_search_paths.emplace_back("playerchar");
	g_game_context.user_speedrun_mode = false;
	g_game_context.speedrun_mode = false;
	g_game_context.speedrun_finished = false;
	g_game_context.preserve_position_on_level_load = false;
	g_game_context.level_is_loaded = false;
	g_game_context.has_preserved_player_position = false;
}

void sys_player_input(entt::registry &reg) {
	for (const entt::entity e : reg.view<CompPlayer>()) {
		CompPlayer &player = reg.get<CompPlayer>(e);
		if (player.input_index < 0 || (unsigned int)player.input_index >= g_client_context.input_control.profiles.size()) {
			continue;
		}
		const InputProfile &profile = g_client_context.input_control.profiles[player.input_index];
		player.input_left             = game_input_is_held(profile.left);
		player.input_right            = game_input_is_held(profile.right);
		player.input_up               = game_input_is_held(profile.up);
		player.input_down             = game_input_is_held(profile.down);
		player.input_down_ff          = game_input_is_pressed(profile.down);
		player.input_jump             = game_input_is_held(profile.jump);
		player.input_jump_ff          = game_input_is_pressed(profile.jump);
		player.input_dash             = game_input_is_held(profile.dash);
		player.input_dash_ff          = game_input_is_pressed(profile.dash);
		player.input_aircontrol       = game_input_is_held(profile.aircontrol);
		player.input_toggle_free_move = game_input_is_pressed(profile.freemove);
		player.input_respawn          = game_input_is_pressed(profile.respawn);
	}
}

void speedrun_invalidate() {
	if (!g_game_context.speedrun_finished) {
		g_game_context.speedrun_time += 99999.0;
	}
}

entt::exclude_t no_collision_exclude = entt::exclude<CompFreeMove, CompTransit>;
entt::exclude_t no_physics_exclude = entt::exclude<CompFreeMove, CompTransit, CompDash>;

// product of syntactical guesswork
template<typename... A, typename... B>
inline constexpr const entt::exclude_t<A..., B...> excludes(entt::exclude_t<A...>, entt::exclude_t<B...>) {
	return entt::exclude_t<A..., B...>{};
}
template<typename... A, typename... B, typename... C>
inline constexpr const entt::exclude_t<A..., B..., C...> excludes(entt::exclude_t<A...>, entt::exclude_t<B...>, entt::exclude_t<C...>) {
	return entt::exclude_t<A..., B..., C...>{};
}

// physics constants
#define GRAVITY 0.3125f
#define MAX_VERTICAL_SPEED 5.99609375f
#define MAX_LATERAL_SPEED 3.3203125f
#define GROUND_FRICTION 0.19921875f
#define AIR_FRICTION -0.0f
#define JUMP_FORCE -5.0f
#define JUMP_GRAVITY_MULTIPLIER 0.4f
#define GROUND_ACCEL 0.33203125f
#define AIR_ACCEL 0.125f
#define HEAD_BONK_STRENGTH 0.0f
#define FREE_MOVE_SPEED 6.5f
#define FREE_MOVE_SPEED_FAST 20.0f
#define FREE_MOVE_SPEED_SLOW 1.0f
#define TRANSIT_SPEED 15.0f
#define GRACE_JUMP_TIME 8.0f
#define GRACE_BHOP_TIME 5.0f
#define DASH_DISTANCE 80.0f
#define DASH_TIME 10.0f

void sys_physics(entt::registry &reg) {
	const float tf = g_game_context.time_factor;

	const auto entities = reg.view<CompPosition, CompPhysics>(no_physics_exclude);
	for (const entt::entity e : entities) {
		CompPosition &pos = reg.get<CompPosition>(e);
		CompPhysics &physics = reg.get<CompPhysics>(e);

		// lateral friction
		physics.dx = detract(physics.dx, 0.0f, ((physics.is_on_ground && physics.was_on_ground) ? GROUND_FRICTION : AIR_FRICTION) * tf);

		// gravity
		physics.dy += GRAVITY * physics.gravity_multiplier * tf;

		// speed caps
		physics.dx = clampf(physics.dx, -MAX_LATERAL_SPEED, MAX_LATERAL_SPEED);
		physics.dy = clampf(physics.dy, -MAX_VERTICAL_SPEED, MAX_VERTICAL_SPEED);

		// speed displacement
		pos.x += physics.dx * tf;
		pos.y += physics.dy * tf;
	}
}

void sub_jump(CompPhysics &physics, CompPlayer &player) {
	physics.dy = JUMP_FORCE;
	player.grace_jump_timer = 0.0f;
	player.grace_bhop_timer = 0.0f;
	physics.is_on_ground = false;
	player.looking_back = false;
}

void sys_player(entt::registry &reg) {
	const float tf = g_game_context.time_factor;

	const auto entities = reg.view<CompPhysics, CompPlayer>(entt::exclude<CompTransit>);
	for (const entt::entity e : entities) {
		CompPosition &pos = reg.get<CompPosition>(e);
		CompPhysics &physics = reg.get<CompPhysics>(e);
		CompPlayer &player = reg.get<CompPlayer>(e);

		bool is_free_move = reg.all_of<CompFreeMove>(e);
		bool is_dashing   = reg.all_of<  CompDash  >(e);

		if (player.input_toggle_free_move) {
			if (is_free_move) {
				reg.remove<CompFreeMove>(e);
				physics.dx = 0.0f;
				physics.dy = 0.0f;
			} else {
				if (!reg.all_of<CompFreeMove>(e)) {
					reg.emplace<CompFreeMove>(e);
				}
			}
		}

		if (player.input_respawn) {
			pos.x = g_game_context.spawnpoint_x;
			pos.y = g_game_context.spawnpoint_y;
			physics.dx = 0.0f;
			physics.dy = 0.0f;
		}

		
		if (is_dashing){
			CompDash &dash = reg.get<CompDash>(e);
			dash.dash_time += tf;

			pos.x += dash.vel_dx;
			physics.dx = dash.vel_dx;
			
			pos.y += dash.vel_dy;
			physics.dy = dash.vel_dy;

			if (dash.dash_time >= DASH_TIME) {
				reg.remove<CompDash>(e);
				player.air_time = 0.0f;
			}
		}

		if (is_free_move || is_dashing) {
			continue;
		}

		// horizontal movement and facing
		if (player.input_left && !player.input_right) {
			player.hfacing = CompPlayer::FacingH::LEFT;
			player.looking_back = false;
			physics.dx -= (physics.is_on_ground ? GROUND_ACCEL : AIR_ACCEL) * tf;
		} else if (!player.input_left && player.input_right) {
			player.hfacing = CompPlayer::FacingH::RIGHT;
			player.looking_back = false;
			physics.dx += (physics.is_on_ground ? GROUND_ACCEL : AIR_ACCEL) * tf;
		}
		if (physics.is_on_ground) {
			player.can_dash = true;
			if (player.input_left ^ player.input_right) {
				player.walking_time += tf;
				player.idle_time = 0.0f;
				player.air_time = 0.0f;
			} else {
				player.walking_time = 0.0f;
				player.idle_time += tf;
				player.air_time = 0.0f;
			}
		} else {
			player.walking_time = 0.0f;
			player.idle_time = 0.0f;
			player.air_time += tf;
		}

		// vertical facing
		if (player.input_up) {
			player.vfacing = CompPlayer::FacingV::UP;
			player.looking_back = false;
		} else if (player.input_down && !player.leaving_door) {
			player.vfacing = CompPlayer::FacingV::DOWN;
		} else {
			player.vfacing = CompPlayer::FacingV::NEUTRAL;
		}

		if (player.can_dash && player.input_dash_ff) {
			player.can_dash = false;
			reg.emplace<CompDash>(e);
			CompDash &dash = reg.get<CompDash>(e);
			float dashVel = DASH_DISTANCE/DASH_TIME;
			switch (int(player.input_up) - int(player.input_down)) {
				case 1:
					dash.vel_dy = -dashVel;
					break;
				case -1:
					dash.vel_dy = dashVel;
					break;
				default:
					dash.vel_dy = 0.0f;
					break;
			}
			if (dash.vel_dy == 0){
				switch (player.hfacing) {
					case CompPlayer::FacingH::LEFT:
						dash.vel_dx = -dashVel;
						break;
					case CompPlayer::FacingH::RIGHT:
						dash.vel_dx = dashVel;
						break;
					default:
					break; // ???
				}
			}else{
				switch (int(player.input_right) - int(player.input_left)) {
					case 1:
						dash.vel_dx = dashVel * sqrt(2)/2;
						dash.vel_dy *= sqrt(2)/2;
						break;
					case -1:
						dash.vel_dx = -dashVel * sqrt(2)/2;
						dash.vel_dy *= sqrt(2)/2;
						break;
					default:
						dash.vel_dx = 0.0f;
						break;
				}
			}
		}

		// look back
		if (player.input_down_ff) {
			if (physics.is_on_ground && !(player.input_left || player.input_right)) {
				player.looking_back = true;
			}
		}
		if (!physics.is_on_ground) {
			player.looking_back = false;
		}

		// leaving doors
		if (!player.input_down) {
			player.leaving_door = false;
		}

		// grace bhops
		if (player.grace_bhop_timer > 0.0f) {
			player.grace_bhop_timer -= tf;
		}

		// grace jumps
		if (!physics.is_on_ground && physics.was_on_ground && physics.dy >= 0) {
			player.grace_jump_timer = GRACE_JUMP_TIME;
		}
		if (physics.is_on_ground) {
			player.grace_jump_base_height = pos.y;
		}

		// jumps
		if (player.input_jump_ff) {
			if (physics.is_on_ground || player.grace_jump_timer > 0.0f) {
				pos.y = player.grace_jump_base_height;
				sub_jump(physics, player);
			} else {
				player.grace_bhop_timer = GRACE_BHOP_TIME;
			}
		} else if (physics.is_on_ground && !physics.was_on_ground && player.grace_bhop_timer > 0) {
			sub_jump(physics, player);
		}
		if (player.grace_jump_timer > 0.0f) {
			player.grace_jump_timer -= tf;
		}

		// held jump
		if (!physics.is_on_ground && (player.input_jump || player.input_aircontrol)) {
			physics.gravity_multiplier = JUMP_GRAVITY_MULTIPLIER;
		} else {
			physics.gravity_multiplier = 1.0f;
		}
	}
}

void sys_free_move(entt::registry &reg) {
	const float tf = g_game_context.time_factor;

	const auto entities = reg.view<CompFreeMove, CompPosition, CompPlayer>();
	for (const entt::entity e : entities) {
		CompPosition &pos = reg.get<CompPosition>(e);
		CompPlayer &player = reg.get<CompPlayer>(e);

		float free_move_speed;
		if (player.input_aircontrol) { free_move_speed = FREE_MOVE_SPEED_FAST; }
		else if (player.input_jump) { free_move_speed = FREE_MOVE_SPEED_SLOW; }
		else { free_move_speed = FREE_MOVE_SPEED; }

		if (player.input_left)
			pos.x -= free_move_speed * tf;
		if (player.input_right)
			pos.x += free_move_speed * tf;
		if (player.input_up)
			pos.y -= free_move_speed * tf;
		if (player.input_down)
			pos.y += free_move_speed * tf;

		speedrun_invalidate();
	}
}

void sys_player_render(entt::registry &reg) {
	const auto entities = reg.view<CompPosition, CompPhysics, CompPlayer>(entt::exclude<CompTransit>);
	for (const entt::entity e : entities) {
		CompPosition &pos = reg.get<CompPosition>(e);
		CompPhysics &physics = reg.get<CompPhysics>(e);
		CompPlayer &player = reg.get<CompPlayer>(e);

		int frame_index = 0;

		if (player.looking_back) {
			frame_index = g_game_context.playerchar.back;
		} else if (physics.is_on_ground) {
			if (player.walking_time > 0.0f) {
				int walk_cycle_index = player.walking_time * g_game_context.playerchar.animation_time;
				if (player.vfacing == CompPlayer::FacingV::UP) {
					frame_index = g_game_context.playerchar.walk_up[walk_cycle_index % g_game_context.playerchar.walk_up.size()];
				} else {
					frame_index = g_game_context.playerchar.walk[walk_cycle_index % g_game_context.playerchar.walk.size()];
				}
			} else {
				int idle_cycle_index = player.idle_time * g_game_context.playerchar.animation_time;
				if (player.vfacing == CompPlayer::FacingV::UP) {
					frame_index = g_game_context.playerchar.idle_up[idle_cycle_index % g_game_context.playerchar.idle_up.size()];
				} else {
					frame_index = g_game_context.playerchar.idle[idle_cycle_index % g_game_context.playerchar.idle.size()];
				}
			}
		} else {
			int jump_cycle_index = player.air_time * g_game_context.playerchar.animation_time;
			if (physics.dy >= 0.0f) {
				// moving downward
				switch (player.vfacing) {
					case CompPlayer::FacingV::NEUTRAL:
						frame_index = g_game_context.playerchar.fall[jump_cycle_index % g_game_context.playerchar.fall.size()];
						break;
					case CompPlayer::FacingV::UP:
						frame_index = g_game_context.playerchar.fall_up[jump_cycle_index % g_game_context.playerchar.fall_up.size()];
						break;
					case CompPlayer::FacingV::DOWN:
						frame_index = g_game_context.playerchar.fall_down[jump_cycle_index % g_game_context.playerchar.fall_down.size()];
						break;
				}
			} else {
				// moving upward
				switch (player.vfacing) {
					case CompPlayer::FacingV::NEUTRAL:
						frame_index = g_game_context.playerchar.jump[jump_cycle_index % g_game_context.playerchar.jump.size()];
						break;
					case CompPlayer::FacingV::UP:
						frame_index = g_game_context.playerchar.jump_up[jump_cycle_index % g_game_context.playerchar.jump_up.size()];
						break;
					case CompPlayer::FacingV::DOWN:
						frame_index = g_game_context.playerchar.jump_down[jump_cycle_index % g_game_context.playerchar.jump_down.size()];
						break;
				}
			}
		}

		unsigned int width = g_game_context.playerchar.frame_width;
		unsigned int height = g_game_context.playerchar.image_height;
		NEWRECT srcrect = NEWRECT::of_points(frame_index * width, 0.0f, (frame_index + 1) * width, height);
		if ((player.hfacing == CompPlayer::FacingH::RIGHT) ^ g_game_context.playerchar.rightfacing) {
			srcrect = NEWRECT::xflip(srcrect);
		}

		NEWRECT dstrect = NEWRECT::offset(NEWRECT::centered_of_size(width, height), pos.x, pos.y - (int)((height - 32) / 2) - g_game_context.playerchar.vertical_offset);
		gfx_draw_texture(g_game_context.playerchar_texture, srcrect, dstrect);

		constexpr float keys_orbit_width = 50.0f;
		constexpr float keys_orbit_height = 35.0f;
		constexpr float keys_orbit_speed = 0.001f;
		int num_keys = player.keys_gids.size();
		int i = 0;
		for (unsigned int key_gid : player.keys_gids) {
			float v = SDL_GetTicks() * keys_orbit_speed;
			float ex = cosf(v + M_PI * 2 * (i / (float)num_keys)) * keys_orbit_width;
			float ey = sinf(v + M_PI * 2 * (i / (float)num_keys)) * keys_orbit_height;
			const ZrTile *tile = get_tile(key_gid);
			NEWRECT keys_dstrect = NEWRECT::offset(NEWRECT::centered_of_size(tile->srcw, tile->srch), pos.x + ex, pos.y + ey);
			gfx_draw_tile(key_gid, keys_dstrect);
			i++;
		}
	}
}

void sys_debug_entity_info(entt::registry &reg) {
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);

	const auto entities = reg.view<CompPosition>();
	for (const entt::entity e : entities) {
		const CompPosition &pos = reg.get<CompPosition>(e);

		float x, y;
		gfx_transform_coordinates(pos.x, pos.y, &x, &y);
		ImGui::SetNextWindowPos(ImVec2(x, y), ImGuiCond_Always);
		char buf[32];
		snprintf(buf, sizeof buf, "debug_entity_info_%08x", (unsigned int)e);
		if (ImGui::Begin(buf, nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_AlwaysAutoResize)) {
			if (g_client_context.caption_entity_ids) {
				ImGui::Text("Eid 0x%08x", (unsigned int)e);
			}

			if (reg.all_of<CompPlayer>(e)) {
				const CompPlayer &player = reg.get<CompPlayer>(e);
				//ImGui::TextColored(ImVec4(1.0f, 0.5f, 0.5f, 1.0f), "grace_jump_timer: % 2d", player.grace_jump_timer);
				//ImGui::TextColored(ImVec4(1.0f, 0.5f, 0.5f, 1.0f), "grace_bhop_timer: % 2d", player.grace_bhop_timer);

				static const ImVec4 white(1.0f, 1.0f, 1.0f, 1.0f);
				static const ImVec4 green(0.0f, 0.75f, 1.0f, 1.0f);

				ImGui::TextColored(player.input_dash       ? green : white, "Z "); ImGui::SameLine();
				ImGui::TextColored(player.input_jump       ? green : white, "X "); ImGui::SameLine();
				ImGui::TextColored(player.input_aircontrol ? green : white, "C "); ImGui::SameLine();
				ImGui::TextColored(player.input_left       ? green : white, "< "); ImGui::SameLine();
				ImGui::TextColored(player.input_right      ? green : white, "> "); ImGui::SameLine();
				ImGui::TextColored(player.input_up         ? green : white, "^ "); ImGui::SameLine();
				ImGui::TextColored(player.input_down       ? green : white, "v");

				if (g_game_context.total_coins > 0) {
					ImGui::Text("coins: %d / %d", g_game_context.coin_count, g_game_context.total_coins);
				}

				if (g_game_context.speedrun_mode) {
					if (g_game_context.speedrun_finished) {
						ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 1.0f, 0.5f, 1.0f));
					}
					unsigned int min = g_game_context.speedrun_time / 60;
					unsigned int sec = (unsigned int)g_game_context.speedrun_time % 60;
					unsigned int csec = (unsigned int)(g_game_context.speedrun_time * 100) % 100;
					if (min == 0) {
						ImGui::Text("time: %d.%02d", sec, csec);
					} else {
						ImGui::Text("time: %d:%02d.%02d", min, sec, csec);
					}
					if (g_game_context.speedrun_finished) {
						ImGui::PopStyleColor();
					}
				}
			}

			ImGui::End();
		}
	}

	ImGui::PopStyleColor();
	ImGui::PopStyleVar(2);
}

// collision constants
#define FLOOR_CLIP_UP_HEIGHT 0.5f
#define FLOOR_CLIP_UP_INSET 0.25f
#define FLOOR_CLIP_DOWN_HEIGHT 0.25f
#define FLOOR_CLIP_DOWN_INSET 0.25f
#define WALL_CLIP_OUT_WIDTH 0.5f
#define WALL_CLIP_OUT_INSET 0.25f
#define CEILING_CLIP_DOWN_HEIGHT 0.5f
#define CEILING_CLIP_DOWN_INSET 0.25f

NEWRECT body_collision_get_floor_snap_up_rect(const BodyCollisionPart &bodypart, float x, float y) {
	NEWRECT rect;
	rect.x1 = x + bodypart.rect.x1 + NEWRECT::width(bodypart.rect) * FLOOR_CLIP_UP_INSET;
	rect.y1 = y + bodypart.rect.y2 - NEWRECT::height(bodypart.rect) * FLOOR_CLIP_UP_HEIGHT;
	rect.x2 = x + bodypart.rect.x2 - NEWRECT::width(bodypart.rect) * FLOOR_CLIP_UP_INSET;
	rect.y2 = y + bodypart.rect.y2;
	return rect;
}
NEWRECT body_collision_get_floor_snap_down_rect(const BodyCollisionPart &bodypart, float x, float y) {
	NEWRECT rect;
	rect.x1 = x + bodypart.rect.x1 + NEWRECT::width(bodypart.rect) * FLOOR_CLIP_DOWN_INSET;
	rect.y1 = y + bodypart.rect.y2;
	rect.x2 = x + bodypart.rect.x2 - NEWRECT::width(bodypart.rect) * FLOOR_CLIP_DOWN_INSET;
	rect.y2 = y + bodypart.rect.y2 + NEWRECT::height(bodypart.rect) * FLOOR_CLIP_DOWN_HEIGHT;
	return rect;
}
NEWRECT body_collision_get_wall_snap_left_rect(const BodyCollisionPart &bodypart, float x, float y) {
	NEWRECT rect;
	rect.x1 = x + bodypart.rect.x2 - NEWRECT::width(bodypart.rect) * WALL_CLIP_OUT_WIDTH;
	rect.y1 = y + bodypart.rect.y1 + NEWRECT::height(bodypart.rect) * WALL_CLIP_OUT_INSET;
	rect.x2 = x + bodypart.rect.x2;
	rect.y2 = y + bodypart.rect.y2 - NEWRECT::height(bodypart.rect) * WALL_CLIP_OUT_INSET;
	return rect;
}
NEWRECT body_collision_get_wall_snap_right_rect(const BodyCollisionPart &bodypart, float x, float y) {
	NEWRECT rect;
	rect.x1 = x + bodypart.rect.x1;
	rect.y1 = y + bodypart.rect.y1 + NEWRECT::height(bodypart.rect) * WALL_CLIP_OUT_INSET;
	rect.x2 = x + bodypart.rect.x1 + NEWRECT::width(bodypart.rect) * WALL_CLIP_OUT_WIDTH;
	rect.y2 = y + bodypart.rect.y2 - NEWRECT::height(bodypart.rect) * WALL_CLIP_OUT_INSET;
	return rect;
}
NEWRECT body_collision_get_ceiling_snap_down_rect(const BodyCollisionPart &bodypart, float x, float y) {
	NEWRECT rect;
	rect.x1 = x + bodypart.rect.x1 + NEWRECT::width(bodypart.rect) * CEILING_CLIP_DOWN_INSET;
	rect.y1 = y + bodypart.rect.y1;
	rect.x2 = x + bodypart.rect.x2 - NEWRECT::width(bodypart.rect) * CEILING_CLIP_DOWN_INSET;
	rect.y2 = y + bodypart.rect.y1 + NEWRECT::height(bodypart.rect) * CEILING_CLIP_DOWN_HEIGHT;
	return rect;
}

// TODO: should floor snap down be processed very last?
void sys_static_collision(entt::registry &reg) {
	const auto bodies = reg.view<CompPosition, CompPhysics, CompBodyCollision>(no_collision_exclude);
	const auto statics = reg.view<CompPosition, CompStaticCollision>();

	for (const entt::entity body : bodies) {
		CompPosition &bodypos = reg.get<CompPosition>(body);
		CompPhysics &bodyphysics = reg.get<CompPhysics>(body);
		CompBodyCollision &bodycol = reg.get<CompBodyCollision>(body);

		float body_angle = atan2f(bodyphysics.dy, bodyphysics.dx);

		bool floor_snap_up_displaced = false;
		float floor_snap_up_displacement = 0.0f;
		float floor_snap_up_slope_angle = 0.0f;
		bool floor_snap_down_displaced = false;
		float floor_snap_down_displacement = 0.0f;

		for (const entt::entity stat : statics) {
			if (body == stat) break;
			CompPosition &statpos = reg.get<CompPosition>(stat);
			CompStaticCollision &statcol = reg.get<CompStaticCollision>(stat);
			for (const StaticCollisionPart statpart : statcol.parts) {
				if (statpart.type != StaticCollisionPart::FLOOR) continue;

				for (const BodyCollisionPart bodypart : bodycol.parts) {
					if (!bodypart.solid) continue;
					// snap up
					{
						NEWRECT floor_snap_up_rect = body_collision_get_floor_snap_up_rect(bodypart, bodypos.x, bodypos.y);
						float isec_x1 = statpos.x + statpart.x1;
						float isec_y1 = statpos.y + statpart.y1;
						float isec_x2 = statpos.x + statpart.x2;
						float isec_y2 = statpos.y + statpart.y2;
						if (intersect_rect_and_line(floor_snap_up_rect, &isec_x1, &isec_y1, &isec_x2, &isec_y2)) {
							float highest_y_pos = minf(isec_y1, isec_y2);
							float slope_angle = fmod(atan2f(isec_y2 - isec_y1, isec_x2 - isec_x1) - M_PI, M_PI); // -pi ~ 0
							float y_displacement = highest_y_pos - floor_snap_up_rect.y2;
							floor_snap_up_displaced = true;
							floor_snap_up_displacement = minf(floor_snap_up_displacement, y_displacement);
							floor_snap_up_slope_angle = slope_angle;
						}
					}

					// snap down
					if (bodyphysics.is_on_ground && !floor_snap_up_displaced && bodyphysics.dy >= 0.0f) { // moving downward
						NEWRECT floor_snap_down_rect = body_collision_get_floor_snap_down_rect(bodypart, bodypos.x, bodypos.y);
						float isec_x1 = statpos.x + statpart.x1;
						float isec_y1 = statpos.y + statpart.y1;
						float isec_x2 = statpos.x + statpart.x2;
						float isec_y2 = statpos.y + statpart.y2;
						if (intersect_rect_and_line(floor_snap_down_rect, &isec_x1, &isec_y1, &isec_x2, &isec_y2)) {
							float highest_y_pos = minf(isec_y1, isec_y2);
							float y_displacement = highest_y_pos - floor_snap_down_rect.y1;
							if (!floor_snap_down_displaced || y_displacement < floor_snap_down_displacement) {
								floor_snap_down_displaced = true;
								floor_snap_down_displacement = y_displacement;
							}
						}
					}
				}
			}
		}

		bodyphysics.was_on_ground = bodyphysics.is_on_ground;
		if (floor_snap_up_displaced) {
			// this DOES optimize to a short-circuit
			const bool moving_downward = bodyphysics.dy >= 0.0f;
			const bool right_acute = floor_snap_up_slope_angle > -0.5f * M_PI && body_angle > floor_snap_up_slope_angle; // < up-right slope angle is steeper than body angle
			const bool left_acute = floor_snap_up_slope_angle < -0.5f * M_PI && body_angle < floor_snap_up_slope_angle; // < up-left slope angle is steeper than body anngle

			if (moving_downward || right_acute || left_acute) {
				bodypos.y += floor_snap_up_displacement;
				bodyphysics.is_on_ground = true;
				bodyphysics.dy = 0.0f;
			}
		} else if (floor_snap_down_displaced) {
			bodypos.y += floor_snap_down_displacement;
			bodyphysics.is_on_ground = true;
			bodyphysics.dy = 0.0f;
		} else {
			bodyphysics.is_on_ground = false;
		}

		bool wall_snap_left_displaced = false;
		float wall_snap_left_displacement = 0.0f;
		bool wall_snap_right_displaced = false;
		float wall_snap_right_displacement = 0.0f;

		for (const entt::entity stat : statics) {
			if (body == stat) break;
			CompPosition &statpos = reg.get<CompPosition>(stat);
			CompStaticCollision &statcol = reg.get<CompStaticCollision>(stat);
			for (const StaticCollisionPart statpart : statcol.parts) {
				if (statpart.type == StaticCollisionPart::WALL_LEFT) {
					for (const BodyCollisionPart bodypart : bodycol.parts) {
						if (!bodypart.solid) continue;
						NEWRECT wall_clip_left_rect = body_collision_get_wall_snap_left_rect(bodypart, bodypos.x, bodypos.y);
						float isec_x1 = statpos.x + statpart.x1;
						float isec_y1 = statpos.y + statpart.y1;
						float isec_x2 = statpos.x + statpart.x2;
						float isec_y2 = statpos.y + statpart.y2;
						if (intersect_rect_and_line(wall_clip_left_rect, &isec_x1, &isec_y1, &isec_x2, &isec_y2)) {
							float leftmost_x_pos = minf(isec_x1, isec_x2);
							float x_displacement = leftmost_x_pos - wall_clip_left_rect.x2;
							if (!wall_snap_left_displaced || x_displacement < wall_snap_left_displacement) {
								wall_snap_left_displacement = x_displacement;
								wall_snap_left_displaced = true;
							}
						}
					}
				} else if (statpart.type == StaticCollisionPart::WALL_RIGHT) {
					for (const BodyCollisionPart bodypart : bodycol.parts) {
						if (!bodypart.solid) continue;
						NEWRECT wall_clip_right_rect = body_collision_get_wall_snap_right_rect(bodypart, bodypos.x, bodypos.y);
						float isec_x1 = statpos.x + statpart.x1;
						float isec_y1 = statpos.y + statpart.y1;
						float isec_x2 = statpos.x + statpart.x2;
						float isec_y2 = statpos.y + statpart.y2;
						if (intersect_rect_and_line(wall_clip_right_rect, &isec_x1, &isec_y1, &isec_x2, &isec_y2)) {
							float rightmost_x_pos = maxf(isec_x1, isec_x2);
							float x_displacement = rightmost_x_pos - wall_clip_right_rect.x1;
							if (!wall_snap_right_displaced || x_displacement > wall_snap_right_displacement) {
								wall_snap_right_displacement = x_displacement;
								wall_snap_right_displaced = true;
							}
						}
					}
				}
			}
		}

		if (wall_snap_left_displaced && wall_snap_right_displaced) {
			bodyphysics.dx = 0.0f;
		} else if (wall_snap_left_displaced) {
			bodypos.x += wall_snap_left_displacement;
			if (bodyphysics.dx >= 0.0f) {
				bodyphysics.dx = 0.0f;
			}
		} else if (wall_snap_right_displaced) {
			bodypos.x += wall_snap_right_displacement;
			if (bodyphysics.dx <= 0.0f) {
				bodyphysics.dx = 0.0f;
			}
		}

		bool ceiling_snap_down_displaced = false;
		float ceiling_snap_down_displacement = 0.0f;

		for (const entt::entity stat : statics) {
			if (body == stat) break;
			CompPosition &statpos = reg.get<CompPosition>(stat);
			CompStaticCollision &statcol = reg.get<CompStaticCollision>(stat);
			for (const StaticCollisionPart statpart : statcol.parts) {
				if (statpart.type != StaticCollisionPart::CEILING) continue;

				for (const BodyCollisionPart bodypart : bodycol.parts) {
					if (!bodypart.solid) continue;
					NEWRECT ceiling_snap_down_rect = body_collision_get_ceiling_snap_down_rect(bodypart, bodypos.x, bodypos.y);
					float isec_x1 = statpos.x + statpart.x1;
					float isec_y1 = statpos.y + statpart.y1;
					float isec_x2 = statpos.x + statpart.x2;
					float isec_y2 = statpos.y + statpart.y2;
					if (intersect_rect_and_line(ceiling_snap_down_rect, &isec_x1, &isec_y1, &isec_x2, &isec_y2)) {
						float lowest_y_pos = maxf(isec_y1, isec_y2);
						float y_displacement = lowest_y_pos - ceiling_snap_down_rect.y1;
						if (!ceiling_snap_down_displaced || y_displacement > ceiling_snap_down_displacement) {
							ceiling_snap_down_displacement = y_displacement;
							ceiling_snap_down_displaced = true;
						}
					}
				}
			}
		}

		if (ceiling_snap_down_displaced) {
			bodypos.y += ceiling_snap_down_displacement;
			if (bodyphysics.dy < HEAD_BONK_STRENGTH) {
				bodyphysics.dy = HEAD_BONK_STRENGTH;
			}
		}
	}
}

struct BodyCollisionManifold {
	entt::entity collider;
	NEWRECT collider_rect;
	entt::entity collidee;
	NEWRECT collidee_rect;
};

template<typename... Component, typename... Exclude>
std::vector<BodyCollisionManifold> body_colliders(entt::registry &reg, entt::entity collider, entt::exclude_t<Exclude...> = {}) {
	std::vector<BodyCollisionManifold> collisions;

	const CompPosition &collider_pos = reg.get<CompPosition>(collider);
	const CompBodyCollision &collider_bodycol = reg.get<CompBodyCollision>(collider);

	const auto collidees = reg.view<CompPosition, CompBodyCollision, Component...>(excludes(no_collision_exclude, entt::exclude<Exclude...>));
	for (const entt::entity collidee : collidees) {
		const CompPosition &collidee_pos = reg.get<CompPosition>(collidee);
		const CompBodyCollision &collidee_bodycol = reg.get<CompBodyCollision>(collidee);

		for (const BodyCollisionPart collider_bodypart : collider_bodycol.parts) {
			NEWRECT collider_rect = NEWRECT::offset(collider_bodypart.rect, collider_pos.x, collider_pos.y);

			for (const BodyCollisionPart collidee_bodypart : collidee_bodycol.parts) {
				NEWRECT collidee_rect = NEWRECT::offset(collidee_bodypart.rect, collidee_pos.x, collidee_pos.y);

				BodyCollisionManifold &manifold = collisions.emplace_back();
				manifold.collider = collider;
				manifold.collider_rect = collider_rect;
				manifold.collidee = collidee;
				manifold.collidee_rect = collidee_rect;
			}
		}
	}

	return collisions;
}

void sys_coin_collection(entt::registry &reg) {
	// collider is the coin collector
	// collidee is the coin tile
	const auto colliders = reg.view<CompPosition, CompBodyCollision, CompCoinCollector>(no_collision_exclude);
	for (const entt::entity collider : colliders) {
		for (const BodyCollisionManifold &manifold : body_colliders<CompCoinTile>(reg, collider)) {

			if (reg.valid(manifold.collidee) && intersect_rects(manifold.collider_rect, manifold.collidee_rect)) {
				g_game_context.coin_count++;
				reg.destroy(manifold.collidee);
			}
		}
	}
}

void sys_key_collection(entt::registry &reg) {
	// collider is the player
	// collidee is the key tile
	const auto colliders = reg.view<CompPosition, CompBodyCollision, CompPlayer>(no_collision_exclude);
	for (const entt::entity collider : colliders) {
		for (const BodyCollisionManifold &manifold : body_colliders<CompKeyTile>(reg, collider)) {

			if (reg.valid(manifold.collidee) && intersect_rects(manifold.collider_rect, manifold.collidee_rect)) {
				CompPlayer &player = reg.get<CompPlayer>(manifold.collider);
				CompTile &tile = reg.get<CompTile>(manifold.collidee);

				player.keys_gids.push_back(tile.gid);
				reg.destroy(manifold.collidee);
			}
		}
	}
}

void sys_warps(entt::registry &reg) {
	// collider is the player
	// collidee is the warp tile
	const auto colliders = reg.view<CompPosition, CompBodyCollision, CompPlayer, CompPhysics>(no_collision_exclude);
	for (const entt::entity collider : colliders) {
		for (const BodyCollisionManifold &manifold : body_colliders<CompWarpTile>(reg, collider)) {
			if (intersect_rects(manifold.collider_rect, manifold.collidee_rect)) {
				CompPlayer &player = reg.get<CompPlayer>(manifold.collider);
				CompPhysics &playerphysics = reg.get<CompPhysics>(manifold.collider);
				CompWarpTile &warptile = reg.get<CompWarpTile>(manifold.collidee);

				if ((warptile.on_touch || (player.input_down && !player.leaving_door && playerphysics.is_on_ground)) && !reg.all_of<CompTransit>(manifold.collider)) {
					// check for key
					if (warptile.key_gid) {
						auto it = std::find(player.keys_gids.begin(), player.keys_gids.end(), warptile.key_gid);
						if (it == player.keys_gids.end()) {
							// not have
							break;
						} else {
							// have
							player.keys_gids.erase(it);
							warptile.key_gid = 0;
							CompTile &tile = reg.get<CompTile>(manifold.collidee);
							tile.gid = warptile.unlock_tile_gid;
						}
					}

					player.looking_back = false;
					if (!warptile.on_touch) {
						player.leaving_door = true;
					}

					if (warptile.points.size() == 1) {
						// warp instantly if there's only one point
						CompPosition &playerpos = reg.get<CompPosition>(collider);
						playerpos.x = warptile.points[0].x;
						playerpos.y = warptile.points[0].y;
						if (warptile.set_velocity) {
							CompPhysics &playerphysics = reg.get<CompPhysics>(collider);
							playerphysics.dx = warptile.vel_dx;
							playerphysics.dy = warptile.vel_dy;
						}
						player.grace_jump_base_height = playerpos.y;
						player.grace_jump_timer = GRACE_JUMP_TIME;

					} else if (warptile.warp_to_spawn) {
						CompPosition &playerpos = reg.get<CompPosition>(collider);
						playerpos.x = g_game_context.spawnpoint_x;
						playerpos.y = g_game_context.spawnpoint_y;
						if (warptile.set_velocity) {
							CompPhysics &playerphysics = reg.get<CompPhysics>(collider);
							playerphysics.dx = warptile.vel_dx;
							playerphysics.dy = warptile.vel_dy;
						}
						player.grace_jump_base_height = playerpos.y;
						player.grace_jump_timer = GRACE_JUMP_TIME;

					} else {
						CompTransit &transit = reg.emplace<CompTransit>(manifold.collider);
						transit.time = 0.0f;
						transit.points = warptile.points;
						transit.speed_multiplier = warptile.speed_multiplier;
						transit.set_velocity = warptile.set_velocity;
						transit.vel_dx = warptile.vel_dx;
						transit.vel_dy = warptile.vel_dy;

					}
				}
			}
		}
	}
}

void sys_transit(entt::registry &reg) {
	const float tf = g_game_context.time_factor;

	const auto entities = reg.view<CompPosition, CompPhysics, CompPlayer, CompTransit>();
	for (const entt::entity e : entities) {
		CompPosition &pos = reg.get<CompPosition>(e);
		CompPhysics &physics = reg.get<CompPhysics>(e);
		CompPlayer &player = reg.get<CompPlayer>(e);
		CompTransit &transit = reg.get<CompTransit>(e);

		transit.time += TRANSIT_SPEED * tf * transit.speed_multiplier;

		float last_x, last_y, last_time;
		int num_points = 0;
		for (const TransitPoint &point : transit.points) {
			// checking `num_points > 0` here is unnecessary because `transit.time < point.end_time` can never be true for the first point under any sane circumstance
			// (the first point's end time is always 0, and the transit time is always greater than 0)
			// but adding the check satisfies GCC's static analysis

			if (transit.time < point.end_time && num_points > 0) {
				// reached point, interp between this and last
				float t = (transit.time - last_time) / (point.end_time - last_time);
				pos.x = lerpf(last_x, point.x, t);
				pos.y = lerpf(last_y, point.y, t);

				if (last_x > point.x) {
					player.hfacing = CompPlayer::FacingH::LEFT;
				} else if (last_x < point.x) {
					player.hfacing = CompPlayer::FacingH::RIGHT;
				}
				return;
			}

			last_x = point.x;
			last_y = point.y;
			last_time = point.end_time;
			num_points++;
		}

		const TransitPoint &end_point = transit.points.back();

		pos.x = end_point.x;
		pos.y = end_point.y;

		if (transit.set_velocity) {
			physics.dx = transit.vel_dx;
			physics.dy = transit.vel_dy;
		}

		player.grace_jump_base_height = pos.y;
		player.grace_jump_timer = GRACE_JUMP_TIME;

		reg.remove<CompTransit>(e);
	}
}

void sys_debug_static_collision(entt::registry &reg) {
	const auto entities = reg.view<CompPosition, CompStaticCollision>();
	for (const entt::entity e : entities) {
		const CompPosition &pos = reg.get<CompPosition>(e);
		const CompStaticCollision &statcol = reg.get<CompStaticCollision>(e);

		for (const StaticCollisionPart statpart : statcol.parts) {
			float x1 = pos.x + statpart.x1;
			float y1 = pos.y + statpart.y1;
			float x2 = pos.x + statpart.x2;
			float y2 = pos.y + statpart.y2;
			switch (statpart.type) {
				case StaticCollisionPart::FLOOR:
					if (g_client_context.collision_view_control.show_static_floor) {
						gfx_draw_line(g_client_context.collision_view_colors.static_floor, x1, y1, x2, y2);
					}
					break;
				case StaticCollisionPart::WALL_LEFT:
					if (g_client_context.collision_view_control.show_static_wall_left) {
						gfx_draw_line(g_client_context.collision_view_colors.static_wall_left, x1, y1, x2, y2);
					}
					break;
				case StaticCollisionPart::WALL_RIGHT:
					if (g_client_context.collision_view_control.show_static_wall_right) {
						gfx_draw_line(g_client_context.collision_view_colors.static_wall_right, x1, y1, x2, y2);
					}
					break;
				case StaticCollisionPart::CEILING:
					if (g_client_context.collision_view_control.show_static_ceiling) {
						gfx_draw_line(g_client_context.collision_view_colors.static_ceiling, x1, y1, x2, y2);
					}
					break;
				default:
					break; // ???
			}
		}
	}
}

void sys_debug_body_collision(entt::registry &reg) {
	const auto entities = reg.view<CompPosition, CompBodyCollision>();
	for (const entt::entity e : entities) {
		const CompPosition &pos = reg.get<CompPosition>(e);
		const CompBodyCollision &bodycol = reg.get<CompBodyCollision>(e);

		for (const BodyCollisionPart bodypart : bodycol.parts) {
			// main
			if (g_client_context.collision_view_control.show_body_main) {
				NEWRECT r = NEWRECT::offset(bodypart.rect, pos.x, pos.y);
				gfx_draw_rect_filled(g_client_context.collision_view_colors.body_main_bg, r);
				gfx_draw_rect(g_client_context.collision_view_colors.body_main, r);
			}

			if (bodypart.solid) {
				// floor snap up
				if (g_client_context.collision_view_control.show_body_floor_snap_up) {
					NEWRECT r = body_collision_get_floor_snap_up_rect(bodypart, pos.x, pos.y);
					gfx_draw_rect_filled(g_client_context.collision_view_colors.body_floor_snap_up_bg, r);
					gfx_draw_rect(g_client_context.collision_view_colors.body_floor_snap_up, r);
				}

				// floor snap down
				if (g_client_context.collision_view_control.show_body_floor_snap_down) {
					NEWRECT r = body_collision_get_floor_snap_down_rect(bodypart, pos.x, pos.y);
					gfx_draw_rect_filled(g_client_context.collision_view_colors.body_floor_snap_down_bg, r);
					gfx_draw_rect(g_client_context.collision_view_colors.body_floor_snap_down, r);
				}

				// wall snap left
				if (g_client_context.collision_view_control.show_body_wall_snap_left) {
					NEWRECT r = body_collision_get_wall_snap_left_rect(bodypart, pos.x, pos.y);
					gfx_draw_rect_filled(g_client_context.collision_view_colors.body_wall_snap_left_bg, r);
					gfx_draw_rect(g_client_context.collision_view_colors.body_wall_snap_left, r);
				}

				// wall snap right
				if (g_client_context.collision_view_control.show_body_wall_snap_right) {
					NEWRECT r = body_collision_get_wall_snap_right_rect(bodypart, pos.x, pos.y);
					gfx_draw_rect_filled(g_client_context.collision_view_colors.body_wall_snap_right_bg, r);
					gfx_draw_rect(g_client_context.collision_view_colors.body_wall_snap_right, r);
				}

				// ceiling snap down
				if (g_client_context.collision_view_control.show_body_ceiling_snap_down) {
					NEWRECT r = body_collision_get_ceiling_snap_down_rect(bodypart, pos.x, pos.y);
					gfx_draw_rect_filled(g_client_context.collision_view_colors.body_ceiling_snap_down_bg, r);
					gfx_draw_rect(g_client_context.collision_view_colors.body_ceiling_snap_down, r);
				}
			}
		}
	}
}

void sub_tile_layer_render(entt::registry &reg, entt::entity e) {
	CompPosition &pos = reg.get<CompPosition>(e);
	CompTileLayer &tilelayer = reg.get<CompTileLayer>(e);

	TileLayerRenderOrder order = tilelayer.render_order;
	bool bottom_to_top = order == RIGHT_UP || order == LEFT_UP;
	bool right_to_left = order == LEFT_UP || order == LEFT_DOWN;

	if (tilelayer.width == 0 || tilelayer.height == 0) return; // this case shouldn't be possible

	int xstride = g_client_context.window_width / 2 / abs(g_client_context.view_scale);
	int ystride = g_client_context.window_height / 2 / abs(g_client_context.view_scale);

	int leftmost = clamp((g_client_context.view_x - pos.x - xstride) / tilelayer.tile_width, 0, tilelayer.width - 1);
	int topmost = clamp((g_client_context.view_y - pos.y - ystride) / tilelayer.tile_height, 0, tilelayer.height - 1);
	int rightmost = clamp((g_client_context.view_x - pos.x + xstride) / tilelayer.tile_width, 0, tilelayer.width - 1);
	int bottommost = clamp((g_client_context.view_y - pos.y + ystride) / tilelayer.tile_height, 0, tilelayer.height - 1);

	int startx = right_to_left ? rightmost  : leftmost;
	int endx   = right_to_left ? leftmost   : rightmost;
	int dx     = right_to_left ? -1         : 1;
	int starty = bottom_to_top ? bottommost : topmost;
	int endy   = bottom_to_top ? topmost    : bottommost;
	int dy     = bottom_to_top ? -1         : 1;

	int iy = starty;
	for (;;) {
		int ix = startx;
		for (;;) {
			unsigned int gid = tilelayer.tile_gids[iy * tilelayer.width + ix];
			const ZrTile *tile = get_tile(gid);

			float x = pos.x + ix * tilelayer.tile_width;
			float y = pos.y + iy * tilelayer.tile_height;
			NEWRECT dstrect;
			dstrect.x1 = x;
			dstrect.y1 = y - tile->srch + tilelayer.tile_height;
			dstrect.x2 = x + tile->srcw;
			dstrect.y2 = y + tilelayer.tile_height;
			gfx_draw_tile(gid, dstrect);

			if (ix == endx) break;
			ix += dx;
		}
		if (iy == endy) break;
		iy += dy;
	}
}

ZrTexture *inquiry_tex;

void sub_tile_render(entt::registry &reg, entt::entity e) {
	CompPosition &pos = reg.get<CompPosition>(e);
	CompTile &tile = reg.get<CompTile>(e);

	gfx_draw_tile(tile.gid, NEWRECT::offset(tile.rect, pos.x, pos.y));

	if (reg.all_of<CompWarpTile>(e)) {
		CompWarpTile &warptile = reg.get<CompWarpTile>(e);
		if (warptile.key_gid != 0) {
			gfx_draw_texture(inquiry_tex, { 0.0f, 0.0f, 64.0f, 64.0f }, NEWRECT::offset(NEWRECT::centered_of_size(64.0f, 64.0f), pos.x, pos.y - 32.0f));
			gfx_draw_tile(warptile.key_gid, NEWRECT::offset(tile.rect, pos.x - 8.0f, pos.y - 48.0f));
		}
	}
}

void sys_general_render(entt::registry &reg) {
	reg.sort<CompRender>([&reg](const entt::entity l, const entt::entity r) {
		if (reg.all_of<CompOnLayer>(l) && reg.all_of<CompOnLayer>(r)) {
			unsigned int l_onlayer_index = reg.get<CompOnLayer>(l).index;
			unsigned int r_onlayer_index = reg.get<CompOnLayer>(r).index;
			if (l_onlayer_index < r_onlayer_index) return true;
			if (l_onlayer_index > r_onlayer_index) return false;
		}

		if (reg.all_of<CompObjectLayerObject>(l) && reg.all_of<CompObjectLayerObject>(r)) {
			CompObjectLayerObject &l_objectlayerobject = reg.get<CompObjectLayerObject>(l);
			CompObjectLayerObject &r_objectlayerobject = reg.get<CompObjectLayerObject>(r);

			if (l_objectlayerobject.draworder == ObjectLayerDrawOrder::TOPDOWN) {
				if (reg.get<CompPosition>(l).y < reg.get<CompPosition>(r).y) return true;
				if (reg.get<CompPosition>(l).y > reg.get<CompPosition>(r).y) return false;
			}

			return l_objectlayerobject.index < r_objectlayerobject.index;
		}

		return false;
	}, entt::insertion_sort{});

	const auto entities = reg.view<CompRender>();
	for (const entt::entity e : entities) {
		if (reg.all_of<CompPosition, CompTile>(e)) {
			sub_tile_render(reg, e);
		}
		if (reg.all_of<CompPosition, CompTileLayer>(e)) {
			sub_tile_layer_render(reg, e);
		}
	}
}

void ui_show_paused(bool frame_advancing) {
	ImGui::SetNextWindowPos(ImVec2(g_client_context.window_width / 2.0f, g_client_context.window_height / 3.0f), ImGuiCond_Always, ImVec2(0.5f, 0.5f));
	if (ImGui::Begin("paused", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_AlwaysAutoResize)) {
		const ImVec4 color = frame_advancing ? ImVec4(1.0f, 1.0f, 0.5f, 1.0f) : ImVec4(1.0f, 0.5f, 0.5f, 1.0f);
		ImGui::TextColored(color, "Paused");
	}
	ImGui::End();
}

int main([[maybe_unused]] int argc, char *argv[]) {
	init_input_constants();
	input_init();

	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER);
	SDL_GameControllerEventState(SDL_ENABLE);

	fs_init(argv[0]);

	// default context settings
	init_contexts();

	// config
	load_config_from_disk();

	// init window
#ifdef SDL_VIDEO_DRIVER_X11
	SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0"); // annoying
#endif
	SDL_SetHint(SDL_HINT_VIDEO_ALLOW_SCREENSAVER, "1");
	SDL_SetHint(SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH, "1");

	unsigned int windowflags = SDL_WINDOW_HIDDEN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL;
	if (g_client_context.window_fullscreen) windowflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	if (g_client_context.window_maximized) windowflags |= SDL_WINDOW_MAXIMIZED;
	g_client_context.window = SDL_CreateWindow("zrapr", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, g_client_context.window_width, g_client_context.window_height, windowflags);
	pull_window_state();

	// init window icon
	init_window_icon();

	// init gfx
	gfx_init();

	inquiry_tex = gfx_load_texture_from_file("inquiry.png");
	g_client_context.folders_tex = gfx_load_texture_from_file("folders.png");

	ImGui::SetAllocatorFunctions(
		[](size_t sz, [[maybe_unused]] void *user_data) -> void * { return SDL_malloc(sz); },
		[](void *ptr, [[maybe_unused]] void *user_data) -> void { SDL_free(ptr); },
		nullptr
	);

	// imgui
	g_client_context.imgui_context = ImGui::CreateContext();
	ImGui_ImplSDL2_InitForOpenGL(g_client_context.window, SDL_GL_GetCurrentContext());
	ImGui_ImplOpenGL3_Init();

	g_client_context.imgui_io = &ImGui::GetIO();
	g_client_context.imgui_io->ConfigFlags |= ImGuiConfigFlags_DockingEnable;

	// using a pointer to local data is acceptable here because it's in main()
	// our local variable (imgui_ini_file) will only get destructed at the end of main() anyways
	std::string imgui_ini_file;
	imgui_ini_file.reserve(256);
	imgui_ini_file += fs_get_pref_dir();
	imgui_ini_file += "/imgui.ini";
	g_client_context.imgui_io->IniFilename = imgui_ini_file.c_str();

	ImGuiStyle &style = ImGui::GetStyle();
	style.Colors[ImGuiCol_WindowBg].w = 0.88f;
	style.Colors[ImGuiCol_ModalWindowDimBg].w = 0.1f;
	style.ScrollbarRounding = 0.0f;
	/*for (int c = 0; c < ImGuiCol_COUNT; c++) {
		float tmp = style.Colors[c].x;
		style.Colors[c].x = style.Colors[c].y;
		style.Colors[c].y = tmp;
	}*/

#ifdef NDEBUG
	// show the window
	SDL_ShowWindow(g_client_context.window);
#endif

	entt::registry reg;

	if (!g_game_context.level_path.empty()) {
		load_level(g_game_context.level_path.c_str(), reg);
	}
	if (!g_game_context.playerchar_path.empty()) {
		load_and_set_playerchar(g_game_context.playerchar_path.c_str());
	}

	bool running = true;
	while (running) {
		Uint64 frame_start_time = SDL_GetPerformanceCounter();

		SDL_Event ev;
		while (SDL_PollEvent(&ev)) {
			if (ev.type == SDL_QUIT) {
				running = false;
			}
			if (ev.type == SDL_KEYDOWN) {
				if (ev.key.keysym.sym == SDLK_q) {
					running = false;
				} else if (ev.key.keysym.sym == SDLK_F11) {
					g_client_context.window_fullscreen ^= true;
					push_window_state();
				}
			}
			ImGui_ImplSDL2_ProcessEvent(&ev);
			if (ev.type == SDL_WINDOWEVENT) {
				if (ev.window.event == SDL_WINDOWEVENT_RESIZED) {
					pull_window_state();
				}
			} else if (ev.type == SDL_KEYDOWN) {
				input_event_key_down(ev.key.keysym.scancode);
				g_client_context.hide_cursor_time = 0;
			} else if (ev.type == SDL_KEYUP) {
				input_event_key_up(ev.key.keysym.scancode);
				g_client_context.hide_cursor_time = 0;
			} else if (ev.type == SDL_MOUSEMOTION) {
				g_client_context.hide_cursor_time = ev.motion.timestamp + 2000;
			} else if (ev.type == SDL_CONTROLLERDEVICEADDED) {
				input_controller_open(ev.cdevice.which);
			} else if (ev.type == SDL_CONTROLLERDEVICEREMOVED) {
				input_controller_close(ev.cdevice.which);
			} else if (ev.type == SDL_CONTROLLERBUTTONDOWN) {
				input_event_button_down(ev.cdevice.which, (SDL_GameControllerButton)ev.cbutton.button);
				g_client_context.hide_cursor_time = 0;
			} else if (ev.type == SDL_CONTROLLERBUTTONUP) {
				input_event_button_up(ev.cdevice.which, (SDL_GameControllerButton)ev.cbutton.button);
				g_client_context.hide_cursor_time = 0;
			} else if (ev.type == SDL_CONTROLLERAXISMOTION) {
				input_event_axis_update(ev.cdevice.which, (SDL_GameControllerAxis)ev.caxis.axis, ev.caxis.value);
				g_client_context.hide_cursor_time = 0;
			}
		}
		input_event_frame_end();

		if (SDL_GetTicks() >= g_client_context.hide_cursor_time) {
			ImGui::SetMouseCursor(ImGuiMouseCursor_None);
		}

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame();
		ImGui::NewFrame();

		// make sure the time factor multiplier isn't utterly stupid
		if (g_game_context.time_factor_multiplier < 0.0f) {
			g_game_context.time_factor_multiplier = 0.0f;
		} else if (g_game_context.time_factor_multiplier > 2.0f) {
			g_game_context.time_factor_multiplier = 2.0f;
		}

		g_game_context.time_factor = (60.0f / g_game_context.target_frames_per_second) * g_game_context.time_factor_multiplier;

		if (global_input_is_pressed(g_client_context.input_control.pause)) {
			g_game_context.paused ^= true;
		}
		bool is_frame_advancing = false;
		if (global_input_is_pressed(g_client_context.input_control.frame_advance)) {
			g_game_context.paused = true;
			is_frame_advancing = true;
			speedrun_invalidate();
		}

		if (global_input_is_pressed(g_client_context.input_control.zoom_in)) {
			g_client_context.view_scale++;
			config_mark_dirty();
		}
		if (global_input_is_pressed(g_client_context.input_control.zoom_out)) {
			g_client_context.view_scale--;
			config_mark_dirty();
		}
		if (global_input_is_pressed(g_client_context.input_control.toggle_collision_view)) {
			bool is_collision_on =
				g_client_context.collision_view_control.show_static_floor &&
				g_client_context.collision_view_control.show_static_wall_left &&
				g_client_context.collision_view_control.show_static_wall_right &&
				g_client_context.collision_view_control.show_static_ceiling &&
				g_client_context.collision_view_control.show_body_main &&
				g_client_context.collision_view_control.show_body_floor_snap_up &&
				g_client_context.collision_view_control.show_body_floor_snap_down &&
				g_client_context.collision_view_control.show_body_wall_snap_left &&
				g_client_context.collision_view_control.show_body_wall_snap_right &&
				g_client_context.collision_view_control.show_body_ceiling_snap_down;

			g_client_context.collision_view_control.show_static_floor = !is_collision_on;
			g_client_context.collision_view_control.show_static_wall_left = !is_collision_on;
			g_client_context.collision_view_control.show_static_wall_right = !is_collision_on;
			g_client_context.collision_view_control.show_static_ceiling = !is_collision_on;
			g_client_context.collision_view_control.show_body_main = !is_collision_on;
			g_client_context.collision_view_control.show_body_floor_snap_up = !is_collision_on;
			g_client_context.collision_view_control.show_body_floor_snap_down = !is_collision_on;
			g_client_context.collision_view_control.show_body_wall_snap_left = !is_collision_on;
			g_client_context.collision_view_control.show_body_wall_snap_right = !is_collision_on;
			g_client_context.collision_view_control.show_body_ceiling_snap_down = !is_collision_on;

			config_mark_dirty();
		}

		g_game_context.is_game_frame = !g_game_context.paused || is_frame_advancing;

		if (g_game_context.is_game_frame) {
			sys_player_input(reg);
			sys_player(reg);
			sys_free_move(reg);
			sys_physics(reg);
			sys_static_collision(reg);
			sys_coin_collection(reg);
			sys_key_collection(reg);
			sys_warps(reg);
			sys_transit(reg);

			if (reg.valid(g_client_context.camera_entity) && reg.all_of<CompPosition>(g_client_context.camera_entity)) {
				CompPosition &pos = reg.get<CompPosition>(g_client_context.camera_entity);
				g_client_context.view_x = pos.x;
				g_client_context.view_y = pos.y;
			}

			if (g_game_context.speedrun_mode && g_game_context.coin_count >= g_game_context.total_coins) {
				g_game_context.speedrun_finished = true;
			}
		}

		if (g_game_context.speedrun_mode) {
			if (!g_game_context.speedrun_finished) {
				g_game_context.speedrun_time += 1.0 / g_game_context.target_frames_per_second;
			}
		}

		if (global_input_is_pressed(g_client_context.input_control.reload_level)) {
			load_level(g_game_context.level_path.c_str(), reg);
		}
		if (global_input_is_pressed(g_client_context.input_control.reload_playerchar)) {
			load_and_set_playerchar(g_game_context.playerchar_path.c_str());
		}

		ui_main_menu_bar(&running);

		if (g_game_context.paused) {
			ui_show_paused(is_frame_advancing);
		}

		if (g_client_context.show_imgui_demo) {
			ImGui::ShowDemoWindow(&g_client_context.show_imgui_demo);
		}
		if (g_client_context.show_ecs_view) {
			ui_ecs_view(&g_client_context.show_ecs_view, reg);
		}
		if (g_client_context.show_collision_view_control) {
			ui_collision_view_control(&g_client_context.show_collision_view_control);
		}
		if (g_client_context.show_input_control) {
			ui_input_control(&g_client_context.show_input_control);
		}
		if (g_client_context.show_level_select) {
			ui_level_select(&g_client_context.show_level_select, reg);
		}
		if (g_client_context.show_playerchar_select) {
			ui_playerchar_select(&g_client_context.show_playerchar_select, reg);
		}
		if (g_client_context.show_log) {
			ui_log(&g_client_context.show_log);
		}

		// "this isn't important" -LDA
		// make sure the view scale isn't utterly stupid
		// if (g_client_context.view_scale < 1) {
		// 	g_client_context.view_scale = 1;
		// } else if (g_client_context.view_scale > 10) {
		// 	g_client_context.view_scale = 10;
		// }

		config_tick();

		gfx_begin_frame();

		gfx_clear(g_game_context.map_background_color);

		sys_general_render(reg);
		sys_player_render(reg);

		sys_debug_static_collision(reg);
		sys_debug_body_collision(reg);
		sys_debug_entity_info(reg);

		gfx_end_frame();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		Uint64 frame_end_time = SDL_GetPerformanceCounter();
		g_client_context.frame_duration_seconds = (frame_end_time - frame_start_time) / (float)SDL_GetPerformanceFrequency();

		SDL_GL_SwapWindow(g_client_context.window);

		// make sure the target framerate isn't utterly stupid
		if (g_game_context.target_frames_per_second < 50.0f) {
			g_game_context.target_frames_per_second = 50.0f;
		} else if (g_game_context.target_frames_per_second > 150.0f) {
			g_game_context.target_frames_per_second = 150.0f;
		}

		if (!g_client_context.window_vsync) {
			float frame_period_seconds = 1.0f / g_game_context.target_frames_per_second;
			float seconds_to_wait = maxf(0.0f, frame_period_seconds - g_client_context.frame_duration_seconds);
			sleep(seconds_to_wait);
		}

#ifndef NDEBUG
		static bool shown = false;
		if (!shown) {
			SDL_ShowWindow(g_client_context.window);
			shown = true;
		}
#endif
	}

	save_config_to_disk();

	ImGui::DestroyContext(g_client_context.imgui_context);

	input_uninit();
	fs_uninit();
	gfx_uninit();

	SDL_Quit();

	return 0;
}
