// gfx_tile.hh

#pragma once

#include "geom.hh"

void gfx_draw_tile(unsigned int gid, const NEWRECT rect);
