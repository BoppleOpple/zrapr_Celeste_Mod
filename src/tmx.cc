// tmx.cc

#include "tmx.hh"

#include <cstdint>
#include <map>
#include <string_view>
#include <string>
#include <vector>

#include <SDL_endian.h>
#include <tinyxml2.h>

#include "fs.hh"
#include "log.hh"
#include "map.hh"
#include "util/base64.hh"
#include "util/csv.hh"
#include "util/inflate.hh"

std::vector<unsigned int> bytes_to_uints_le(const std::vector<uint8_t> uchars) {
	std::vector<unsigned int> uints;
	unsigned int num_uints = uchars.size() / 4;
	uints.resize(num_uints);
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
	memcpy(uints.data(), uchars.data(), num_uints * 4);
#else
	unsigned int *uints_data = uints.data();
	const unsigned int *uchars_data_as_uints = (unsigned int *)uchars.data();
	for (unsigned int i = 0; i < num_uints; i++) {
		uints_data[i] = SDL_SwapLE32(uchars_data_as_uints[i]);
	}
#endif
	return uints;
}

// object types
std::map<std::string, TmxProperties> object_types;

/* value_key is 'default' in objecttypes.xml and 'value' anywhere else */
TmxProperties parse_properties(tinyxml2::XMLElement *properties_elem, const char *value_key = "value") {
	if (properties_elem == nullptr) return TmxProperties();

	tinyxml2::XMLElement *property_elem = properties_elem->FirstChildElement("property");
	std::map<std::string, TmxProperty> properties;

	while (property_elem != nullptr) {
		const char *name = property_elem->Attribute("name");
		const char *type_str;
		bool has_type_str = property_elem->QueryStringAttribute("type", &type_str) == tinyxml2::XML_SUCCESS;

		TmxProperty &prop = properties.emplace(name, TmxProperty()).first->second;

		if (!has_type_str || !strcmp(type_str, "string")) {
			prop.type = TmxProperty::Type::STRING;
			const char *s = property_elem->Attribute(value_key);
			prop.value_string = s == nullptr ? "" : s;

		} else if (!strcmp(type_str, "int")) {
			prop.type = TmxProperty::Type::INT;
			prop.value_int = property_elem->IntAttribute(value_key, 0);

		} else if (!strcmp(type_str, "float")) {
			prop.type = TmxProperty::Type::FLOAT;
			prop.value_float = property_elem->FloatAttribute(value_key, 0.0f);

		} else if (!strcmp(type_str, "bool")) {
			prop.type = TmxProperty::Type::BOOL;
			prop.value_bool = property_elem->BoolAttribute(value_key, false);

		} else if (!strcmp(type_str, "object")) {
			prop.type = TmxProperty::Type::OBJECT;
			prop.value_uint = property_elem->UnsignedAttribute(value_key, 0u);

		}

		property_elem = property_elem->NextSiblingElement("property");
	}

	return TmxProperties(properties);
}

TmxProperties get_object_type_properties(const char *type) {
	auto it = object_types.find(type);
	if (it != object_types.end()) {
		return it->second;
	} else {
		zr_log_warn("object type '%s' does not exist", type);
		return TmxProperties();
	}
}

void load_object_types() {
	object_types.clear();

	tinyxml2::XMLDocument doc;
	tinyxml2::XMLError err = fs_load_xml("objecttypes.xml", &doc);
	if (err != tinyxml2::XML_SUCCESS) {
		zr_log_error("failed to load object types: %s", doc.ErrorIDToName(err));
		return;
	}

	tinyxml2::XMLElement *objecttypes_elem = doc.FirstChildElement("objecttypes");
	if (objecttypes_elem == nullptr) {
		zr_log_error("missing objecttypes");
		return;
	}

	tinyxml2::XMLElement *objecttype_elem = objecttypes_elem->FirstChildElement("objecttype");
	while (objecttype_elem != nullptr) {
		const char *objecttype_name = objecttype_elem->Attribute("name");
		object_types.emplace(objecttype_name, parse_properties(objecttype_elem, "default"));
		objecttype_elem = objecttype_elem->NextSiblingElement("objecttype");
	}
}

std::vector<std::pair<float, float>> parse_poly_points(const char *str) {
	std::vector<std::pair<float, float>> points;
	const char *str_end = str + strlen(str);
	for (;;) {
		float x = strtof(str, (char **)&str);
		str++; // comma
		float y = strtof(str, (char **)&str);
		points.emplace_back(x, y);
		if (str >= str_end) break;
		str++; // space
	}
	return points;
}

TmxObject parse_object(tinyxml2::XMLElement *object_elem) {
	TmxObject object;

	object.id = object_elem->UnsignedAttribute("id");
	const char *type = object_elem->Attribute("type");
	object.x = object_elem->FloatAttribute("x");
	object.y = object_elem->FloatAttribute("y");
	object.width = object_elem->FloatAttribute("width");
	object.height = object_elem->FloatAttribute("height");
	object.properties = parse_properties(object_elem->FirstChildElement("properties"));
	if (type != nullptr) {
		object.properties.merge(get_object_type_properties(type));
	}

	object.has_gid = object_elem->QueryUnsignedAttribute("gid", &object.gid) == tinyxml2::XML_SUCCESS;
	if (object.has_gid) {
		object.type = TmxObject::RECTANGLE;
		return object;
	}

	tinyxml2::XMLElement *object_child = object_elem->LastChildElement();
	if (object_child == nullptr) {
		object.type = TmxObject::RECTANGLE;
	} else if (!strcmp(object_child->Value(), "polyline")) {
		object.type = TmxObject::POLYLINE;
		object.polypoints.points = parse_poly_points(object_child->Attribute("points"));
	} else if (!strcmp(object_child->Value(), "polygon")) {
		object.type = TmxObject::POLYGON;
		object.polypoints.points = parse_poly_points(object_child->Attribute("points"));
	} else if (!strcmp(object_child->Value(), "ellipse")) {
		object.type = TmxObject::ELLIPSE;
	} else if (!strcmp(object_child->Value(), "point")) {
		object.type = TmxObject::POINT;
	} else if (!strcmp(object_child->Value(), "text")) {
		zr_log_warn("object %u: text objects are not supported", object.id);
	} else {
		object.type = TmxObject::RECTANGLE;
	}

	return object;
}

void parse_layer_base(TmxLayer *layer, tinyxml2::XMLElement *layer_elem) {
	layer->id = layer_elem->UnsignedAttribute("id");
	// NYI name
	layer->offset_x = layer_elem->FloatAttribute("offsetx");
	layer->offset_y = layer_elem->FloatAttribute("offsety");
	// NYI opacity, tintcolor, visible, parallax
	// NYI layer properties
}

enum LayerDataType { INVALID, CSV, BASE64, BASE64_GZIP, BASE64_ZLIB, BASE64_ZSTD };

LayerDataType get_layer_data_type(const tinyxml2::XMLElement *data_elem) {
	const char *encoding = data_elem->Attribute("encoding");
	if (encoding == nullptr) {
		return LayerDataType::INVALID;
	} else if (!strcmp(encoding, "base64")) {
		const char *compression = data_elem->Attribute("compression");
		if (compression == nullptr) {
			return LayerDataType::BASE64;
		} else if (!strcmp(compression, "gzip")) {
			return LayerDataType::BASE64_GZIP;
		} else if (!strcmp(compression, "zlib")) {
			return LayerDataType::BASE64_ZLIB;
		} else if (!strcmp(compression, "zstd")) {
			return LayerDataType::BASE64_ZSTD;
		}
	} else if (!strcmp(encoding, "csv")) {
		return LayerDataType::CSV;
	}
	return LayerDataType::INVALID;
}

bool parse_object_layer(TmxObjectLayer *objectlayer, tinyxml2::XMLElement *objectgroup_elem) {
	objectlayer->type = TmxLayer::OBJECT_LAYER;
	parse_layer_base(objectlayer, objectgroup_elem);

	const char *draworder_str = objectgroup_elem->Attribute("draworder");

	if (draworder_str == nullptr) {
		objectlayer->draworder = ObjectLayerDrawOrder::TOPDOWN;
	} else if (!strcmp(draworder_str, "topdown")) {
		objectlayer->draworder = ObjectLayerDrawOrder::TOPDOWN;
	} else if (!strcmp(draworder_str, "index")) {
		objectlayer->draworder = ObjectLayerDrawOrder::INDEX;
	} else {
		objectlayer->draworder = ObjectLayerDrawOrder::TOPDOWN;
		zr_log_warn("invalid draworder '%s', defaulting to topdown", draworder_str);
	}

	tinyxml2::XMLElement *object_elem = objectgroup_elem->FirstChildElement("object");
	while (object_elem != nullptr) {
		objectlayer->objects.emplace_back(parse_object(object_elem));
		object_elem = object_elem->NextSiblingElement("object");
	}

	return true;
}

std::vector<unsigned int> decode_data(const TmxTileLayer *tilelayer, LayerDataType type, const char *text) {
	switch (type) {
		case LayerDataType::CSV:
			return csv_decode(text);
		case LayerDataType::BASE64:
			return bytes_to_uints_le(base64_decode(text));
		case LayerDataType::BASE64_GZIP:
		case LayerDataType::BASE64_ZLIB:
			return bytes_to_uints_le(inflate_zlib(base64_decode(text)));
		case LayerDataType::BASE64_ZSTD:
			return bytes_to_uints_le(inflate_zstd(base64_decode(text)));
		default:
			zr_log_error("layer id %u: invalid layer data format. supported layer formats are base64 (gzip, zlib, or zstd compressed) and csv", tilelayer->id);
			return std::vector<unsigned int>();
	}
}

struct Chunk {
	int x;
	int y;
	unsigned int width;
	unsigned int height;
	std::vector<unsigned int> data;
};

static inline int min(int a, int b) { return a < b ? a : b; }
static inline int max(int a, int b) { return a > b ? a : b; }

bool parse_tile_layer(TmxTileLayer *tilelayer, tinyxml2::XMLElement *layer_elem) {
	tilelayer->type = TmxLayer::TILE_LAYER;
	parse_layer_base(tilelayer, layer_elem);

	tilelayer->width = layer_elem->UnsignedAttribute("width");
	tilelayer->height = layer_elem->UnsignedAttribute("height");

	tinyxml2::XMLElement *data_elem = layer_elem->FirstChildElement("data");
	LayerDataType type = get_layer_data_type(data_elem);

	const char *text = data_elem->GetText();

	if (text != nullptr) {
		tilelayer->tile_gids = decode_data(tilelayer, type, text);
	} else {
		std::vector<Chunk> chunks;
		tinyxml2::XMLElement *chunk_elem = data_elem->FirstChildElement("chunk");
		while (chunk_elem != nullptr) {
			Chunk &chunk = chunks.emplace_back();
			chunk.x = chunk_elem->IntAttribute("x");
			chunk.y = chunk_elem->IntAttribute("y");
			chunk.width = chunk_elem->UnsignedAttribute("width");
			chunk.height = chunk_elem->UnsignedAttribute("height");
			chunk.data = decode_data(tilelayer, type, chunk_elem->GetText());

			chunk_elem = chunk_elem->NextSiblingElement("chunk");
		}

		int min_x = 0;
		int min_y = 0;
		int max_x = 0;
		int max_y = 0;
		for (const Chunk &chunk : chunks) {
			min_x = min(min_x, chunk.x);
			min_y = min(min_y, chunk.y);
			max_x = max(max_x, chunk.x + chunk.width);
			max_y = max(max_y, chunk.y + chunk.height);
		}
		int total_width = max_x - min_x;
		int total_height = max_y - min_y;
		tilelayer->tile_gids.resize(total_width * total_height, 0);

		for (const Chunk &chunk : chunks) {
			for (unsigned int iy = 0; iy < chunk.height; iy++) {
				const unsigned int *src = &chunk.data[iy * chunk.width];
				const unsigned int dst_x = chunk.x - min_x;
				const unsigned int dst_y = chunk.y - min_y + iy;
				const unsigned int dst_idx = dst_y * total_width + dst_x;
				const unsigned int dst_amt = chunk.width;
				memcpy(&tilelayer->tile_gids[dst_idx], src, dst_amt * sizeof(unsigned int));
			}
		}

		tilelayer->tile_offset_x = min_x;
		tilelayer->tile_offset_y = min_y;
		tilelayer->width = total_width;
		tilelayer->height = total_height;
	}

	tilelayer->tile_gids.shrink_to_fit();

	return true;
}

bool parse_group_layer(TmxGroupLayer *grouplayer, tinyxml2::XMLElement *group_elem) {
	grouplayer->type = TmxLayer::GROUP_LAYER;
	parse_layer_base(grouplayer, group_elem);

	tinyxml2::XMLElement *group_child = group_elem->FirstChildElement();
	while (group_child != nullptr) {
		if (!strcmp(group_child->Value(), "layer")) {
			grouplayer->layers.emplace_back(new TmxTileLayer);
			TmxTileLayer *child_tilelayer = static_cast<TmxTileLayer *>(grouplayer->layers.back().get());
			parse_tile_layer(child_tilelayer, group_child);

		} else if (!strcmp(group_child->Value(), "objectgroup")) {
			grouplayer->layers.emplace_back(new TmxObjectLayer);
			TmxObjectLayer *child_objectlayer = static_cast<TmxObjectLayer *>(grouplayer->layers.back().get());
			parse_object_layer(child_objectlayer, group_child);

		} else if (!strcmp(group_child->Value(), "group")) {
			grouplayer->layers.emplace_back(new TmxGroupLayer);
			TmxGroupLayer *child_grouplayer = static_cast<TmxGroupLayer *>(grouplayer->layers.back().get());
			parse_group_layer(child_grouplayer, group_child);

		}

		group_child = group_child->NextSiblingElement();
	}
	return true;
}

bool parse_tileset(TmxTileset &tileset, std::string_view base_dir, tinyxml2::XMLElement *tileset_elem) {
	tileset.firstgid = tileset_elem->IntAttribute("firstgid");

	const char *source;
	tinyxml2::XMLDocument tsxdoc;
	if (tileset_elem->QueryStringAttribute("source", &source) == tinyxml2::XML_SUCCESS) {
		std::string source_path;
		source_path.reserve(256);
		source_path += base_dir;
		source_path += '/';
		source_path += source;

		tinyxml2::XMLError err = fs_load_xml(source_path.c_str(), &tsxdoc);
		if (err != tinyxml2::XML_SUCCESS) {
			if (err == tinyxml2::XML_ERROR_FILE_NOT_FOUND) {
				zr_log_error("%s: could not open file", source);
			} else {
				zr_log_error("%s: error parsing xml file: %s", source, tsxdoc.ErrorIDToName(err));
			}
			return false;
		}
		tileset_elem = tsxdoc.FirstChildElement("tileset");
		if (tileset_elem == nullptr) {
			zr_log_error("%s: missing tileset", source);
			return false;
		}
	}

	const char *tileset_name = tileset_elem->Attribute("name");

	tinyxml2::XMLElement *image_elem = tileset_elem->FirstChildElement("image");
	if (image_elem == nullptr) {
		zr_log_error("tilset %s: missing image", tileset_name);
		return false;
	}
	const char *image_source = image_elem->Attribute("source");
	tileset.image.reserve(256);
	tileset.image += base_dir;
	tileset.image += '/';
	tileset.image += image_source;

	const char *image_trans = image_elem->Attribute("trans");
	if (image_trans != nullptr) {
		zr_log_warn("tileset %s: image transparency key is not supported", tileset_name);
	}

	// NYI spacing, margin

	int tilewidth = tileset_elem->IntAttribute("tilewidth");
	int tileheight = tileset_elem->IntAttribute("tileheight");
	tileset.tilecount = tileset_elem->UnsignedAttribute("tilecount");
	unsigned int columns = tileset_elem->UnsignedAttribute("columns");

	tileset.tiles.resize(tileset.tilecount);
	for (unsigned int lid = 0; lid < tileset.tilecount; lid++) {
		TmxTile tile;
		tile.lid = lid;
		tile.src_x = (lid % columns) * tilewidth;
		tile.src_y = (lid / columns) * tileheight;
		tile.width = tilewidth;
		tile.height = tileheight;
		tileset.tiles[lid] = tile;
	}

	tinyxml2::XMLElement *tile_elem = tileset_elem->FirstChildElement("tile");
	while (tile_elem != nullptr) {
		unsigned int lid = tile_elem->UnsignedAttribute("id");
		if (lid > tileset.tilecount) {
			zr_log_warn("tileset %s: lid %u is out of range", tileset_name, lid);
			continue;
		}
		TmxTile &tile = tileset.tiles[lid];

		// type
		const char *type = tile_elem->Attribute("type");

		// objectgroup
		tinyxml2::XMLElement *objectgroup_elem = tile_elem->FirstChildElement("objectgroup");
		if (objectgroup_elem != nullptr) {
			parse_object_layer(&tile.collision, objectgroup_elem);
		}

		// animation
		tinyxml2::XMLElement *animation_elem = tile_elem->FirstChildElement("animation");
		if (animation_elem != nullptr) {
			tinyxml2::XMLElement *frame_elem = animation_elem->FirstChildElement("frame");
			while (frame_elem != nullptr) {
				TmxAnimationFrame &frame = tile.animation.emplace_back();
				unsigned int frame_lid = frame_elem->UnsignedAttribute("tileid");
				if (frame_lid > tileset.tilecount) {
					zr_log_warn("tileset %s: lid %u animation: lid %u is out of range", tileset_name, lid, frame_lid);
					break;
				}
				frame.lid = frame_lid;
				frame.duration = frame_elem->UnsignedAttribute("duration");
				frame_elem = frame_elem->NextSiblingElement("frame");
			}
		}

		// properties
		tile.properties = parse_properties(tile_elem->FirstChildElement("properties"));
		if (type != nullptr) {
			tile.properties.merge(get_object_type_properties(type));
		}

		tile_elem = tile_elem->NextSiblingElement("tile");
	}

	return true;
}

bool parse_map(TmxMap *map, const char *filename) {
	tinyxml2::XMLDocument tmxdoc;
	tinyxml2::XMLError err = fs_load_xml(filename, &tmxdoc);
	if (err != tinyxml2::XML_SUCCESS) {
		if (err == tinyxml2::XML_ERROR_FILE_NOT_FOUND) {
			zr_log_error("%s: could not open file", filename);
		} else {
			zr_log_error("%s: error parsing xml file: %s", filename, tmxdoc.ErrorIDToName(err));
		}
		return false;
	}

	std::string_view base_dir;
	const char *sep_ptr = strrchr(filename, '/');
	if (sep_ptr != nullptr) {
		base_dir = std::string_view(filename, sep_ptr - filename);
	} else {
		base_dir = std::string_view("");
	}

	tinyxml2::XMLElement *map_elem = tmxdoc.FirstChildElement("map");
	if (map_elem == nullptr) {
		zr_log_error("missing map");
		return false;
	}

	const char *map_orientation = map_elem->Attribute("orientation");
	const char *map_renderorder_str = map_elem->Attribute("renderorder");
	map->width = map_elem->IntAttribute("width", 0);
	map->height = map_elem->IntAttribute("height", 0);
	map->tilewidth = map_elem->IntAttribute("tilewidth");
	map->tileheight = map_elem->IntAttribute("tileheight");
	const char *map_backgroundcolor = map_elem->Attribute("backgroundcolor");

	if (map_orientation == nullptr || strcmp(map_orientation, "orthogonal")) {
		zr_log_error("the only supported map orientation is 'orthogonal'");
		return false;
	}

	if (!strcmp(map_renderorder_str, "right-down")) {
		map->renderorder = TileLayerRenderOrder::RIGHT_DOWN;
	} else if (!strcmp(map_renderorder_str, "right-up")) {
		map->renderorder = TileLayerRenderOrder::RIGHT_UP;
	} else if (!strcmp(map_renderorder_str, "left-down")) {
		map->renderorder = TileLayerRenderOrder::LEFT_DOWN;
	} else if (!strcmp(map_renderorder_str, "left-up")) {
		map->renderorder = TileLayerRenderOrder::LEFT_UP;
	} else {
		map->renderorder = TileLayerRenderOrder::RIGHT_DOWN;
		zr_log_warn("invalid renderorder '%s', defaulting to right-down", map_renderorder_str);
	}

	map->background_color = { 0x0f, 0x1f, 0x2f, 0xff };
	if (map_backgroundcolor != nullptr) {
		if (*map_backgroundcolor == '#') {
			map_backgroundcolor++;
		}
		const char *endptr;
		unsigned long hex = strtoul(map_backgroundcolor, (char **)&endptr, 16);
		map->background_color.a = endptr - map_backgroundcolor > 6 ? (hex >> 24) & 0xff : 0xff;
		map->background_color.r = (hex >> 16) & 0xff;
		map->background_color.g = (hex >> 8) & 0xff;
		map->background_color.b = hex & 0xff;
	}

	map->properties = parse_properties(map_elem->FirstChildElement("properties"));

	tinyxml2::XMLElement *tileset_elem = map_elem->FirstChildElement("tileset");
	while (tileset_elem != nullptr) {
		map->tilesets.emplace_back();
		TmxTileset &tileset = map->tilesets.back();
		parse_tileset(tileset, base_dir, tileset_elem);

		tileset_elem = tileset_elem->NextSiblingElement("tileset");
	}

	tinyxml2::XMLElement *map_child = map_elem->FirstChildElement();
	while (map_child != nullptr) {
		if (!strcmp(map_child->Value(), "layer")) {
			map->layers.emplace_back(new TmxTileLayer);
			TmxTileLayer *child_tilelayer = static_cast<TmxTileLayer *>(map->layers.back().get());
			parse_tile_layer(child_tilelayer, map_child);

		} else if (!strcmp(map_child->Value(), "objectgroup")) {
			map->layers.emplace_back(new TmxObjectLayer);
			TmxObjectLayer *child_objectlayer = static_cast<TmxObjectLayer *>(map->layers.back().get());
			parse_object_layer(child_objectlayer, map_child);

		} else if (!strcmp(map_child->Value(), "group")) {
			map->layers.emplace_back(new TmxGroupLayer);
			TmxGroupLayer *child_grouplayer = static_cast<TmxGroupLayer *>(map->layers.back().get());
			parse_group_layer(child_grouplayer, map_child);

		}

		map_child = map_child->NextSiblingElement();
	}

	return true;
}
