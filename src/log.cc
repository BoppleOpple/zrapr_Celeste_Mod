// log.cc

#include "log.hh"

#include <cstdarg>
#include <cstdio>
#include <string>
#include <unistd.h>
#include <vector>

#include "context.hh"

std::vector<std::string> cat_stack;

void zr_log(ZrLogLevel level, const char *fmt, ...) {
	va_list v;
	va_start(v, fmt);
	zr_log_v(level, fmt, v);
	va_end(v);
}
void zr_log_v(ZrLogLevel level, const char *fmt, va_list v) {
	ZrLogEntry &entry = g_client_context.log_entries.emplace_back();
	entry.level = level;

	va_list v_copy;
	va_copy(v_copy, v);
	int str_len = vsnprintf(nullptr, 0, fmt, v);
	entry.message.resize(str_len + 1);
	vsprintf(entry.message.data(), fmt, v_copy);
	va_end(v_copy);

	entry.cat = "root";
	for (const std::string &part : cat_stack) {
		entry.cat += '/';
		entry.cat += part;
	}
	entry.cat.shrink_to_fit();

	bool is_tty = isatty(fileno(stdout));
	const char *fmt_info = is_tty ? "\x1b[0m[info] [%s] %s\n" : "[info] [%s] %s\n";
	const char *fmt_warn = is_tty ? "\x1b[93m[warn] [%s] %s\x1b[0m\n" : "[warn] [%s] %s\n";
	const char *fmt_error = is_tty ? "\x1b[91m[error] [%s] %s\x1b[0m\n" : "[error] [%s] %s\n";
	const char *fmt_debug = is_tty ? "\x1b[96m[debug] [%s] %s\x1b[0m\n" : "[debug] [%s] %s\n";

	if (g_client_context.log_to_stdout) {
		switch (entry.level) {
			case ZrLogLevel::INFO:
				printf(fmt_info, entry.cat.c_str(), entry.message.c_str());
				break;
			case ZrLogLevel::WARN:
				printf(fmt_warn, entry.cat.c_str(), entry.message.c_str());
				break;
			case ZrLogLevel::ERROR:
				printf(fmt_error, entry.cat.c_str(), entry.message.c_str());
				break;
			case ZrLogLevel::DEBUG:
				printf(fmt_debug, entry.cat.c_str(), entry.message.c_str());
				break;
		}
	}

	int cutoff = g_client_context.log_entries.size() - g_client_context.log_max_entries;
	if (cutoff > 0) {
		g_client_context.log_entries.erase(g_client_context.log_entries.begin(), g_client_context.log_entries.begin() + cutoff);
	}
}

void zr_log_info_v(const char *fmt, va_list v) {
	zr_log_v(ZrLogLevel::INFO, fmt, v);
}
void zr_log_warn_v(const char *fmt, va_list v) {
	zr_log_v(ZrLogLevel::WARN, fmt, v);
}
void zr_log_error_v(const char *fmt, va_list v) {
	zr_log_v(ZrLogLevel::ERROR, fmt, v);
}
void zr_log_debug_v(const char *fmt, va_list v) {
	zr_log_v(ZrLogLevel::DEBUG, fmt, v);
}

void zr_log_info(const char *fmt, ...) {
	va_list v;
	va_start(v, fmt);
	zr_log_v(ZrLogLevel::INFO, fmt, v);
	va_end(v);
}
void zr_log_warn(const char *fmt, ...) {
	va_list v;
	va_start(v, fmt);
	zr_log_v(ZrLogLevel::WARN, fmt, v);
	va_end(v);
}
void zr_log_error(const char *fmt, ...) {
	va_list v;
	va_start(v, fmt);
	zr_log_v(ZrLogLevel::ERROR, fmt, v);
	va_end(v);
}
void zr_log_debug(const char *fmt, ...) {
	va_list v;
	va_start(v, fmt);
	zr_log_v(ZrLogLevel::DEBUG, fmt, v);
	va_end(v);
}

void zr_log_cat_push(const char *cat) {
	cat_stack.emplace_back(cat);
}
void zr_log_cat_pop() {
	cat_stack.pop_back();
}
