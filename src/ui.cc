// ui.cc

#include "ui.hh"

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <string_view>
#include <string>
#include <vector>

#include <entt/entt.hpp>
#include <imgui/imgui.h>
#include <imgui/imgui_stdlib.h>
#include <physfs.h>
#include <SDL.h>
#include <stb_image.h>

#include "comp.hh"
#include "configfile.hh"
#include "context.hh"
#include "entities.hh"
#include "fs.hh"
#include "gfx.hh"
#include "input.hh"
#include "log.hh"
#include "map.hh"
#include "playerchar.hh"
#include "util/input_constants.hh"
#include "util/sleep.hh"
#include "window.hh"

#if defined(__APPLE__)
#include "util/macosx_open_dir.h"
#endif

#include <zrapr_build_info.h>

void help_marker(const char *tooltip) {
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered()) {
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 30.0f);
		ImGui::TextUnformatted(tooltip);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}

void ui_main_menu_bar(bool *running) {
	static char buf[256];
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	if (ImGui::BeginMainMenuBar()) {
#if ZRAPR_HAVE_COMMIT
		ImGui::BeginMenu("zrapr " ZRAPR_COMMIT_DESCRIBE " (" ZRAPR_BUILD_TYPE ")", false);
#else
		ImGui::BeginMenu("zrapr (" ZRAPR_BUILD_TYPE ")", false);
#endif

		// Game
		if (ImGui::BeginMenu("Game")) {
			if (ImGui::MenuItem("generate tiled-project file")) {
				create_tiled_project();
			}
			ImGui::Separator();

#if SDL_VERSION_ATLEAST(2, 0, 14)

// different operating systems call their file manager different things
# if defined(_WIN32) || defined(__CYGWIN__)
#  define FILE_MANAGER "file explorer"
# elif defined(__APPLE__)
#  define FILE_MANAGER "finder"
# else
#  define FILE_MANAGER "file manager"
# endif

			if (ImGui::MenuItem("open pref dir in " FILE_MANAGER)) {
# if defined(__APPLE__)
				// apple has to be special
				macosx_open_dir(fs_get_pref_dir());
# else
				// for normal platforms
				SDL_OpenURL(fs_get_pref_dir());
# endif
			}

# undef FILE_MANAGER
#endif

			if (ImGui::MenuItem("save config to disk")) {
				save_config_to_disk();
			};
			ImGui::Separator();

			if (ImGui::MenuItem("exit")) {
				*running = false;
			}
			ImGui::EndMenu();
		}

		// Tools
		if (ImGui::BeginMenu("Tools")) {
			if (ImGui::MenuItem("imgui demo", nullptr, &g_client_context.show_imgui_demo)) config_mark_dirty();
			ImGui::Separator();
			if (ImGui::MenuItem("log", nullptr, &g_client_context.show_log)) config_mark_dirty();
			ImGui::Separator();
			if (ImGui::MenuItem("ecs view", nullptr, &g_client_context.show_ecs_view)) config_mark_dirty();
			ImGui::Separator();
			if (ImGui::MenuItem("collision view control", nullptr, &g_client_context.show_collision_view_control)) config_mark_dirty();
			ImGui::Separator();
			if (ImGui::MenuItem("level select", nullptr, &g_client_context.show_level_select)) config_mark_dirty();
			if (ImGui::MenuItem("playerchar select", nullptr, &g_client_context.show_playerchar_select)) config_mark_dirty();
			ImGui::Separator();
			if (ImGui::MenuItem("input config", nullptr, &g_client_context.show_input_control)) config_mark_dirty();
			ImGui::EndMenu();
		}

		// Resolution
		if (ImGui::BeginMenu("Resolution")) {
			static int temp_window_width;
			static int temp_window_height;
			bool resolution_dirty = false;
			bool need_refresh_window = false;
			if (ImGui::IsWindowAppearing()) {
				temp_window_width = g_client_context.window_width;
				temp_window_height = g_client_context.window_height;
			}

			if (ImGui::Button("default (1280x720)")) {
				temp_window_width = 1280;
				temp_window_height = 720;
				resolution_dirty |= true;
			}

			resolution_dirty |= ImGui::InputScalar("resolution width", ImGuiDataType_S32, &temp_window_width, nullptr, nullptr, "%d", ImGuiInputTextFlags_EnterReturnsTrue);
			resolution_dirty |= ImGui::InputScalar("resolution height", ImGuiDataType_S32, &temp_window_height, nullptr, nullptr, "%d", ImGuiInputTextFlags_EnterReturnsTrue);
			resolution_dirty |= ImGui::Button("apply");

			if (resolution_dirty) {
				g_client_context.window_fullscreen = false;
				g_client_context.window_width = temp_window_width;
				g_client_context.window_height = temp_window_height;
				need_refresh_window |= true;
				config_mark_dirty();
			}

			ImGui::Separator();
			need_refresh_window |= ImGui::Checkbox("fullscreen (borderless)", &g_client_context.window_fullscreen);

			if (need_refresh_window) {
				push_window_state();
			}

			ImGui::EndMenu();
		}

		// Framerate
		if (ImGui::BeginMenu("Framerate")) {
			ImGui::Text("target frames per second");
			ImGui::SameLine();
			help_marker("target frames per second");
			if (ImGui::SliderFloat("##target_frames_per_second", &g_game_context.target_frames_per_second, 50.0f, 150.0f, "%.0f", ImGuiSliderFlags_AlwaysClamp)) {
				config_mark_dirty();
			}

			ImGui::Text("time factor multiplier");
			ImGui::SameLine();
			help_marker("aka \"delta time\" multiplier");
			float time_factor_multiplier_percent = g_game_context.time_factor_multiplier * 100.0f;
			if (ImGui::SliderFloat("##time_factor_multiplier", &time_factor_multiplier_percent, 0.0f, 200.0f, "%.1f%%", ImGuiSliderFlags_AlwaysClamp)) {
				g_game_context.time_factor_multiplier = time_factor_multiplier_percent / 100.0f;
				//config_mark_dirty();
			}

			ImGui::Separator();
			ImGui::Text("sleep method");
			ImGui::SameLine();
			help_marker("API used to sleep between frames");

			ImGui::Indent(15.0f);
			bool sleep_method_dirty = false;
			sleep_method_dirty |= ImGui::RadioButton("SDL_Delay", (int *)&g_sleep_method, SDL_DELAY);
			sleep_method_dirty |= ImGui::RadioButton("C++11 <thread>", (int *)&g_sleep_method, CXX11_THREAD);
#ifdef __GNUC__
			sleep_method_dirty |= ImGui::RadioButton("nanosleep(3)", (int *)&g_sleep_method, POSIX_NANOSLEEP);
#endif
			ImGui::Unindent(15.0f);

			if (sleep_method_dirty) {
				config_mark_dirty();
			}

			ImGui::Separator();
			ImGui::Text("sleep threshold");
			ImGui::SameLine();
			help_marker("the initial call to the chosen sleep API will be this much of the desired wait time. any remaining time will be spent spinning.");
			float sleep_threshold_percent = g_sleep_threshold * 100.0f;
			if (ImGui::SliderFloat("##sleep_threshold", &sleep_threshold_percent, 0.0f, 150.0f, "%.1f%%", ImGuiSliderFlags_AlwaysClamp)) {
				g_sleep_threshold = sleep_threshold_percent / 100.0f;
				config_mark_dirty();
			}
			if (ImGui::Checkbox("sleep correction", &g_sleep_correction)) {
				config_mark_dirty();
			}

			ImGui::Separator();
			if (ImGui::Checkbox("vsync", &g_client_context.window_vsync)) {
				config_mark_dirty();
			}
			ImGui::SameLine();
			help_marker("the step rate is tied to the refresh rate and all sleeping settings are "
				"disregarded.\nthe time factor (\"delta time\") is still determined by the target "
				"framerate. set the target framerate to your refresh rate for best results");

			ImGui::EndMenu();
		}

		// Renderer
		if (ImGui::BeginMenu("Renderer")) {
			if (ImGui::InputFloat("view scale", &g_client_context.view_scale, 1, .1)) {
				config_mark_dirty();
			}
			ImGui::EndMenu();
		}

		// framerate display
		snprintf(buf, sizeof buf, "fps: %2.2f###fps", g_client_context.imgui_io->Framerate);
		ImGui::BeginMenu(buf, false);

		// frametime display
		snprintf(buf, sizeof buf, "frametime: %.3fms###frametime", g_client_context.frame_duration_seconds * 1000.0f);
		ImGui::BeginMenu(buf, false);

		ImGui::EndMainMenuBar();
	}
	ImGui::PopStyleVar();
}

inline ImVec4 sdl_color_to_imgui(SDL_Color sdlcolor) {
	return ImVec4(sdlcolor.r / 255.0f, sdlcolor.g / 255.0f, sdlcolor.b / 255.0f, sdlcolor.a / 255.0f);
}

void collision_view_control_part(const char *name, bool *show, SDL_Color *color, SDL_Color *bgcolor) {
	bool config_dirty = false;

	ImGui::PushID(show);
	ImGui::TableNextRow();
	ImGui::TableNextColumn();

	ImVec4 line_color = sdl_color_to_imgui(*color);

	ImGui::PushID(0);
	float imcolor_line_editing[4] = { line_color.x, line_color.y, line_color.z, line_color.w };
	if (ImGui::ColorEdit4("", imcolor_line_editing, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoBorder)) {
		color->r = imcolor_line_editing[0] * 0xff;
		color->g = imcolor_line_editing[1] * 0xff;
		color->b = imcolor_line_editing[2] * 0xff;
		color->a = imcolor_line_editing[3] * 0xff;
		config_dirty = true;
	}
	ImGui::PopID();

	if (bgcolor != nullptr) {
		ImVec4 bg_color = sdl_color_to_imgui(*bgcolor);

		ImGui::SameLine();
		ImGui::PushID(1);
		float imcolor_bg_editing[4] = { bg_color.x, bg_color.y, bg_color.z, bg_color.w };
		if (ImGui::ColorEdit4("", imcolor_bg_editing, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoBorder)) {
			bgcolor->r = imcolor_bg_editing[0] * 0xff;
			bgcolor->g = imcolor_bg_editing[1] * 0xff;
			bgcolor->b = imcolor_bg_editing[2] * 0xff;
			bgcolor->a = imcolor_bg_editing[3] * 0xff;
			config_dirty = true;
		}
		ImGui::PopID();
	}

	ImGui::TableNextColumn();
	config_dirty |= ImGui::Checkbox(name, show);

	ImGui::PopID();

	if (config_dirty) {
		config_mark_dirty();
	}
}

void ui_collision_view_control(bool *open_ptr) {
	if (ImGui::Begin("collision view control", open_ptr)) {
		bool all_on = ImGui::Button("all on");
		ImGui::SameLine();
		bool all_off = ImGui::Button("all off");

		if (all_on) {
			g_client_context.collision_view_control.show_static_floor           = true;
			g_client_context.collision_view_control.show_static_wall_left       = true;
			g_client_context.collision_view_control.show_static_wall_right      = true;
			g_client_context.collision_view_control.show_static_ceiling         = true;
			g_client_context.collision_view_control.show_body_main              = true;
			g_client_context.collision_view_control.show_body_floor_snap_up     = true;
			g_client_context.collision_view_control.show_body_floor_snap_down   = true;
			g_client_context.collision_view_control.show_body_wall_snap_left    = true;
			g_client_context.collision_view_control.show_body_wall_snap_right   = true;
			g_client_context.collision_view_control.show_body_ceiling_snap_down = true;
		} else if (all_off) {
			g_client_context.collision_view_control.show_static_floor           = false;
			g_client_context.collision_view_control.show_static_wall_left       = false;
			g_client_context.collision_view_control.show_static_wall_right      = false;
			g_client_context.collision_view_control.show_static_ceiling         = false;
			g_client_context.collision_view_control.show_body_main              = false;
			g_client_context.collision_view_control.show_body_floor_snap_up     = false;
			g_client_context.collision_view_control.show_body_floor_snap_down   = false;
			g_client_context.collision_view_control.show_body_wall_snap_left    = false;
			g_client_context.collision_view_control.show_body_wall_snap_right   = false;
			g_client_context.collision_view_control.show_body_ceiling_snap_down = false;
		}

		bool is_static_collision_on =
			g_client_context.collision_view_control.show_static_floor &&
			g_client_context.collision_view_control.show_static_wall_left &&
			g_client_context.collision_view_control.show_static_wall_right &&
			g_client_context.collision_view_control.show_static_ceiling;

		if (ImGui::Checkbox("static collision", &is_static_collision_on)) {
			g_client_context.collision_view_control.show_static_floor = is_static_collision_on;
			g_client_context.collision_view_control.show_static_wall_left = is_static_collision_on;
			g_client_context.collision_view_control.show_static_wall_right = is_static_collision_on;
			g_client_context.collision_view_control.show_static_ceiling = is_static_collision_on;
		}

		bool changed_body_colision = ImGui::Checkbox("body collision", &g_client_context.collision_view_control.show_body_main);
		ImGui::SameLine();
		bool changed_extra = ImGui::Checkbox("extra", &g_client_context.collision_view_control.setting_extra_body_collision);

		if (changed_body_colision || changed_extra) {
			bool v = g_client_context.collision_view_control.show_body_main && g_client_context.collision_view_control.setting_extra_body_collision;
			g_client_context.collision_view_control.show_body_floor_snap_up = v;
			g_client_context.collision_view_control.show_body_floor_snap_down = v;
			g_client_context.collision_view_control.show_body_wall_snap_left = v;
			g_client_context.collision_view_control.show_body_wall_snap_right = v;
			g_client_context.collision_view_control.show_body_ceiling_snap_down = v;
		}

		if (ImGui::CollapsingHeader("detailed settings")) {

			if (ImGui::BeginTable("table", 2)) {
				ImGui::TableSetupColumn("colors", ImGuiTableColumnFlags_WidthFixed);
				ImGui::TableSetupColumn("show", ImGuiTableColumnFlags_WidthStretch);

				collision_view_control_part("static_floor",
					&g_client_context.collision_view_control.show_static_floor,
					&g_client_context.collision_view_colors.static_floor,
					nullptr);
				collision_view_control_part("static_wall_left",
					&g_client_context.collision_view_control.show_static_wall_left,
					&g_client_context.collision_view_colors.static_wall_left,
					nullptr);
				collision_view_control_part("static_wall_right",
					&g_client_context.collision_view_control.show_static_wall_right,
					&g_client_context.collision_view_colors.static_wall_right,
					nullptr);
				collision_view_control_part("static_ceiling",
					&g_client_context.collision_view_control.show_static_ceiling,
					&g_client_context.collision_view_colors.static_ceiling,
					nullptr);
				collision_view_control_part("body_main",
					&g_client_context.collision_view_control.show_body_main,
					&g_client_context.collision_view_colors.body_main,
					&g_client_context.collision_view_colors.body_main_bg);
				collision_view_control_part("body_floor_snap_up",
					&g_client_context.collision_view_control.show_body_floor_snap_up,
					&g_client_context.collision_view_colors.body_floor_snap_up,
					&g_client_context.collision_view_colors.body_floor_snap_up_bg);
				collision_view_control_part("body_floor_snap_down",
					&g_client_context.collision_view_control.show_body_floor_snap_down,
					&g_client_context.collision_view_colors.body_floor_snap_down,
					&g_client_context.collision_view_colors.body_floor_snap_down_bg);
				collision_view_control_part("body_wall_snap_left",
					&g_client_context.collision_view_control.show_body_wall_snap_left,
					&g_client_context.collision_view_colors.body_wall_snap_left,
					&g_client_context.collision_view_colors.body_wall_snap_left_bg);
				collision_view_control_part("body_wall_snap_right",
					&g_client_context.collision_view_control.show_body_wall_snap_right,
					&g_client_context.collision_view_colors.body_wall_snap_right,
					&g_client_context.collision_view_colors.body_wall_snap_right_bg);
				collision_view_control_part("body_ceiling_snap_down",
					&g_client_context.collision_view_control.show_body_ceiling_snap_down,
					&g_client_context.collision_view_colors.body_ceiling_snap_down,
					&g_client_context.collision_view_colors.body_ceiling_snap_down_bg);

				ImGui::EndTable();
			}
		}
	}
	ImGui::End();
}

void input_control_part(const char *name, InputSpec &spec) {
	static const ImVec4 white(1.0f, 1.0f, 1.0f, 1.0f);
	static const ImVec4 green(0.0f, 0.75f, 1.0f, 1.0f);

	ImGui::PushID(name);

	ImGui::TableNextRow();
	ImGui::TableNextColumn();
	ImGui::TextColored(global_input_is_held(spec) ? green : white, "%s", name);

	ImGui::TableNextColumn();
	unsigned int im_id = 0;
	for (auto it = spec.entries.begin(); it != spec.entries.end();) {
		ImGui::PushID(im_id++);

		InputSpecEntry &entry = *it;
		std::string button_label;
		if (entry.waiting_for_bind) {
			button_label = "...";
		} else {
			button_label = inputspecentry_to_string(entry);
		}

		if (ImGui::Button(button_label.c_str(), ImVec2(120.0f, 0.0f))) {
			input_set_waiting_for_bind(entry);
		}

		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.14f, 0.30f, 0.51f, 0.40f));
		ImGui::SameLine(0.0f, 2.0f);
		if (ImGui::Button("-")) {
			spec.entries.erase(it);
		} else {
			it++;
		}
		ImGui::PopStyleColor(1);

		ImGui::SameLine();
		ImGui::PopID();
	}

	if (ImGui::Button("+")) {
		InputSpecEntry new_entry;
		input_set_waiting_for_bind(new_entry);
		spec.entries.push_back(new_entry);
	}

	ImGui::PopID();
}

void ui_input_control(bool *open_ptr) {
	const float name_col_width = 130.0f;
	if (ImGui::Begin("input config", open_ptr)) {
		int i = 0;
		if (ImGui::BeginTabBar("profiles")) {
			for (InputProfile &profile : g_client_context.input_control.profiles) {
				char buf[32];
				snprintf(buf, sizeof buf, "profile %d", i++);
				if (ImGui::BeginTabItem(buf, nullptr)) {
					if (ImGui::BeginTable("bindings", 2)) {
						ImGui::TableSetupColumn("name", ImGuiTableColumnFlags_WidthFixed, name_col_width);
						ImGui::TableSetupColumn("spec", ImGuiTableColumnFlags_WidthFixed);

						for (auto &[name, spec] : input_get_specs_profiled_ordered()) {
							input_control_part(name.c_str(), &profile->*spec);
						}

						ImGui::EndTable();
					}
					ImGui::EndTabItem();
				}
			}
			ImGui::EndTabBar();
		}
		ImGui::Separator();
		if (ImGui::BeginTable("bindings", 2)) {
			ImGui::TableSetupColumn("name", ImGuiTableColumnFlags_WidthFixed, name_col_width);
			ImGui::TableSetupColumn("spec", ImGuiTableColumnFlags_WidthFixed);

			for (auto &[name, spec] : input_get_specs_ordered()) {
				input_control_part(name.c_str(), *spec);
			}

			ImGui::EndTable();
		}
	}

	if (ImGui::CollapsingHeader("controllers")) {
		const char *inst_id_label = "inst id:";
		float inst_id_label_width = ImGui::CalcTextSize(inst_id_label).x;
		if (ImGui::BeginTable("controllers", 2, ImGuiTableFlags_BordersInner)) {
			ImGui::TableSetupColumn("id", ImGuiTableColumnFlags_WidthFixed, inst_id_label_width);
			ImGui::TableSetupColumn("info", ImGuiTableColumnFlags_WidthFixed);
			for (const auto &[jsid, controller] : input_get_controllers()) {
				ImGui::PushID(jsid);

				char jsid_buf[12];
				SDL_itoa(jsid, jsid_buf, 10);
				float jsid_buf_width = ImGui::CalcTextSize(jsid_buf).x;

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::TextUnformatted(inst_id_label);
				ImGui::SetCursorPosX(ImGui::GetCursorPosX() + (inst_id_label_width - jsid_buf_width) / 2.0f);
				ImGui::TextUnformatted(jsid_buf);

				ImGui::TableNextColumn();

#if SDL_VERSION_ATLEAST(2, 0, 12)
				SDL_GameControllerType type = SDL_GameControllerGetType(controller);
				ImGui::Text("type: %s", game_controller_type_str(type));
#endif
				ImGui::Text("name: %s\n", SDL_GameControllerName(controller));
				ImGui::Text("device index: %d", input_get_device_index(jsid));
				char guid[36];
				SDL_JoystickGetGUIDString(SDL_JoystickGetGUID(SDL_GameControllerGetJoystick(controller)), guid, sizeof guid);
				ImGui::Text("guid: %s\n", guid);

				ImGui::PopID();
			}
			ImGui::EndTable();
		}
	}

	ImGui::End();
}

bool ends_with(std::string_view strview, std::string_view substrview) {
	return strview.length() >= substrview.length() && !strview.compare(strview.length() - substrview.length(), substrview.length(), substrview);
}

std::vector<std::string> level_select_files;

void populate_level_select_files(const char *path) {
	for (const DirEntry &dirent : fs_enumerate(path)) {
		std::string subpath;
		subpath.reserve(256);
		if (*path != '\0') {
			subpath += path;
			subpath += '/';
		}
		subpath += dirent.name;
		if (dirent.is_dir()) {
			populate_level_select_files(subpath.c_str());
		} else if (ends_with(dirent.name, ".tmx")) {
			level_select_files.emplace_back(subpath);
		}
	}
}

void ui_level_select(bool *open_ptr, entt::registry &reg) {
	ImGui::SetNextWindowSize(ImVec2(400, 500), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("level select", open_ptr)) {
		bool refresh = ImGui::Button("refresh");
		if (ImGui::IsWindowAppearing() || refresh) {
			level_select_files.clear();
			populate_level_select_files("");
		}
		ImGui::SameLine();
		if (ImGui::Button("reload current level")) {
			load_level(g_game_context.level_path.c_str(), reg);
		}

		if (ImGui::Checkbox("start speedrun", &g_game_context.user_speedrun_mode)) {
			config_mark_dirty();
		}

		ImGui::SameLine();
		if (g_game_context.user_speedrun_mode) ImGui::BeginDisabled();
		if (ImGui::Checkbox("preserve position", &g_game_context.preserve_position_on_level_load)) {
			config_mark_dirty();
		}
		if (g_game_context.user_speedrun_mode) ImGui::EndDisabled();

		ImGui::BeginChild("scrolling");
		if (ImGui::BeginTable("levels", 2, ImGuiTableFlags_BordersInnerH)) {
			ImGui::TableSetupColumn("load", ImGuiTableColumnFlags_WidthFixed);
			ImGui::TableSetupColumn("file", ImGuiTableColumnFlags_WidthStretch);

			unsigned int i = 0;
			for (std::string filename : level_select_files) {
				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				char buf[32];
				snprintf(buf, sizeof buf, "load##%u", i++);
				if (ImGui::Button(buf)) {
					load_level(filename.c_str(), reg);
				}

				ImGui::TableNextColumn();
				bool is_current_level = !filename.compare(g_game_context.level_path);
				if (is_current_level) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.75f, 1.0f, 1.0f));
				ImGui::TextUnformatted(filename.c_str());
				if (is_current_level) ImGui::PopStyleColor();
			}

			ImGui::EndTable();
		}
		ImGui::EndChild();
	}
	ImGui::End();
}

struct PlayerCharSelectEntry {
	std::string path;
	ZrTexture *icon;
};

struct PlayerCharTreeEntry {
	std::string path;
	int begin; // inclusive
	int end; // exclusive
	bool open;
};

std::vector<PlayerCharSelectEntry> playerchar_select_entries;
std::vector<PlayerCharTreeEntry> playerchar_tree_entries;

void populate_playerchar_select_entries(const char *path) {
	for (const DirEntry &dirent : fs_enumerate(path)) {
		std::string subpath;
		subpath.reserve(256);
		subpath += path;
		subpath += '/';
		subpath += dirent.name;
		if (dirent.is_dir()) {
			size_t tree_entry_idx = playerchar_tree_entries.size();
			PlayerCharTreeEntry &tree_entry = playerchar_tree_entries.emplace_back();
			tree_entry.path = subpath;
			tree_entry.begin = playerchar_select_entries.size();

			populate_playerchar_select_entries(subpath.c_str());

			// tree_entry is not valid anymore
			playerchar_tree_entries.at(tree_entry_idx).end = playerchar_select_entries.size();
		} else if (ends_with(dirent.name, ".png")) {
			PlayerCharSelectEntry &pc_entry = playerchar_select_entries.emplace_back();
			pc_entry.path = subpath;
		}
	}
}

void playerchar_select_separator() {
	ImDrawList *draw_list = ImGui::GetWindowDrawList();
	float y = ImGui::GetCursorScreenPos().y - 2.0f;
	float x1 = ImGui::GetCursorScreenPos().x;
	float x2 = x1 + ImGui::GetWindowSize().x;
	draw_list->AddLine(ImVec2(x1, y), ImVec2(x2, y), ImGui::GetColorU32(ImGuiCol_Separator));
}

void save_expanded_folders() {
	g_client_context.playerchar_select_expanded_folders.clear();
	for (const PlayerCharTreeEntry &tree_entry : playerchar_tree_entries) {
		if (tree_entry.open) {
			uint32_t hash = entt::hashed_string::value(tree_entry.path.c_str());
			g_client_context.playerchar_select_expanded_folders.emplace(hash);
		}
	}
	config_mark_dirty();
}

void ui_playerchar_select(bool *open_ptr, [[maybe_unused]] entt::registry &reg) {
	ImGui::SetNextWindowSize(ImVec2(350, 400), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("playerchar select", open_ptr)) {
		bool refresh = ImGui::Button("refresh");

		ImGui::SameLine();
		if (ImGui::Button("reload current playerchar")) {
			load_and_set_playerchar(g_game_context.playerchar_path.c_str());
		}

		ImGui::SameLine();
		char buf[48];
		snprintf(buf, sizeof buf, "search paths (%u)###search paths", (unsigned int)g_game_context.playerchar_search_paths.size());
		if (ImGui::CollapsingHeader(buf)) {
			constexpr float vsep_width = 7.0f;
			constexpr float indent_width = 18.0f;

			ImVec2 pos_top = ImGui::GetCursorScreenPos();
			ImGui::SetCursorPosY(ImGui::GetCursorPosY() + ImGui::GetFontSize());
			ImGui::Indent(indent_width + vsep_width);
			ImVec2 item_spacing = ImGui::GetStyle().ItemSpacing;
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(item_spacing.x / 2.0f, item_spacing.y));

			size_t i = 0;
			auto it = g_game_context.playerchar_search_paths.begin();
			auto end = g_game_context.playerchar_search_paths.end();
			unsigned int num_paths = g_game_context.playerchar_search_paths.size();
			for (; it != end; it++) {
				ImGui::PushID(i++);
				if (ImGui::InputText("", &*it, ImGuiInputTextFlags_EnterReturnsTrue)) {
					config_mark_dirty();
					refresh = true;
				}

				ImGui::SameLine();
				if (ImGui::Button("+")) {
					g_game_context.playerchar_search_paths.emplace(it + 1, "");
					config_mark_dirty();
				}

				if (num_paths > 1) {
					ImGui::SameLine();
					if (ImGui::Button("-")) {
						g_game_context.playerchar_search_paths.erase(it);
						config_mark_dirty();
						refresh = true;
					}
				}

				ImGui::PopID();
			}

			ImGui::PopStyleVar(1);
			ImGui::Unindent(indent_width + vsep_width);
			ImGui::SetCursorPosY(ImGui::GetCursorPosY() + ImGui::GetFontSize());

			ImVec2 pos_bottom = ImGui::GetCursorScreenPos();
			ImDrawList *draw_list = ImGui::GetWindowDrawList();
			draw_list->AddRectFilled(pos_top, ImVec2(pos_bottom.x + vsep_width, pos_bottom.y - item_spacing.y), ImGui::GetColorU32(ImGuiCol_Separator));
		}

		if (ImGui::IsWindowAppearing() || refresh) {
			for (const PlayerCharSelectEntry &pc_entry : playerchar_select_entries) {
				gfx_free_texture(pc_entry.icon);
			}

			playerchar_select_entries.clear();
			playerchar_tree_entries.clear();

			for (const std::string &path : g_game_context.playerchar_search_paths) {
				if (!path.empty()) {
					size_t tree_entry_idx = playerchar_tree_entries.size();
					PlayerCharTreeEntry &tree_entry = playerchar_tree_entries.emplace_back();
					tree_entry.path = path;
					tree_entry.begin = playerchar_select_entries.size();

					populate_playerchar_select_entries(path.c_str());

					// tree_entry is not valid anymore
					playerchar_tree_entries.at(tree_entry_idx).end = playerchar_select_entries.size();
				}
			}

			for (PlayerCharTreeEntry &tree_entry : playerchar_tree_entries) {
				uint32_t hash = entt::hashed_string::value(tree_entry.path.c_str());
				tree_entry.open = g_client_context.playerchar_select_expanded_folders.contains(hash);
			}

			for (PlayerCharSelectEntry &pc_entry : playerchar_select_entries) {
				PlayerChar pc;
				if (load_playerchar(&pc, pc_entry.path.c_str())) {
					uint8_t *preview_bitmap = (uint8_t *)SDL_malloc(32 * 32 * 4);

					int base_x = (1.0f - (32.0f / pc.frame_width) + pc.idle[0]) * 32;
					int base_y = pc.image_height - 32;
					for (int iy = 32; iy--;) {
						uint8_t *src = pc.bitmap.data() + (iy + base_y) * (pc.image_width * 4) + (base_x * 4);
						uint8_t *dst = preview_bitmap + iy * 32 * 4;
						memcpy(dst, src, 32 * 4);
					}

					if (!pc.rightfacing) {
						for (int iy = 0; iy < 32 * 32; iy += 32) {
							for (int ix = 16; ix--;) {
								uint32_t *ptr = (uint32_t *)preview_bitmap + iy;
								std::swap(ptr[ix], ptr[31 - ix]);
							}
						}
					}

					pc_entry.icon = gfx_create_texture(32, 32, preview_bitmap);
				}
			}
		}

		ImGui::BeginChild("scrolling");

		ImVec2 frame_padding = ImGui::GetStyle().FramePadding;
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(frame_padding.x, 10.5f));

		int closed_level = 0;
		int i = 0;
		for (const PlayerCharSelectEntry &pc_entry : playerchar_select_entries) {
			for (PlayerCharTreeEntry &tree_entry : playerchar_tree_entries) {
				if (i == tree_entry.begin) {
					if (closed_level == 0) {
						ImVec2 size(32.0f, 32.0f);
						ImVec2 uv0, uv1;
						if (tree_entry.open) {
							uv0 = ImVec2(0.5f, 0.0f);
							uv1 = ImVec2(1.0f, 1.0f);
						} else {
							uv0 = ImVec2(0.0f, 0.0f);
							uv1 = ImVec2(0.5f, 1.0f);
						}
						ImGui::Image(gfx_get_texture_handle(g_client_context.folders_tex), size, uv0, uv1);
						ImGui::SameLine();
						ImGui::SameLine();
						ImGui::AlignTextToFramePadding();

						ImGui::SetNextItemOpen(tree_entry.open, ImGuiCond_Appearing);
						tree_entry.open = ImGui::TreeNodeEx(tree_entry.path.c_str(), ImGuiTreeNodeFlags_NoTreePushOnOpen);

						if (ImGui::IsItemToggledOpen()) {
							save_expanded_folders();
						}

						playerchar_select_separator();
					}
					if (!tree_entry.open) {
						closed_level++;
					}
					ImGui::Indent(14.0f);
				}
				if (i == tree_entry.end) {
					if (!tree_entry.open) {
						closed_level--;
					}
					ImGui::Unindent(14.0f);
				}
			}

			if (closed_level == 0) {
				unsigned int w, h;
				gfx_get_texture_dimensions(pc_entry.icon, &w, &h);
				ImVec2 size(32.0f, 32.0f);
				ImVec2 uv0(0.0f, 0.0f);
				ImVec2 uv1(32.0f / w, 32.0f / h);
				ImGui::Image(gfx_get_texture_handle(pc_entry.icon), size, uv0, uv1);
				ImGui::SameLine();

				const char *path = pc_entry.path.c_str();

				ImGui::PushID(path);
				if (ImGui::Button("load")) {
					load_and_set_playerchar(path);
				}
				ImGui::PopID();
				ImGui::SameLine();

				bool is_current_playerchar = !g_game_context.playerchar_path.compare(path);
				if (is_current_playerchar) ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.75f, 1.0f, 1.0f));
				ImGui::TextUnformatted(path);
				if (is_current_playerchar) ImGui::PopStyleColor();

				playerchar_select_separator();
			}

			i++;
		}

		ImGui::PopStyleVar();

		ImGui::EndChild();
	}
	ImGui::End();
}

void ui_log(bool *open_ptr) {
	ImGui::SetNextWindowSize(ImVec2(700, 300), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("log", open_ptr)) {
		if (ImGui::Button("clear")) {
			g_client_context.log_entries.clear();
		}
		ImGui::Separator();
		ImGui::BeginChild("scrolling");
		for (const ZrLogEntry &entry : g_client_context.log_entries) {
			switch (entry.level) {
				case ZrLogLevel::INFO:
					ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "[info] [%s] %s", entry.cat.c_str(), entry.message.c_str());
					break;
				case ZrLogLevel::WARN:
					ImGui::TextColored(ImVec4(1.0f, 0.875f, 0.5f, 1.0f), "[warn] [%s] %s", entry.cat.c_str(), entry.message.c_str());
					break;
				case ZrLogLevel::ERROR:
					ImGui::TextColored(ImVec4(1.0f, 0.5f, 0.5f, 1.0f), "[error] [%s] %s", entry.cat.c_str(), entry.message.c_str());
					break;
				case ZrLogLevel::DEBUG:
					ImGui::TextColored(ImVec4(0.5f, 0.875f, 1.0f, 1.0f), "[debug] [%s] %s", entry.cat.c_str(), entry.message.c_str());
					break;
			}
		}
		if (ImGui::GetScrollY() >= ImGui::GetScrollMaxY()) {
			ImGui::SetScrollHereY(1.0f);
		}
		ImGui::EndChild();
	}
	ImGui::End();
}
