// ui_ecs_view.cc

#include "ui.hh"

#include <algorithm>
#include <string>

#include <entt/entt.hpp>
#include <imgui/imgui.h>

#include "comp.hh"
#include "configfile.hh"
#include "context.hh"
#include "map.hh"

static inline const char *bool2str(const bool b) { return b ? "true" : "false"; }

void ui_ecs_view(bool *open_ptr, entt::registry &reg) {
	std::vector<entt::entity> entities;
	entities.reserve(reg.alive());
	reg.each([&entities](const entt::entity entity) {
		entities.push_back(entity);
	});
	std::sort(entities.begin(), entities.end());

	ImGui::SetNextWindowSize(ImVec2(400, 600), ImGuiCond_FirstUseEver);
	ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, 0.0f);
	if (ImGui::Begin("ecs view", open_ptr)) {
		bool expand_all = ImGui::Button("expand all");
		ImGui::SameLine();
		bool collapse_all = ImGui::Button("collapse all");

		if (ImGui::Checkbox("show entity ids in caption", &g_client_context.caption_entity_ids)) {
			config_mark_dirty();
		}

		ImGui::BeginChild("scrolling");
		for (const entt::entity e : entities) {
			ImGui::PushID((int)e);
			if (expand_all) {
				ImGui::SetNextItemOpen(true, ImGuiCond_Always);
			} else if (collapse_all) {
				ImGui::SetNextItemOpen(false, ImGuiCond_Always);
			} else {
				ImGui::SetNextItemOpen(false, ImGuiCond_Appearing);
			}
			if (ImGui::TreeNodeEx("entity", ImGuiTreeNodeFlags_None, "Eid 0x%08x", (unsigned int)e)) {
				if (ImGui::BeginTable("components", 2, ImGuiTableFlags_Borders)) {
					ImGui::TableSetupColumn("name", ImGuiTableColumnFlags_WidthFixed);
					ImGui::TableSetupColumn("data", ImGuiTableColumnFlags_WidthStretch);
					ImGui::TableHeadersRow();

					if (reg.all_of<CompPosition>(e)) {
						CompPosition &pos = reg.get<CompPosition>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("position");
						ImGui::TableNextColumn();
						ImGui::Text("(%.3f, %.3f)", pos.x, pos.y);
					}
					if (reg.all_of<CompPhysics>(e)) {
						CompPhysics &physics = reg.get<CompPhysics>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("physics");
						ImGui::TableNextColumn();
						ImGui::Text("vel (%.3f, %.3f)", physics.dx, physics.dy);
						ImGui::Text("is_on_ground %s", bool2str(physics.is_on_ground));
						ImGui::Text("was_on_ground %s", bool2str(physics.was_on_ground));
						ImGui::Text("gravity_multiplier %.3f", physics.gravity_multiplier);
					}
					if (reg.all_of<CompPlayer>(e)) {
						CompPlayer &player = reg.get<CompPlayer>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("player");
						ImGui::TableNextColumn();
						ImGui::Text("grace_jump_timer %.1f", player.grace_jump_timer);
						ImGui::Text("grace_bhop_timer %.1f", player.grace_bhop_timer);
						ImGui::Text("grace_jump_base_height %.3f", player.grace_jump_base_height);
						const char *hfacing_str = nullptr;
						switch (player.hfacing) {
							case CompPlayer::FacingH::LEFT:  hfacing_str = "left";  break;
							case CompPlayer::FacingH::RIGHT: hfacing_str = "right"; break;
						}
						ImGui::Text("hfacing %s", hfacing_str);
						const char *vfacing_str = nullptr;
						switch (player.vfacing) {
							case CompPlayer::FacingV::NEUTRAL: vfacing_str = "neutral"; break;
							case CompPlayer::FacingV::UP:      vfacing_str = "up";      break;
							case CompPlayer::FacingV::DOWN:    vfacing_str = "down";    break;
						}
						ImGui::Text("vfacing %s", vfacing_str);
						ImGui::Text("walking_time %.2f", player.walking_time);
						ImGui::Text("idle_time %.2f", player.idle_time);
						ImGui::Text("air_time %.2f", player.air_time);
						ImGui::Text("input_index %d", player.input_index);
						ImGui::Text("looking_back %s", bool2str(player.looking_back));
						ImGui::Text("leaving_door %s", bool2str(player.leaving_door));
						if (!player.keys_gids.empty()) {
							std::string keys_gids_str;
							int num_keys = player.keys_gids.size();
							int i = 0;
							for (unsigned int gid : player.keys_gids) {
								keys_gids_str += std::to_string(gid);
								if (++i != num_keys) { keys_gids_str += ", "; }
							}
							ImGui::Text("keys_gids (%s)", keys_gids_str.c_str());
						} else {
							ImGui::TextUnformatted("keys_gids (none)");
						}
					}
					if (reg.all_of<CompTransit>(e)) {
						CompTransit &transit = reg.get<CompTransit>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("transit");
						ImGui::TableNextColumn();
						ImGui::Text("time %.2f", transit.time);
						unsigned int num_points = transit.points.size();
						char buf[32];
						snprintf(buf, sizeof buf, "%u %s###points", num_points, num_points == 1 ? "point" : "points");
						if (ImGui::CollapsingHeader(buf)) {
							if (ImGui::BeginTable("points", 2, ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable)) {
								ImGui::TableSetupColumn("end time", ImGuiTableColumnFlags_WidthFixed);
								ImGui::TableSetupColumn("pos", ImGuiTableColumnFlags_WidthStretch);
								ImGui::TableHeadersRow();
								for (const TransitPoint &point : transit.points) {
									ImGui::TableNextRow();
									ImGui::TableNextColumn();
									ImGui::Text("%.2f", point.end_time);
									ImGui::TableNextColumn();
									ImGui::Text("(%.3f, %.3f)", point.x, point.y);
								}
								ImGui::EndTable();
							}
						}
						if (transit.set_velocity) {
							ImGui::Text("sets velocity (%.3f, %.3f)", transit.vel_dx, transit.vel_dy);
						} else {
							ImGui::Text("preserves velocity");
						}
					}
					if (reg.all_of<CompBodyCollision>(e)) {
						CompBodyCollision &bodycol = reg.get<CompBodyCollision>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("bodycollision");
						ImGui::TableNextColumn();
						unsigned int num_parts = bodycol.parts.size();
						char buf[32];
						snprintf(buf, sizeof buf, "%u %s###parts", num_parts, num_parts == 1 ? "part" : "parts");
						if (ImGui::CollapsingHeader(buf)) {
							if (ImGui::BeginTable("bodycollision", 2, ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable)) {
								ImGui::TableSetupColumn("p1", ImGuiTableColumnFlags_WidthStretch);
								ImGui::TableSetupColumn("p2", ImGuiTableColumnFlags_WidthStretch);
								ImGui::TableHeadersRow();
								for (BodyCollisionPart &part : bodycol.parts) {
									ImGui::TableNextRow();
									ImGui::TableNextColumn();
									ImGui::Text("(%.3f, %.3f)", part.rect.x1, part.rect.y1);
									ImGui::TableNextColumn();
									ImGui::Text("(%.3f, %.3f)", part.rect.x2, part.rect.y2);
								}
								ImGui::EndTable();
							}
						}
					}
					if (reg.all_of<CompStaticCollision>(e)) {
						CompStaticCollision &statcol = reg.get<CompStaticCollision>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("staticcollision");
						ImGui::TableNextColumn();
						unsigned int num_parts = statcol.parts.size();
						char buf[32];
						snprintf(buf, sizeof buf, "%u %s###parts", num_parts, num_parts == 1 ? "part" : "parts");
						if (ImGui::CollapsingHeader(buf)) {
							if (ImGui::BeginTable("staticcollision", 4, ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable)) {
								ImGui::TableSetupColumn("ind", ImGuiTableColumnFlags_WidthFixed);
								ImGui::TableSetupColumn("type", ImGuiTableColumnFlags_WidthFixed);
								ImGui::TableSetupColumn("pos1", ImGuiTableColumnFlags_WidthStretch);
								ImGui::TableSetupColumn("pos2", ImGuiTableColumnFlags_WidthStretch);
								ImGui::TableHeadersRow();
								int ind = 0;
								for (StaticCollisionPart &part : statcol.parts) {
									ImGui::TableNextRow();
									ImGui::TableNextColumn();
									ImGui::Text("%d", ind++);
									ImGui::TableNextColumn();
									const char *type_str = nullptr;
									switch (part.type) {
										case StaticCollisionPart::FLOOR:      type_str = "floor";     break;
										case StaticCollisionPart::WALL_LEFT:  type_str = "leftwall";  break;
										case StaticCollisionPart::WALL_RIGHT: type_str = "rightwall"; break;
										case StaticCollisionPart::CEILING:    type_str = "ceiling";   break;
										default: break; // ???
									}
									ImGui::TextUnformatted(type_str);
									ImGui::TableNextColumn();
									ImGui::Text("(%.3f, %.3f)", part.x1, part.y1);
									ImGui::TableNextColumn();
									ImGui::Text("(%.3f, %.3f)", part.x2, part.y2);
								}
								ImGui::EndTable();
							}
						}
					}
					if (reg.all_of<CompTileLayer>(e)) {
						CompTileLayer &tilelayer = reg.get<CompTileLayer>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("tilelayer");
						ImGui::TableNextColumn();
						ImGui::Text("dimensions (%d, %d)", tilelayer.width, tilelayer.height);
						ImGui::Text("tile dimensions (%d, %d)", tilelayer.tile_width, tilelayer.tile_height);
						const char *render_order_str = nullptr;
						switch (tilelayer.render_order) {
							case TileLayerRenderOrder::RIGHT_DOWN: render_order_str = "right-down"; break;
							case TileLayerRenderOrder::RIGHT_UP:   render_order_str = "right-up";   break;
							case TileLayerRenderOrder::LEFT_DOWN:  render_order_str = "left-down";  break;
							case TileLayerRenderOrder::LEFT_UP:    render_order_str = "left-up";    break;
						}
						ImGui::Text("render order %s\n", render_order_str);
					}
					if (reg.all_of<CompTile>(e)) {
						CompTile &tile = reg.get<CompTile>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("tile");
						ImGui::TableNextColumn();
						ImGui::Text("gid %u", tile.gid);
						ImGui::Text("p1 (%.2f, %.2f)", tile.rect.x1, tile.rect.y1);
						ImGui::Text("p2 (%.2f, %.2f)", tile.rect.x2, tile.rect.y2);
					}
					if (reg.all_of<CompCoinTile>(e)) {
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("cointile");
						ImGui::TableNextColumn();
						ImGui::TextDisabled("(no members)");
					}
					if (reg.all_of<CompKeyTile>(e)) {
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("keytile");
						ImGui::TableNextColumn();
						ImGui::TextDisabled("(no members)");
					}
					if (reg.all_of<CompWarpTile>(e)) {
						CompWarpTile &warptile = reg.get<CompWarpTile>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("warptile");
						ImGui::TableNextColumn();
						if (warptile.warp_to_spawn) {
							ImGui::TextUnformatted("warps to spawn");
						} else {
							unsigned int num_points = warptile.points.size();
							char buf[32];
							snprintf(buf, sizeof buf, "%u %s###points", num_points, num_points == 1 ? "point" : "points");
							if (ImGui::CollapsingHeader(buf)) {
								if (ImGui::BeginTable("points", 2, ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable)) {
									ImGui::TableSetupColumn("end time", ImGuiTableColumnFlags_WidthFixed);
									ImGui::TableSetupColumn("pos", ImGuiTableColumnFlags_WidthStretch);
									ImGui::TableHeadersRow();
									for (const TransitPoint &point : warptile.points) {
										ImGui::TableNextRow();
										ImGui::TableNextColumn();
										ImGui::Text("%.2f", point.end_time);
										ImGui::TableNextColumn();
										ImGui::Text("(%.3f, %.3f)", point.x, point.y);
									}
									ImGui::EndTable();
								}
							}
						}
						ImGui::Text("on_touch %d", warptile.on_touch);
						if (warptile.set_velocity) {
							ImGui::Text("sets velocity (%.3f, %.3f)", warptile.vel_dx, warptile.vel_dy);
						} else {
							ImGui::TextUnformatted("preserves velocity");
						}
						ImGui::Text("key_gid %u", warptile.key_gid);
						ImGui::Text("unlock_tile_gid %u", warptile.unlock_tile_gid);
					}
					if (reg.all_of<CompOnLayer>(e)) {
						CompOnLayer &onlayer = reg.get<CompOnLayer>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("onlayer");
						ImGui::TableNextColumn();
						ImGui::Text("index %u", onlayer.index);
					}
					if (reg.all_of<CompObjectLayerObject>(e)) {
						CompObjectLayerObject &objectlayerobject = reg.get<CompObjectLayerObject>(e);
						ImGui::TableNextRow();
						ImGui::TableNextColumn();
						ImGui::TextUnformatted("objectlayerobject");
						ImGui::TableNextColumn();
						const char *draworder_str = nullptr;
						switch (objectlayerobject.draworder) {
							case ObjectLayerDrawOrder::TOPDOWN: draworder_str = "topdown"; break;
							case ObjectLayerDrawOrder::INDEX:   draworder_str = "index";   break;
						}
						ImGui::Text("draworder %s", draworder_str);
						ImGui::Text("index %u", objectlayerobject.index);
					}

					ImGui::EndTable();
				}
				ImGui::TreePop();
			}
			ImGui::PopID();
		}
		ImGui::EndChild();
	}
	ImGui::End();
	ImGui::PopStyleVar();
}
