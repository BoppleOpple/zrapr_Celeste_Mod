// gfx.cc

#include "window.hh"

#include <cstdint>

#include <stb_image.h>

#include "configfile.hh"
#include "context.hh"
#include "gfx.hh"

/* we changed the window state. push to the system, and update the renderer */
void push_window_state() {
	if (g_client_context.window_fullscreen) {
		SDL_SetWindowFullscreen(g_client_context.window, SDL_WINDOW_FULLSCREEN_DESKTOP);
		SDL_GetWindowSize(g_client_context.window, &g_client_context.window_width, &g_client_context.window_height);
	} else {
		SDL_SetWindowFullscreen(g_client_context.window, 0);
		SDL_RestoreWindow(g_client_context.window);
		SDL_SetWindowSize(g_client_context.window, g_client_context.window_width, g_client_context.window_height);
	}
	gfx_set_backbuffer_size(g_client_context.window_width, g_client_context.window_height);
}

/* the user changed the window state. update internal state accordingly, and update the renderer */
void pull_window_state() {
	g_client_context.window_fullscreen = SDL_GetWindowFlags(g_client_context.window) & SDL_WINDOW_FULLSCREEN;
	SDL_GetWindowSize(g_client_context.window, &g_client_context.window_width, &g_client_context.window_height);
	gfx_set_backbuffer_size(g_client_context.window_width, g_client_context.window_height);
	g_client_context.window_maximized = SDL_GetWindowFlags(g_client_context.window) & SDL_WINDOW_MAXIMIZED;
	config_mark_dirty();
}

void init_window_icon() {
	// od -v -An -tx8 icon.png
	static const uint64_t icon_png[] = {
		0x0a1a0a0d474e5089ull, 0x524448490d000000ull, 0x4000000040000000ull, 0x7169aa0000000608ull,
		0x49427304000000deull, 0x64087c0808080854ull, 0x4144495f01000088ull, 0x026abdd8edda7854ull,
		0x80c416f761861441ull, 0x8a5d82365484d920ull, 0xc043692348121608ull, 0x6a7f04b98b532934ull,
		0x21026c5690851b4bull, 0xc62563481b9242c5ull, 0x8a73ad885bc12a90ull, 0x75fbe2ecebb30c99ull,
		0x99ec314f3bab3387ull, 0x0000000009e7b720ull, 0xfdfd76dfe10a94abull, 0x0fff23b8fd740abbull,
		0x0de9201648000000ull, 0x0edc0000000206f9ull, 0x3b70438000000041ull, 0xb42eaf775281bdd7ull,
		0x6b5b7d4a96bf9febull, 0x87e7d4a675e8f52aull, 0x0fd4a4f8ff7d4ac7ull, 0x100000000815b69dull,
		0x8f0fdb43f3eac02full, 0x007d979f6deb562aull, 0x2000000000000000ull, 0xfad581e1eb32003dull,
		0x00000010158fddfcull, 0xf6d4b9ba2861ee90ull, 0x1069f99fb6a9e3ebull, 0x63220ae00e520000ull,
		0xfa928174c530e0baull, 0xef8018c80000615bull, 0xc4000000020d270dull, 0x23876c443f23e1fbull,
		0x000082464861c8d7ull, 0x01043ef802c90000ull, 0x1941b90037000000ull, 0xeb1cace3501361b9ull,
		0x001f7ab736f2f430ull, 0x3be430e9d8200000ull, 0x8038c8000041a7deull, 0xd395fa513f526409ull,
		0xdd398bf6ae17a777ull, 0x000000168ad29befull, 0x5f685a183601ffc0ull, 0x0000000049bdbc3dull,
		0x826042ae444e4549ull
	};
	// du -b icon.png
	static const int icon_png_len = 424;

	int width, height;
	void *icon_bitmap = stbi_load_from_memory((const stbi_uc *)icon_png, icon_png_len, &width, &height, nullptr, 4);
	SDL_Surface *icon_surface = SDL_CreateRGBSurfaceWithFormatFrom(icon_bitmap, width, height, -1, width * 4, SDL_PIXELFORMAT_RGBA32);
	SDL_SetWindowIcon(g_client_context.window, icon_surface);
	SDL_FreeSurface(icon_surface);
	stbi_image_free(icon_bitmap);
}
