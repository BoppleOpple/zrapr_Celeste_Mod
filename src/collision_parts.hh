// collision_parts.hh

#pragma once

#include <vector>

#include "geom.hh"

struct BodyCollisionPart {
	NEWRECT rect;
	bool solid; /* interact with static collision */
};

struct StaticCollisionPart {
	enum Type {
		FLOOR,      /* Floor */
		WALL_LEFT,  /* Left-outward wall */
		WALL_RIGHT, /* Right-outward wall */
		CEILING     /* Ceiling */
	};

	Type type;
	float x1, y1;
	float x2, y2;
};

