// gfx.cc

#include "gfx.hh"

#include <cstdarg>
#include <set>

#include <glad/glad.h>
#include <physfs.h>
#include <SDL.h>

#include "anno.hh"
#include "context.hh"
#include "log.hh"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_STDIO
#define STBI_ONLY_PNG
#define STBI_MAX_DIMENSIONS 32768
#define STBI_MALLOC SDL_malloc
#define STBI_REALLOC SDL_realloc
#define STBI_FREE SDL_free
#include <stb_image.h>

struct ZrTexture {
	GLuint id;
	int width, height;
	unsigned short bx, by;
};

struct Vtx {
	GLfloat position[2];
	union {
		GLushort texcoord[2];
		GLubyte color[4];
	};
};

GLuint gl_load_shader(const char *name, GLenum type, const char *src) {
	GLint success;
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &src, nullptr);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLint log_length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_length); // includes null terminator
		char *log = (char *)SDL_malloc(log_length);
		glGetShaderInfoLog(shader, log_length, nullptr, log);
		if (log[log_length - 2] == '\n') log[log_length - 2] = '\0';
		zr_log_error("%s: shader compile error:\n%s", name, log);
		SDL_free(log);
		return 0;
	}
	return shader;
}

GLuint gl_link_program(const char *name, int num_shaders, ...) {
	GLuint program = glCreateProgram();
	va_list v;
	va_start(v, num_shaders);
	for (int i = 0; i < num_shaders; i++) {
		glAttachShader(program, va_arg(v, GLuint));
	}
	va_end(v);
	glLinkProgram(program);
	GLint success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		GLint log_length;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
		char *log = (char *)SDL_malloc(log_length);
		glGetProgramInfoLog(program, log_length, nullptr, log); // includes null terminator
		if (log[log_length - 2] == '\n') log[log_length - 2] = '\0';
		zr_log_error("%s: program link error:\n%s", name, log);
		SDL_free(log);
		return 0;
	}
	return program;
}

// states

SDL_GLContext glcontext;
bool prev_vsync;

int bres_width;
int bres_height;
int vres_width;
int vres_height;
float viewport_transform[16];

GLuint vao;
GLuint ebo;
GLuint vbo;
GLuint color_program;
//GLint color_program_u_view_scale;
//GLint color_program_u_view_offset;
GLint color_program_u_viewport_transform;
GLuint texture_program;
GLint texture_program_u_view_scale;
GLint texture_program_u_view_offset;
GLint texture_program_u_viewport_transform;
GLint texture_program_u_texture;
GLint texture_program_u_texcoord_inverse_transform;
GLuint final_program;
GLint final_program_u_texture;
GLuint final_fbo;
GLuint final_tex;

std::set<GLuint> textures;
ZrTexture missing_texture;

// shader sources

// do not change these without changing the shader sources
constexpr GLuint ZR_ATTRIB_INPUT_POSITION = 0;
constexpr GLuint ZR_ATTRIB_INPUT_TEXCOORD = 1;
constexpr GLuint ZR_ATTRIB_INPUT_COLOR = 2;

const char *color_vertshader_src =
	"#version 330 core\n"
	"uniform mat4 zr_viewport_transform;\n"
	"layout(location = 0) in vec2 zr_vtx_pos;\n"
	"layout(location = 2) in vec4 zr_vtx_color;\n"
	"out vec4 zr_color;\n"
	"void main() {\n"
	"  gl_Position = zr_viewport_transform * vec4(zr_vtx_pos.xy, 0.0, 1.0);\n"
	"  zr_color = zr_vtx_color;\n"
	"}\n";
const char *color_fragshader_src =
	"#version 330 core\n"
	"in vec4 zr_color;\n"
	"out vec4 color;\n"
	"void main() {\n"
	"  color = zr_color / 255.0;\n"
	"}\n";
const char *texture_vertshader_src =
	"#version 330 core\n"
	"uniform float zr_view_scale;\n"
	"uniform vec2 zr_view_offset;\n"
	"uniform mat4 zr_viewport_transform;\n"
	"layout(location = 0) in vec2 zr_vtx_pos;\n"
	"layout(location = 1) in vec2 zr_vtx_texcoord;\n"
	"out vec2 zr_texcoord;\n"
	"void main() {\n"
	"  gl_Position = zr_viewport_transform * vec4(floor(zr_view_scale * zr_vtx_pos.xy) - floor(zr_view_scale * zr_view_offset), 0.0, 1.0);\n"
	"  zr_texcoord = zr_vtx_texcoord;\n"
	"}\n";
const char *texture_fragshader_src =
	"#version 330 core\n"
	"uniform sampler2D zr_texture;\n"
	"uniform vec2 zr_texcoord_inverse_transform;\n"
	"in vec2 zr_texcoord;\n"
	"layout(location = 0) out vec4 color;\n"
	"void main() {\n"
	"  color = texture(zr_texture, zr_texcoord * zr_texcoord_inverse_transform);\n"
	"}\n";
const char *final_vertshader_src =
	"#version 330 core\n"
	"layout(location = 0) in vec2 zr_vtx_pos;\n"
	"layout(location = 1) in vec2 zr_vtx_texcoord;\n"
	"out vec2 zr_texcoord;\n"
	"void main() {\n"
	"  gl_Position = vec4(zr_vtx_pos, 0.0, 1.0);\n"
	"  zr_texcoord = zr_vtx_texcoord;\n"
	"}\n";
const char *final_fragshader_src =
	"#version 330 core\n"
	"uniform sampler2D zr_texture;\n"
	"in vec2 zr_texcoord;\n"
	"layout(location = 0) out vec4 color;\n"
	"void main() {\n"
	"  color = texture(zr_texture, zr_texcoord);\n"
	"}\n";

void gl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, [[maybe_unused]] GLsizei length, const char *message, [[maybe_unused]] const void *user) {
	const char *source_str = nullptr;
	switch (source) {
		case GL_DEBUG_SOURCE_API_ARB:             source_str = "API";             break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   source_str = "WINDOW_SYSTEM";   break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: source_str = "SHADER_COMPILER"; break;
		case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     source_str = "THIRD_PARTY";     break;
		case GL_DEBUG_SOURCE_APPLICATION_ARB:     source_str = "APPLICATION";     break;
		case GL_DEBUG_SOURCE_OTHER_ARB:           source_str = "OTHER";           break;
	}
	const char *type_str = nullptr;
	switch (type) {
		case GL_DEBUG_TYPE_ERROR_ARB:               type_str = "ERROR";               break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: type_str = "DEPRECATED_BEHAVIOR"; break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  type_str = "UNDEFINED_BEHAVIOR";  break;
		case GL_DEBUG_TYPE_PORTABILITY_ARB:         type_str = "PORTABILITY";         break;
		case GL_DEBUG_TYPE_PERFORMANCE_ARB:         type_str = "PERFORMANCE";         break;
		case GL_DEBUG_TYPE_OTHER_ARB:               type_str = "OTHER";               break;
	}
	const char *severity_str = nullptr;
	switch (severity) {
		case GL_DEBUG_SEVERITY_NOTIFICATION: severity_str = "notif";  break;
		case GL_DEBUG_SEVERITY_HIGH_ARB:     severity_str = "high";   break;
		case GL_DEBUG_SEVERITY_MEDIUM_ARB:   severity_str = "medium"; break;
		case GL_DEBUG_SEVERITY_LOW_ARB:      severity_str = "low";    break;
	}
	zr_log_info("[src=%s type=%s id=%d sev=%s] %s\n", source_str, type_str, id, severity_str, message);
}

// od -v -An -tx8 missing.png
const uint64_t missing_png[] = {
	0x0a1a0a0d474e5089ull, 0x524448490d000000ull, 0x2000000020000000ull, 0xed18fc0000000208ull,
	0x59487009000000a3ull, 0x2e0000232e000073ull, 0x0000763fa5780123ull, 0xc748544144493000ull,
	0x020800000dc1ceedull, 0x49a7819d687fdc31ull, 0x4c9a69b366524defull, 0x200bc000000007b7ull,
	0x3c0000000021fd65ull, 0xce8d014076240e00ull, 0x454900000000fbfdull, 0x0000826042ae444eull
};
// du -b missing.png
const int missing_png_len = 126;

void init_missing_texture() {
	missing_texture.bx = 0;
	missing_texture.by = 0;
	void *missing_bitmap = stbi_load_from_memory((const stbi_uc *)missing_png, missing_png_len, &missing_texture.width, &missing_texture.height, nullptr, 3);
	glGenTextures(1, &missing_texture.id);
	glBindTexture(GL_TEXTURE_2D, missing_texture.id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, missing_texture.width, missing_texture.height, 0, GL_RGB, GL_UNSIGNED_BYTE, missing_bitmap);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_TEXTURE, missing_texture.width, -1, "zr missing texture");
	stbi_image_free(missing_bitmap);
}

[[noreturn]] void respectfully_tell_the_user_their_device_sucks_and_exit() {
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "zrapr", "Your device does not support OpenGL 3.3+", g_client_context.window);
	SDL_Quit();
	exit(1);
}

void gfx_init() {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	// create context
	glcontext = SDL_GL_CreateContext(g_client_context.window);
	if (glcontext == nullptr) {
		respectfully_tell_the_user_their_device_sucks_and_exit();
	}
	// load opengl
	gladLoadGLLoader(SDL_GL_GetProcAddress);
	if (!GLAD_GL_VERSION_3_3) {
		respectfully_tell_the_user_their_device_sucks_and_exit();
	}

	SDL_GL_SetSwapInterval(g_client_context.window_vsync ? 1 : 0); // vsync
	prev_vsync = g_client_context.window_vsync;

	int resolution_width, resolution_height;
	SDL_GetWindowSize(g_client_context.window, &resolution_width, &resolution_height);

	// "missing texture" texture
	init_missing_texture();

	//glEnable(GL_DEBUG_OUTPUT);
	//glDebugMessageCallback(gl_debug_callback, 0);

	// generate VAO
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_VERTEX_ARRAY, vao, -1, "zr vao");

	// setup EBO
	glGenBuffers(1, &ebo);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_BUFFER, ebo, -1, "zr ebo");

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_BUFFER, vbo, -1, "zr vbo");
	glVertexAttribPointer(ZR_ATTRIB_INPUT_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vtx), (void *)offsetof(Vtx, position));
	glVertexAttribPointer(ZR_ATTRIB_INPUT_TEXCOORD, 2, GL_UNSIGNED_SHORT, GL_FALSE, sizeof(Vtx), (void *)offsetof(Vtx, texcoord));
	glVertexAttribPointer(ZR_ATTRIB_INPUT_COLOR, 4, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(Vtx), (void *)offsetof(Vtx, color));
	glEnableVertexAttribArray(ZR_ATTRIB_INPUT_POSITION);

	// setup color shader
	GLuint color_vertshader = gl_load_shader("color_vertshader", GL_VERTEX_SHADER, color_vertshader_src);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_SHADER, color_vertshader, -1, "zr color vertshader");
	GLuint color_fragshader = gl_load_shader("color_fragshader", GL_FRAGMENT_SHADER, color_fragshader_src);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_SHADER, color_fragshader, -1, "zr color fragshader");
	color_program = gl_link_program("color_program", 2, color_vertshader, color_fragshader);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_PROGRAM, color_program, -1, "zr color program");
	color_program_u_viewport_transform = glGetUniformLocation(color_program, "zr_viewport_transform");

	// setup texture shader
	GLuint texture_vertshader = gl_load_shader("texture_vertshader", GL_VERTEX_SHADER, texture_vertshader_src);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_SHADER, texture_vertshader, -1, "zr texture vertshader");
	GLuint texture_fragshader = gl_load_shader("texture_fragshader", GL_FRAGMENT_SHADER, texture_fragshader_src);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_SHADER, texture_fragshader, -1, "zr texture fragshader");
	texture_program = gl_link_program("texture_program", 2, texture_vertshader, texture_fragshader);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_PROGRAM, texture_program, -1, "zr texture program");
	texture_program_u_view_scale = glGetUniformLocation(texture_program, "zr_view_scale");
	texture_program_u_view_offset = glGetUniformLocation(texture_program, "zr_view_offset");
	texture_program_u_viewport_transform = glGetUniformLocation(texture_program, "zr_viewport_transform");
	texture_program_u_texture = glGetUniformLocation(texture_program, "zr_texture");
	texture_program_u_texcoord_inverse_transform = glGetUniformLocation(texture_program, "zr_texcoord_inverse_transform");

	// setup final shader
	GLuint final_vertshader = gl_load_shader("final_vertshader", GL_VERTEX_SHADER, final_vertshader_src);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_SHADER, final_vertshader, -1, "zr final vertshader");
	GLuint final_fragshader = gl_load_shader("final_fragshader", GL_FRAGMENT_SHADER, final_fragshader_src);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_SHADER, final_fragshader, -1, "zr final fragshader");
	final_program = gl_link_program("final_program", 2, final_vertshader, final_fragshader);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_PROGRAM, final_program, -1, "zr final program");
	final_program_u_texture = glGetUniformLocation(final_program, "zr_texture");

	// setup final framebuffer
	glGenFramebuffers(1, &final_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, final_fbo);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_FRAMEBUFFER, final_fbo, -1, "zr final fbo");

	// setup final framebuffer texture
	glGenTextures(1, &final_tex);
	glBindTexture(GL_TEXTURE_2D, final_tex);
	gfx_set_backbuffer_size(resolution_width, resolution_height);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, final_tex, 0);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) glObjectLabel(GL_TEXTURE, final_tex, -1, "zr final tex");
}

void gfx_uninit() {
	glDeleteBuffers(1, &ebo);
	glDeleteBuffers(1, &vbo);
	for (GLuint id : textures) {
		glDeleteTextures(1, &id);
	}
	glDeleteFramebuffers(1, &final_fbo);
	glDeleteTextures(1, &final_tex);
	glDeleteProgram(texture_program);
	glDeleteProgram(color_program);
	glDeleteVertexArrays(1, &vao);

	SDL_GL_DeleteContext(glcontext);
}

void flush();

void update_final_framebuffer() {
	if (final_tex == 0) return;
	flush();
	glBindTexture(GL_TEXTURE_2D, final_tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, vres_width, vres_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
}

void set_virtual_resoluition(int rw, int rh) {
	vres_width = rw;
	vres_height = rh;
	update_final_framebuffer();
}

void gfx_set_backbuffer_size(int w, int h) {
	// lie about the resolution
	bres_width = w + w % 2;
	bres_height = h + h % 2;
	set_virtual_resoluition(bres_width, bres_height);
}

void gfx_flush() {
	flush();
}

void update_viewport_transform(float vp_width, float vp_height) {
	float m[16] = {
		2.0f / vp_width, 0.0f, 0.0f, 0.0f,
		0.0f, 2.0f / vp_height, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f,
	};
	memcpy(viewport_transform, m, sizeof viewport_transform);
}

void gfx_begin_frame() {
	// clear
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// vsync
	if (g_client_context.window_vsync != prev_vsync) {
		SDL_GL_SetSwapInterval(g_client_context.window_vsync ? 1 : 0);
		prev_vsync = g_client_context.window_vsync;
	}

	// state setup
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	glViewport(0, 0, vres_width, vres_height);

	// vertex setup
	glEnableVertexAttribArray(ZR_ATTRIB_INPUT_POSITION);
	update_viewport_transform(vres_width, vres_height);
}

void gfx_end_frame() {
	flush();

	// setup backbuffer
	glBindFramebuffer(GL_FRAMEBUFFER, GL_NONE);
	glViewport(0, 0, bres_width, bres_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// program setup
	glUseProgram(final_program);
	glEnableVertexAttribArray(ZR_ATTRIB_INPUT_TEXCOORD);
	glDisableVertexAttribArray(ZR_ATTRIB_INPUT_COLOR);

	// texture
	glBindTexture(GL_TEXTURE_2D, final_tex);

	// VBO setup
	Vtx vertices[4];
	vertices[0].position[0] = -1.0f; vertices[0].position[1] =  1.0f; // TL
	vertices[1].position[0] =  1.0f; vertices[1].position[1] =  1.0f; // TR
	vertices[2].position[0] = -1.0f; vertices[2].position[1] = -1.0f; // BL
	vertices[3].position[0] =  1.0f; vertices[3].position[1] = -1.0f; // BR
	vertices[0].texcoord[0] = 0.0f; vertices[0].texcoord[1] = 0.0f; // TL
	vertices[1].texcoord[0] = 1.0f; vertices[1].texcoord[1] = 0.0f; // TR
	vertices[2].texcoord[0] = 0.0f; vertices[2].texcoord[1] = 1.0f; // BL
	vertices[3].texcoord[0] = 1.0f; vertices[3].texcoord[1] = 1.0f; // BR
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STREAM_DRAW);

	// draw
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

// RENAME TO make this imply use of the backbuffer
void gfx_transform_coordinates(float x, float y, float *xp, float *yp) {
	*xp = floorf(g_client_context.view_scale * x) - floorf(g_client_context.view_scale * g_client_context.view_x) + (int)(bres_width / 2) * (bres_width / (float)vres_width);
	*yp = floorf(g_client_context.view_scale * y) - floorf(g_client_context.view_scale * g_client_context.view_y) + (int)(bres_height / 2) * (bres_height / (float)vres_height);
}

void gfx_clear(SDL_Color color) {
	glBindFramebuffer(GL_FRAMEBUFFER, final_fbo);
	constexpr float m = 255.0f;
	glClearColor(color.r / m, color.g / m, color.b / m, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

ZrTexture *gfx_create_texture(unsigned int width, unsigned int height, void *pixels) {
	ZrTexture *texture = new ZrTexture;
	texture->width = width;
	texture->height = height;
	texture->bx = texture->by = 0;

	glGenTextures(1, &texture->id);
	textures.emplace(texture->id);

	glBindTexture(GL_TEXTURE_2D, texture->id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	if (GLAD_GL_VERSION_4_3 || GLAD_GL_KHR_debug) {
		char buf[32];
		snprintf(buf, sizeof buf, "zr texture %u", texture->id);
		glObjectLabel(GL_TEXTURE, texture->id, -1, buf);
	}

	return texture;
}

void gfx_upload_texture(ZrTexture *texture, int x, int y, int width, int height, void *pixels) {
	flush();

	glBindTexture(GL_TEXTURE_2D, texture->id);
	glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
}

ZrTexture *gfx_load_texture_from_file(const char *path) {
	PHYSFS_File *handle = PHYSFS_openRead(path);
	if (handle == nullptr) {
		zr_log_error("%s: could not open file", path);
		return nullptr;
	}

	PHYSFS_sint64 png_len = PHYSFS_fileLength(handle);
	unsigned char *png = (unsigned char *)SDL_malloc(png_len);
	PHYSFS_readBytes(handle, png, png_len);
	PHYSFS_close(handle);

	int w, h;
	void *bitmap = stbi_load_from_memory(png, png_len, &w, &h, nullptr, 4);
	SDL_free(png);
	if (bitmap == nullptr) {
		zr_log_error("%s: failed to load image: %s", path, stbi_failure_reason());
		return nullptr;
	}

	ZrTexture *texture = gfx_create_texture(w, h);
	gfx_upload_texture(texture, 0, 0, w, h, bitmap);

	stbi_image_free(bitmap);

	return texture;
}

void gfx_get_texture_dimensions(ZrTexture *texture, unsigned int *x, unsigned int *y) {
	if (x != nullptr) { *x = (texture == nullptr ? missing_texture.width : texture->width); }
	if (y != nullptr) { *y = (texture == nullptr ? missing_texture.height : texture->height); }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"
void *gfx_get_texture_handle(ZrTexture *texture) {
	return texture == nullptr ? (void *)missing_texture.id : (void *)texture->id;
}
#pragma GCC diagnostic pop

void gfx_free_texture(ZrTexture *texture) {
	if (texture != nullptr) {
		glDeleteTextures(1, &texture->id);
		textures.erase(textures.find(texture->id));
	}
}

// batching

constexpr size_t VBO_DATA_BUFFER_SIZE = 0x3000;
constexpr size_t EBO_DATA_BUFFER_SIZE = 0x4000;

bool batch_is_dirty = false;
GLuint batch_program;
ZrTexture batch_texture;
GLuint batch_ebo_data[EBO_DATA_BUFFER_SIZE];
Vtx batch_vbo_data[VBO_DATA_BUFFER_SIZE];
unsigned int batch_ebo_index; /* number of vertices in the EBO data (+=6) */
unsigned int batch_vbo_index; /* number of vertices in the VBO data (+=4) */

void setup_color_program() {
	batch_program = color_program;
}

void setup_texture_program() {
	batch_program = texture_program;
}

void setup_texture(ZrTexture *texture) {
	memcpy(&batch_texture, texture, sizeof batch_texture);
}

void finish(unsigned int vbo_data_grow, unsigned int ebo_data_grow) {
	batch_vbo_index += vbo_data_grow;
	batch_ebo_index += ebo_data_grow;
	if (batch_vbo_index + vbo_data_grow > VBO_DATA_BUFFER_SIZE || batch_ebo_index + ebo_data_grow > EBO_DATA_BUFFER_SIZE) {
		flush();
	}
	batch_is_dirty = true;
}

void flush_color_program() {
	// program and VAO setup
	glUseProgram(color_program);
	glDisableVertexAttribArray(ZR_ATTRIB_INPUT_TEXCOORD);
	glEnableVertexAttribArray(ZR_ATTRIB_INPUT_COLOR);
	// EBO upload
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * batch_ebo_index, batch_ebo_data, GL_STREAM_DRAW);
	// VBO upload
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, batch_vbo_index * sizeof(Vtx), batch_vbo_data, GL_STREAM_DRAW);
	// draw
	glUniformMatrix4fv(color_program_u_viewport_transform, 1, GL_FALSE, viewport_transform);
	//glUniform1f(color_program_u_view_scale, g_client_context.view_scale);
	//glUniform2f(color_program_u_view_offset, g_client_context.view_x, g_client_context.view_y);
	glBindFramebuffer(GL_FRAMEBUFFER, final_fbo); // render to final
	glDrawElements(GL_TRIANGLES, batch_ebo_index, GL_UNSIGNED_INT, nullptr);

	// reset
	batch_ebo_index = 0;
	batch_vbo_index = 0;
	batch_is_dirty = true;
}

void flush_texture_program() {
	// program and VAO setup
	glUseProgram(texture_program);
	glEnableVertexAttribArray(ZR_ATTRIB_INPUT_TEXCOORD);
	glDisableVertexAttribArray(ZR_ATTRIB_INPUT_COLOR);
	// texture
	glBindTexture(GL_TEXTURE_2D, batch_texture.id);
	glUniform2f(texture_program_u_texcoord_inverse_transform, 1.0f / batch_texture.width, 1.0f / batch_texture.height);
	// EBO upload
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * batch_ebo_index, batch_ebo_data, GL_STREAM_DRAW);
	// VBO upload
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, batch_vbo_index * sizeof(Vtx), batch_vbo_data, GL_STREAM_DRAW);
	// draw
	glUniformMatrix4fv(texture_program_u_viewport_transform, 1, GL_FALSE, viewport_transform);
	glUniform1f(texture_program_u_view_scale, g_client_context.view_scale);
	glUniform2f(texture_program_u_view_offset, g_client_context.view_x, g_client_context.view_y);
	glBindFramebuffer(GL_FRAMEBUFFER, final_fbo); // render to final
	glDrawElements(GL_TRIANGLES, batch_ebo_index, GL_UNSIGNED_INT, nullptr);

	// reset
	batch_ebo_index = 0;
	batch_vbo_index = 0;
	batch_is_dirty = false;
}

void flush() {
	if (!batch_is_dirty) return;
	if (batch_program == color_program) {
		flush_color_program();
	} else if (batch_program == texture_program) {
		flush_texture_program();
	}
}

void gfx_draw_line(const SDL_Color color, float x1, float y1, float x2, float y2) {
	if (batch_program != color_program) {
		flush();
		setup_color_program();
	}

	// EBO setup
	batch_ebo_data[batch_ebo_index + 0] = batch_vbo_index + 0;
	batch_ebo_data[batch_ebo_index + 1] = batch_vbo_index + 1;
	batch_ebo_data[batch_ebo_index + 2] = batch_vbo_index + 2;
	batch_ebo_data[batch_ebo_index + 3] = batch_vbo_index + 1;
	batch_ebo_data[batch_ebo_index + 4] = batch_vbo_index + 2;
	batch_ebo_data[batch_ebo_index + 5] = batch_vbo_index + 3;

	// vertex setup
	Vtx &vtx1 = batch_vbo_data[batch_vbo_index + 0];
	Vtx &vtx2 = batch_vbo_data[batch_vbo_index + 1];
	Vtx &vtx3 = batch_vbo_data[batch_vbo_index + 2];
	Vtx &vtx4 = batch_vbo_data[batch_vbo_index + 3];

	// positions
	// (vertex positions modified from SDL_gpu)
	constexpr float thickness = 1.0f;
	float line_angle = atan2f(y2 - y1, x2 - x1);
	float tx = g_client_context.view_scale * thickness * 0.5f * sinf(line_angle);
	float ty = g_client_context.view_scale * thickness * 0.5f * cosf(line_angle);
	float vx1 = floorf(g_client_context.view_scale * x1) - floorf(g_client_context.view_scale * g_client_context.view_x);
	float vy1 = floorf(g_client_context.view_scale * y1) - floorf(g_client_context.view_scale * g_client_context.view_y);
	float vx2 = floorf(g_client_context.view_scale * x2) - floorf(g_client_context.view_scale * g_client_context.view_x);
	float vy2 = floorf(g_client_context.view_scale * y2) - floorf(g_client_context.view_scale * g_client_context.view_y);
	vtx1.position[0] = vx1 + tx; vtx1.position[1] = vy1 - ty;
	vtx2.position[0] = vx1 - tx; vtx2.position[1] = vy1 + ty;
	vtx3.position[0] = vx2 + tx; vtx3.position[1] = vy2 - ty;
	vtx4.position[0] = vx2 - tx; vtx4.position[1] = vy2 + ty;

	// colors
	vtx1.color[0] = color.r; vtx1.color[1] = color.g; vtx1.color[2] = color.b; vtx1.color[3] = color.a; // TL
	vtx2.color[0] = color.r; vtx2.color[1] = color.g; vtx2.color[2] = color.b; vtx2.color[3] = color.a; // TR
	vtx3.color[0] = color.r; vtx3.color[1] = color.g; vtx3.color[2] = color.b; vtx3.color[3] = color.a; // BL
	vtx4.color[0] = color.r; vtx4.color[1] = color.g; vtx4.color[2] = color.b; vtx4.color[3] = color.a; // BR

	// finish
	finish(4, 6);
}

void gfx_draw_rect(const SDL_Color color, const NEWRECT rect) {
	gfx_draw_line(color, rect.x1, rect.y1, rect.x2, rect.y1);
	gfx_draw_line(color, rect.x1, rect.y1, rect.x1, rect.y2);
	gfx_draw_line(color, rect.x1, rect.y2, rect.x2, rect.y2);
	gfx_draw_line(color, rect.x2, rect.y1, rect.x2, rect.y2);
}

void gfx_draw_rect_filled(const SDL_Color color, const NEWRECT rect) {
	if (batch_program != color_program) {
		flush();
		setup_color_program();
	}

	// EBO setup
	batch_ebo_data[batch_ebo_index + 0] = batch_vbo_index + 0;
	batch_ebo_data[batch_ebo_index + 1] = batch_vbo_index + 1;
	batch_ebo_data[batch_ebo_index + 2] = batch_vbo_index + 2;
	batch_ebo_data[batch_ebo_index + 3] = batch_vbo_index + 1;
	batch_ebo_data[batch_ebo_index + 4] = batch_vbo_index + 2;
	batch_ebo_data[batch_ebo_index + 5] = batch_vbo_index + 3;

	// vertex setup
	Vtx &vtx1 = batch_vbo_data[batch_vbo_index + 0];
	Vtx &vtx2 = batch_vbo_data[batch_vbo_index + 1];
	Vtx &vtx3 = batch_vbo_data[batch_vbo_index + 2];
	Vtx &vtx4 = batch_vbo_data[batch_vbo_index + 3];

	// positions
	float vx1 = floorf(g_client_context.view_scale * rect.x1) - floorf(g_client_context.view_scale * g_client_context.view_x);
	float vy1 = floorf(g_client_context.view_scale * rect.y1) - floorf(g_client_context.view_scale * g_client_context.view_y);
	float vx2 = floorf(g_client_context.view_scale * rect.x2) - floorf(g_client_context.view_scale * g_client_context.view_x);
	float vy2 = floorf(g_client_context.view_scale * rect.y2) - floorf(g_client_context.view_scale * g_client_context.view_y);
	vtx1.position[0] = vx1; vtx1.position[1] = vy1; // TL
	vtx2.position[0] = vx2; vtx2.position[1] = vy1; // TR
	vtx3.position[0] = vx1; vtx3.position[1] = vy2; // BL
	vtx4.position[0] = vx2; vtx4.position[1] = vy2; // BR

	// colors
	vtx1.color[0] = color.r; vtx1.color[1] = color.g; vtx1.color[2] = color.b; vtx1.color[3] = color.a; // TL
	vtx2.color[0] = color.r; vtx2.color[1] = color.g; vtx2.color[2] = color.b; vtx2.color[3] = color.a; // TR
	vtx3.color[0] = color.r; vtx3.color[1] = color.g; vtx3.color[2] = color.b; vtx3.color[3] = color.a; // BL
	vtx4.color[0] = color.r; vtx4.color[1] = color.g; vtx4.color[2] = color.b; vtx4.color[3] = color.a; // BR

	// finish
	finish(4, 6);
}

void gfx_draw_texture(ZrTexture *texture, const NEWRECT srcrect, const NEWRECT dstrect) {
	if (texture == nullptr) texture = &missing_texture;
	if (batch_program != texture_program) {
		flush();
		setup_texture_program();
	}
	if (texture->id != GL_NONE && texture->id != batch_texture.id) {
		flush();
		setup_texture(texture);
	}

	// EBO setup
	batch_ebo_data[batch_ebo_index + 0] = batch_vbo_index + 0;
	batch_ebo_data[batch_ebo_index + 1] = batch_vbo_index + 1;
	batch_ebo_data[batch_ebo_index + 2] = batch_vbo_index + 2;
	batch_ebo_data[batch_ebo_index + 3] = batch_vbo_index + 1;
	batch_ebo_data[batch_ebo_index + 4] = batch_vbo_index + 2;
	batch_ebo_data[batch_ebo_index + 5] = batch_vbo_index + 3;

	// vertex setup
	Vtx &vtx1 = batch_vbo_data[batch_vbo_index + 0];
	Vtx &vtx2 = batch_vbo_data[batch_vbo_index + 1];
	Vtx &vtx3 = batch_vbo_data[batch_vbo_index + 2];
	Vtx &vtx4 = batch_vbo_data[batch_vbo_index + 3];

	// positions
	vtx1.position[0] = dstrect.x1; vtx1.position[1] = dstrect.y1; // TL
	vtx2.position[0] = dstrect.x2; vtx2.position[1] = dstrect.y1; // TR
	vtx3.position[0] = dstrect.x1; vtx3.position[1] = dstrect.y2; // BL
	vtx4.position[0] = dstrect.x2; vtx4.position[1] = dstrect.y2; // BR

	// texcoords
	vtx1.texcoord[0] = srcrect.x1 + texture->bx; vtx1.texcoord[1] = srcrect.y1 + texture->by; // TL
	vtx2.texcoord[0] = srcrect.x2 + texture->bx; vtx2.texcoord[1] = srcrect.y1 + texture->by; // TR
	vtx3.texcoord[0] = srcrect.x1 + texture->bx; vtx3.texcoord[1] = srcrect.y2 + texture->by; // BL
	vtx4.texcoord[0] = srcrect.x2 + texture->bx; vtx4.texcoord[1] = srcrect.y2 + texture->by; // BR

	// finish
	finish(4, 6);
}
