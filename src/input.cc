// input.cc

#include "input.hh"

#include <cstring>
#include <sstream>
#include <string_view>
#include <string>
#include <unordered_map>

#include <SDL.h>

#include "context.hh"
#include "util/input_constants.hh"

#define ANALOG_DEADZONE 0.244f
#define ANALOG_TO_BUTTON_THRESHOLD 0.3f

struct InputState {
	InputState();

	bool scancodes[SDL_NUM_SCANCODES];
	bool buttons[SDL_CONTROLLER_BUTTON_MAX];
	float axes_positive[SDL_CONTROLLER_AXIS_MAX];
	float axes_negative[SDL_CONTROLLER_AXIS_MAX];
};
InputState::InputState() {
	memset(this->scancodes, 0, sizeof this->scancodes);
	memset(this->buttons, 0, sizeof this->buttons);
	for (int i = 0; i < SDL_CONTROLLER_AXIS_MAX; i++) {
		this->axes_positive[i] = 0.0f;
		this->axes_negative[i] = 0.0f;
	}
}

std::unordered_map<SDL_JoystickID, SDL_GameController *> controller_by_instance_id;
std::unordered_map<SDL_JoystickID, int> controller_device_index_by_instance_id;

InputState working_state;
InputState current_state;
InputState global_previous_state;
InputState game_previous_state;

std::unordered_map<std::string, InputSpec *> input_specs;
std::vector<std::pair<std::string, InputSpec *>> input_specs_ordered;
std::unordered_map<std::string, InputSpec InputProfile::*> input_specs_profiled;
std::vector<std::pair<std::string, InputSpec InputProfile::*>> input_specs_profiled_ordered;

void put_spec(const char *name, InputSpec *spec) {
	input_specs.emplace(name, spec);
	input_specs_ordered.emplace_back(name, spec);
}
void put_spec_profiled(const char *name, InputSpec InputProfile::*spec) {
	input_specs_profiled.emplace(name, spec);
	input_specs_profiled_ordered.emplace_back(name, spec);
}

void input_init() {
	put_spec("frame_advance",         &g_client_context.input_control.frame_advance     );
	put_spec("pause",                 &g_client_context.input_control.pause             );
	put_spec("reload_level",          &g_client_context.input_control.reload_level      );
	put_spec("reload_playerchar",     &g_client_context.input_control.reload_playerchar );
	put_spec("zoom_in",               &g_client_context.input_control.zoom_in           );
	put_spec("zoom_out",              &g_client_context.input_control.zoom_out          );
	put_spec("toggle_collision_view", &g_client_context.input_control.toggle_collision_view );
	put_spec_profiled("left",       &InputProfile::left       );
	put_spec_profiled("right",      &InputProfile::right      );
	put_spec_profiled("up",         &InputProfile::up         );
	put_spec_profiled("down",       &InputProfile::down       );
	put_spec_profiled("jump",       &InputProfile::jump       );
	put_spec_profiled("dash",       &InputProfile::dash       );
	put_spec_profiled("aircontrol", &InputProfile::aircontrol );
	put_spec_profiled("freemove",   &InputProfile::freemove   );
	put_spec_profiled("respawn",    &InputProfile::respawn    );
}

void input_uninit() {
	for (auto [joystick_instance_id, controller_p] : controller_by_instance_id) {
		SDL_GameControllerClose(controller_p);
	}
	controller_by_instance_id.clear();
	controller_device_index_by_instance_id.clear();
}

const std::unordered_map<SDL_JoystickID, SDL_GameController *> &input_get_controllers() {
	return controller_by_instance_id;
}

const std::unordered_map<std::string, InputSpec *> &input_get_specs() { return input_specs; }
const std::vector<std::pair<std::string, InputSpec *>> &input_get_specs_ordered() { return input_specs_ordered; }
const std::unordered_map<std::string, InputSpec InputProfile::*> &input_get_specs_profiled() { return input_specs_profiled; }
const std::vector<std::pair<std::string, InputSpec InputProfile::*>> &input_get_specs_profiled_ordered() { return input_specs_profiled_ordered; }

int input_get_device_index(int joystick_instance_id) {
	return controller_device_index_by_instance_id[joystick_instance_id];
}

void input_controller_open(int joystick_device_index) {
	SDL_GameController *controller = SDL_GameControllerOpen(joystick_device_index);
	SDL_Joystick *joystick = SDL_GameControllerGetJoystick(controller);
	SDL_JoystickID joystick_instance_id = SDL_JoystickInstanceID(joystick);
	controller_by_instance_id[joystick_instance_id] = controller;
	controller_device_index_by_instance_id[joystick_instance_id] = joystick_device_index;
}
void input_controller_close(int joystick_instance_id) {
	SDL_GameControllerClose(controller_by_instance_id[joystick_instance_id]);
	controller_by_instance_id.erase(joystick_instance_id);
	controller_device_index_by_instance_id.erase(joystick_instance_id);
}

void try_bind(InputSpecEntry::Type type, unsigned int code, InputSpec &spec) {
	for (InputSpecEntry &entry : spec.entries) {
		if (entry.waiting_for_bind) {
			entry.type = type;
			entry.code = code;
			entry.waiting_for_bind = false;
		}
	}
}

void try_all_binds(InputSpecEntry::Type type, unsigned int code) {
	for (InputProfile &profile : g_client_context.input_control.profiles) {
		for (auto &[name, spec] : input_specs_profiled_ordered) {
			try_bind(type, code, &profile->*spec);
		}
	}
	for (auto &[name, spec] : input_specs_ordered) {
		try_bind(type, code, *spec);
	}
}

void input_event_key_down(SDL_Scancode scancode) {
	working_state.scancodes[scancode] = true;
	try_all_binds(InputSpecEntry::SCANCODE, scancode);
}
void input_event_key_up(SDL_Scancode scancode) {
	working_state.scancodes[scancode] = false;
}
void input_event_button_down([[maybe_unused]] unsigned int joystick_instance_id, SDL_GameControllerButton button) {
	working_state.buttons[button] = true;
	try_all_binds(InputSpecEntry::GAME_CONTROLLER_BUTTON, button);
}
void input_event_button_up([[maybe_unused]] unsigned int joystick_instance_id, SDL_GameControllerButton button) {
	working_state.buttons[button] = false;
}
void input_event_axis_update([[maybe_unused]] unsigned int joystick_instance_id, SDL_GameControllerAxis axis, Sint16 v) {
	if (v >= 0) {
		float analog = v / 32767.0f;
		working_state.axes_positive[axis] = analog;
		working_state.axes_negative[axis] = 0.0f;
		if (analog >= ANALOG_TO_BUTTON_THRESHOLD) {
			try_all_binds(InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE, axis);
		}
	} else {
		float analog = v / -32768.0f;
		working_state.axes_positive[axis] = 0.0f;
		working_state.axes_negative[axis] = analog;
		if (analog >= ANALOG_TO_BUTTON_THRESHOLD) {
			try_all_binds(InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE, axis);
		}
	}
}

void input_event_frame_end() {
	if (g_game_context.is_game_frame) {
		memcpy(&game_previous_state, &current_state, sizeof(InputState));
	}
	memcpy(&global_previous_state, &current_state, sizeof(InputState));

	if (g_client_context.imgui_io->WantTextInput) {
		// drop all keyboard input if imgui wants focus
		memset(working_state.scancodes, 0, sizeof working_state.scancodes);
	}

	memcpy(&current_state, &working_state, sizeof(InputState));
}

void input_set_waiting_for_bind(InputSpecEntry &entry) {
	entry.waiting_for_bind = true;
}

bool is_high(const InputState &state, const InputSpec &spec) {
	for (const InputSpecEntry &entry : spec.entries) {
		switch (entry.type) {
			case InputSpecEntry::SCANCODE:
				if (state.scancodes[entry.scancode])
					return true;
				break;
			case InputSpecEntry::GAME_CONTROLLER_BUTTON:
				if (state.buttons[entry.button])
					return true;
				break;
			case InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE:
				if (state.axes_positive[entry.axis] >= ANALOG_TO_BUTTON_THRESHOLD)
					return true;
				break;
			case InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE:
				if (state.axes_negative[entry.axis] >= ANALOG_TO_BUTTON_THRESHOLD)
					return true;
				break;
			default:
				break;
		}
	}
	return false;
}

bool global_input_is_pressed(const InputSpec &spec) {
	return is_high(current_state, spec) && !is_high(global_previous_state, spec);
}
bool global_input_is_held(const InputSpec &spec) {
	return is_high(current_state, spec);
}
bool global_input_is_released(const InputSpec &spec) {
	return !is_high(current_state, spec) && is_high(global_previous_state, spec);
}
bool game_input_is_pressed(const InputSpec &spec) {
	return is_high(current_state, spec) && !is_high(game_previous_state, spec);
}
bool game_input_is_held(const InputSpec &spec) {
	return is_high(current_state, spec);
}
bool game_input_is_released(const InputSpec &spec) {
	return !is_high(current_state, spec) && is_high(game_previous_state, spec);
}

float input_get_analog(const InputSpec &spec) {
	float ana = 0.0f;
	for (const InputSpecEntry &entry : spec.entries) {
		switch (entry.type) {
			case InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE:
				if (current_state.axes_positive[entry.axis] >= ANALOG_DEADZONE && current_state.axes_positive[entry.axis] > ana)
					ana = current_state.axes_positive[entry.axis];
				break;
			case InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE:
				if (current_state.axes_negative[entry.axis] >= ANALOG_DEADZONE && current_state.axes_negative[entry.axis] > ana)
					ana = current_state.axes_negative[entry.axis];
				break;
			default:
				if (is_high(current_state, spec))
					return 1.0f;
				break;
		}
	}
	return ana;
}

const std::string SCANCODE_PREFIX = "Kbd:";
const std::string GAMEPAD_PREFIX = "GP:";

std::string inputspecentry_to_string(const InputSpecEntry &entry) {
	switch (entry.type) {
		case InputSpecEntry::SCANCODE:
			return SCANCODE_PREFIX + scancode_to_str(entry.scancode);
		case InputSpecEntry::GAME_CONTROLLER_BUTTON:
			return GAMEPAD_PREFIX + game_controller_button_to_str(entry.button);
		case InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE:
			return GAMEPAD_PREFIX + game_controller_axis_to_str(entry.axis) + '+';
		case InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE:
			return GAMEPAD_PREFIX + game_controller_axis_to_str(entry.axis) + '-';
		case InputSpecEntry::UNBOUND:
		default:
			return "unbound";
	}
}

bool inputspecentry_from_string(const std::string_view strview, InputSpecEntry &entry) {
	if (!strview.compare(0, SCANCODE_PREFIX.size(), SCANCODE_PREFIX)) {
		SDL_Scancode scancode = str_to_scancode(strview.data() + SCANCODE_PREFIX.size());
		if (scancode != SDL_SCANCODE_UNKNOWN) {
			entry.type = InputSpecEntry::SCANCODE;
			entry.scancode = scancode;
			return true;
		}
	} else if (!strview.compare(0, GAMEPAD_PREFIX.size(), GAMEPAD_PREFIX)) {
		std::string_view button_substring = strview.substr(GAMEPAD_PREFIX.size());
		SDL_GameControllerButton button = str_to_game_controller_button(button_substring);
		if (button != SDL_CONTROLLER_BUTTON_INVALID) {
			entry.type = InputSpecEntry::GAME_CONTROLLER_BUTTON;
			entry.button = button;
			return true;
		}
		std::string_view axis_substring = strview.substr(GAMEPAD_PREFIX.size(), strview.size() - GAMEPAD_PREFIX.size() - 1);
		SDL_GameControllerAxis axis = str_to_game_controller_axis(axis_substring);
		if (axis != SDL_CONTROLLER_AXIS_INVALID) {
			char m = strview.at(strview.size() - 1);
			if (m == '+') {
				entry.type = InputSpecEntry::GAME_CONTROLLER_AXIS_POSITIVE;
				entry.axis = axis;
				return true;
			} else if (m == '-') {
				entry.type = InputSpecEntry::GAME_CONTROLLER_AXIS_NEGATIVE;
				entry.axis = axis;
				return true;
			}
		}
	}
	return false;
}

std::string inputspec_to_string(const InputSpec &spec) {
	std::string str;
	for (const InputSpecEntry &entry : spec.entries) {
		str += inputspecentry_to_string(entry);
		str += ';';
	}
	return str;
}

InputSpec inputspec_from_string(const char *str) {
	InputSpec spec;
	std::istringstream iss(str);
	std::string token;
	while (std::getline(iss, token, ';')) {
		if (!token.empty()) {
			InputSpecEntry &entry = spec.entries.emplace_back();
			if (!inputspecentry_from_string(token, entry)) {
				spec.entries.pop_back();
			}
		}
	}
	return spec;
}
