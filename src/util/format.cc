// format.cc

#include "format.hh"

#include <cstdarg>
#include <cstdio>
#include <string>

std::string format(const char *fmt, ...) {
	va_list v;
	va_start(v, fmt);
	int len = vsnprintf(nullptr, 0, fmt, v) + 1;
	va_end(v);
	char *buf = (char *)alloca(len);

	va_start(v, fmt);
	vsnprintf(buf, len, fmt, v);
	va_end(v);
	return buf;
}
