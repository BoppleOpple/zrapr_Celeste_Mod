// sleep.cc

#include "sleep.hh"

#include <cassert>
#include <chrono>
#include <cstring>
#include <thread>

#include <SDL_timer.h>

SleepMethodEnum g_sleep_method = SDL_DELAY;
bool g_sleep_correction = true;
float g_sleep_threshold = 0.85f;

void sleep_sdl_delay(double secs) {
	SDL_Delay(secs * 1000.0);
}

void sleep_cxx11_thread(double secs) {
	std::this_thread::sleep_for(std::chrono::microseconds((long)(secs * 1000000L)));
}

#ifdef __GNUC__

#include <math.h>
#include <errno.h>
#include <time.h>

// https://github.com/SFML/SFML/blob/master/src/SFML/System/Unix/SleepImpl.cpp (MIT)
void sleep_posix_nanosleep(double secs) {
	timespec ts;
	ts.tv_sec = (long)secs;
	ts.tv_nsec = fmod(secs, 1.0) * 1000000000L;
	while ((nanosleep(&ts, &ts) == -1) && (errno == EINTR)) {}
}

#endif

void sleep_body(double secs) {
	switch (g_sleep_method) {
		case SDL_DELAY:       sleep_sdl_delay(secs);       return;
		case CXX11_THREAD:    sleep_cxx11_thread(secs);    return;
#ifdef __GNUC__
		case POSIX_NANOSLEEP: sleep_posix_nanosleep(secs); return;
#endif
		default: assert(false);
	}
}

void sleep(float secs) {
	if (secs == 0.0f) return;
	Uint64 target = SDL_GetPerformanceCounter() + ((double)secs * SDL_GetPerformanceFrequency());
	if (g_sleep_threshold > 0.0f) {
		sleep_body((double)secs * g_sleep_threshold);
	}
	if (g_sleep_correction) {
		while (SDL_GetPerformanceCounter() < target) {}
	}
}

const char *sleepmethod_to_string(SleepMethodEnum m) {
	switch (m) {
		case SDL_DELAY:       return "SDL_DELAY";
		case CXX11_THREAD:    return "CXX11_THREAD";
#ifdef __GNUC__
		case POSIX_NANOSLEEP: return "POSIX_NANOSLEEP";
#endif
	}
	return nullptr;
}
bool sleepmethod_from_string(const char *str, SleepMethodEnum *out) {
	if(0);
	else if (!strcmp(str, "SDL_DELAY"))       { *out = SDL_DELAY;       return true; }
	else if (!strcmp(str, "CXX11_THREAD"))    { *out = CXX11_THREAD;    return true; }
#ifdef __GNUC__
	else if (!strcmp(str, "POSIX_NANOSLEEP")) { *out = POSIX_NANOSLEEP; return true; }
#endif
	return false;
}
