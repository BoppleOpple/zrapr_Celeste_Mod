// format.hh

#pragma once

#include <string>

#include "../anno.hh"

std::string format(PRINTF_FORMAT_STRING const char *fmt, ...) PRINTF_VARARG_FN(1);
