// input_constants.hh

#pragma once

#include <string_view>

#include <SDL.h>

void init_input_constants();

const char *scancode_to_str(SDL_Scancode sc);
SDL_Scancode str_to_scancode(std::string_view strview);

const char *game_controller_button_to_str(SDL_GameControllerButton button);
SDL_GameControllerButton str_to_game_controller_button(std::string_view strview);

const char *game_controller_axis_to_str(SDL_GameControllerAxis axis);
SDL_GameControllerAxis str_to_game_controller_axis(std::string_view strview);

#if SDL_VERSION_ATLEAST(2, 0, 12)
const char *game_controller_type_str(SDL_GameControllerType type);
#endif
