// inflate.hh

#pragma once

#include <stdint.h>

#include <vector>

/* inflate zlib or gzip compressed data */
std::vector<uint8_t> inflate_zlib(std::vector<uint8_t> compressed_data);

/* inflate zstd compressed data */
std::vector<uint8_t> inflate_zstd(std::vector<uint8_t> compressed_data);
