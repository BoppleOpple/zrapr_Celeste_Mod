// base64.hh

#pragma once

#include <cstdint>

#include <vector>

extern std::vector<uint8_t> base64_decode(const char *encoded_data);
