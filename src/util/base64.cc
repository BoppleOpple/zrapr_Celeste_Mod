// base64.cc

// not nearly RFC compliant. just jank enough to work.

#include "base64.hh"

#include <cstdint>
#include <cstdio>
#include <cstring>

#include <vector>

enum {
	BASE64_INVALID = -1
};

// a lookup table like this is much faster than branching
// I don't know how CPU cache works but this definitely fits in it
#define NV BASE64_INVALID
const int8_t base64_value_lookup[256] = {
	/* -----    x0  x1  x2  x3  x4  x5  x6  x7  x8  x9  xa  xb  xc  xd  xe  xf */
	/* 00-0f */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* 10-1f */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* 20-2f */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, 62, NV, NV, NV, 63,
	/* 30-3f */ 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, NV, NV, NV, NV, NV, NV,
	/* 40-4f */ NV,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	/* 50-5f */ 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, NV, NV, NV, NV, NV,
	/* 60-6f */ NV, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	/* 70-7f */ 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, NV, NV, NV, NV, NV,
	/* 80-8f */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* 90-9f */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* a0-af */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* b0-bf */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* c0-cf */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* d0-df */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* e0-ef */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
	/* f0-ff */ NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV, NV,
};
#undef NV

int8_t get_base64_value(char chr) {
	return base64_value_lookup[(uint8_t)chr];
}

std::vector<uint8_t> base64_decode(const char *encoded_data) {
	int encoded_data_len = strlen(encoded_data);

	int padding = 0;
	for (int i = encoded_data_len; i--;) {
		if (encoded_data[i] == '=') {
			padding++;
		} else if (get_base64_value(encoded_data[i]) != BASE64_INVALID) {
			break;
		}
	}

	std::vector<uint8_t> octets;
	// estimate decoded size
	// each 4 sextets (characters) is 3 octets* (bytes)
	// *not counting extra input characters like spaces and newlines
	octets.resize(encoded_data_len * 3 / 4);

	int sextet_index = 0;
	int octet_index = 0;
	for (int i = 0; i < encoded_data_len; i++) {
		char v = get_base64_value(encoded_data[i]);
		if (v != BASE64_INVALID) {
			switch (sextet_index) {
				case 0:
					octets[octet_index++] = v << 2;
					sextet_index++;
					break;
				case 1:
					octets[octet_index - 1] |= v >> 4;
					octets[octet_index++] = (v & 0b1111) << 4;
					sextet_index++;
					break;
				case 2:
					octets[octet_index - 1] |= v >> 2;
					octets[octet_index++] = (v & 0b11) << 6;
					sextet_index++;
					break;
				case 3:
					octets[octet_index - 1] |= v;
					sextet_index = 0;
					break;
			}
		}
	}
	if (padding > 0) {
		octets.resize(octet_index - 1);
	} else {
		octets.resize(octet_index);
	}

	return octets;
}
