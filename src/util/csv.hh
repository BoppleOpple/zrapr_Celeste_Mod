// csv.hh

#pragma once

#include <vector>

extern std::vector<unsigned int> csv_decode(const char *encoded_data);
