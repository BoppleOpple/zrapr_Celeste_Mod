// macosx_open_dir.mm

#include <AppKit/NSWorkspace.h>
#include <Foundation/Foundation.h>

int macosx_open_dir(const char *path) {
	@autoreleasepool {
		NSString *nsstr = [NSString stringWithUTF8String:path];
		NSURL *nsurl = [[NSURL alloc] initFileURLWithPath:nsstr isDirectory:YES];
		return [[NSWorkspace sharedWorkspace] openURL:nsurl] ? 0 : -1;
	}
}
