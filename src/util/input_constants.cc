// input_constants.cc

#include "input_constants.hh"

#include <map>
#include <string_view>
#include <string>

#include <SDL.h>

const char *scancode_to_str_array[SDL_NUM_SCANCODES];
std::map<std::string_view, SDL_Scancode> str_to_scancode_map;

const char *game_controller_button_to_str_array[SDL_CONTROLLER_BUTTON_MAX];
std::map<std::string_view, SDL_GameControllerButton> str_to_game_controller_button_map;

const char *game_controller_axis_to_str_array[SDL_CONTROLLER_AXIS_MAX];
std::map<std::string_view, SDL_GameControllerAxis> str_to_game_controller_axis_map;

void put_scancode(SDL_Scancode sc, const char *str) {
	scancode_to_str_array[sc] = str;
	str_to_scancode_map.emplace(str, sc);
}

void put_game_controller_button(SDL_GameControllerButton button, const char *str) {
	game_controller_button_to_str_array[button] = str;
	str_to_game_controller_button_map.emplace(str, button);
}

void put_game_controller_axis(SDL_GameControllerAxis axis, const char *str) {
	game_controller_axis_to_str_array[axis] = str;
	str_to_game_controller_axis_map.emplace(str, axis);
}

void init_scancodes() {
	memset(scancode_to_str_array, 0, sizeof scancode_to_str_array);
	put_scancode(SDL_SCANCODE_A, "A");
	put_scancode(SDL_SCANCODE_B, "B");
	put_scancode(SDL_SCANCODE_C, "C");
	put_scancode(SDL_SCANCODE_D, "D");
	put_scancode(SDL_SCANCODE_E, "E");
	put_scancode(SDL_SCANCODE_F, "F");
	put_scancode(SDL_SCANCODE_G, "G");
	put_scancode(SDL_SCANCODE_H, "H");
	put_scancode(SDL_SCANCODE_I, "I");
	put_scancode(SDL_SCANCODE_J, "J");
	put_scancode(SDL_SCANCODE_K, "K");
	put_scancode(SDL_SCANCODE_L, "L");
	put_scancode(SDL_SCANCODE_M, "M");
	put_scancode(SDL_SCANCODE_N, "N");
	put_scancode(SDL_SCANCODE_O, "O");
	put_scancode(SDL_SCANCODE_P, "P");
	put_scancode(SDL_SCANCODE_Q, "Q");
	put_scancode(SDL_SCANCODE_R, "R");
	put_scancode(SDL_SCANCODE_S, "S");
	put_scancode(SDL_SCANCODE_T, "T");
	put_scancode(SDL_SCANCODE_U, "U");
	put_scancode(SDL_SCANCODE_V, "V");
	put_scancode(SDL_SCANCODE_W, "W");
	put_scancode(SDL_SCANCODE_X, "X");
	put_scancode(SDL_SCANCODE_Y, "Y");
	put_scancode(SDL_SCANCODE_Z, "Z");
	put_scancode(SDL_SCANCODE_1, "1");
	put_scancode(SDL_SCANCODE_2, "2");
	put_scancode(SDL_SCANCODE_3, "3");
	put_scancode(SDL_SCANCODE_4, "4");
	put_scancode(SDL_SCANCODE_5, "5");
	put_scancode(SDL_SCANCODE_6, "6");
	put_scancode(SDL_SCANCODE_7, "7");
	put_scancode(SDL_SCANCODE_8, "8");
	put_scancode(SDL_SCANCODE_9, "9");
	put_scancode(SDL_SCANCODE_0, "0");
	put_scancode(SDL_SCANCODE_RETURN, "RETURN");
	put_scancode(SDL_SCANCODE_ESCAPE, "ESCAPE");
	put_scancode(SDL_SCANCODE_BACKSPACE, "BACKSPACE");
	put_scancode(SDL_SCANCODE_TAB, "TAB");
	put_scancode(SDL_SCANCODE_SPACE, "SPACE");
	put_scancode(SDL_SCANCODE_MINUS, "MINUS");
	put_scancode(SDL_SCANCODE_EQUALS, "EQUALS");
	put_scancode(SDL_SCANCODE_LEFTBRACKET, "LEFTBRACKET");
	put_scancode(SDL_SCANCODE_RIGHTBRACKET, "RIGHTBRACKET");
	put_scancode(SDL_SCANCODE_BACKSLASH, "BACKSLASH");
	put_scancode(SDL_SCANCODE_SEMICOLON, "SEMICOLON");
	put_scancode(SDL_SCANCODE_APOSTROPHE, "APOSTROPHE");
	put_scancode(SDL_SCANCODE_GRAVE, "GRAVE");
	put_scancode(SDL_SCANCODE_COMMA, "COMMA");
	put_scancode(SDL_SCANCODE_PERIOD, "PERIOD");
	put_scancode(SDL_SCANCODE_SLASH, "SLASH");
	put_scancode(SDL_SCANCODE_CAPSLOCK, "CAPSLOCK");
	put_scancode(SDL_SCANCODE_F1, "F1");
	put_scancode(SDL_SCANCODE_F2, "F2");
	put_scancode(SDL_SCANCODE_F3, "F3");
	put_scancode(SDL_SCANCODE_F4, "F4");
	put_scancode(SDL_SCANCODE_F5, "F5");
	put_scancode(SDL_SCANCODE_F6, "F6");
	put_scancode(SDL_SCANCODE_F7, "F7");
	put_scancode(SDL_SCANCODE_F8, "F8");
	put_scancode(SDL_SCANCODE_F9, "F9");
	put_scancode(SDL_SCANCODE_F10, "F10");
	put_scancode(SDL_SCANCODE_F11, "F11");
	put_scancode(SDL_SCANCODE_F12, "F12");
	put_scancode(SDL_SCANCODE_PRINTSCREEN, "PRINTSCREEN");
	put_scancode(SDL_SCANCODE_SCROLLLOCK, "SCROLLLOCK");
	put_scancode(SDL_SCANCODE_PAUSE, "PAUSE");
	put_scancode(SDL_SCANCODE_INSERT, "INSERT");
	put_scancode(SDL_SCANCODE_HOME, "HOME");
	put_scancode(SDL_SCANCODE_PAGEUP, "PAGEUP");
	put_scancode(SDL_SCANCODE_DELETE, "DELETE");
	put_scancode(SDL_SCANCODE_END, "END");
	put_scancode(SDL_SCANCODE_PAGEDOWN, "PAGEDOWN");
	put_scancode(SDL_SCANCODE_RIGHT, "RIGHT");
	put_scancode(SDL_SCANCODE_LEFT, "LEFT");
	put_scancode(SDL_SCANCODE_DOWN, "DOWN");
	put_scancode(SDL_SCANCODE_UP, "UP");
	put_scancode(SDL_SCANCODE_NUMLOCKCLEAR, "NUMLOCKCLEAR");
	put_scancode(SDL_SCANCODE_KP_DIVIDE, "KP_DIVIDE");
	put_scancode(SDL_SCANCODE_KP_MULTIPLY, "KP_MULTIPLY");
	put_scancode(SDL_SCANCODE_KP_MINUS, "KP_MINUS");
	put_scancode(SDL_SCANCODE_KP_PLUS, "KP_PLUS");
	put_scancode(SDL_SCANCODE_KP_ENTER, "KP_ENTER");
	put_scancode(SDL_SCANCODE_KP_1, "KP_1");
	put_scancode(SDL_SCANCODE_KP_2, "KP_2");
	put_scancode(SDL_SCANCODE_KP_3, "KP_3");
	put_scancode(SDL_SCANCODE_KP_4, "KP_4");
	put_scancode(SDL_SCANCODE_KP_5, "KP_5");
	put_scancode(SDL_SCANCODE_KP_6, "KP_6");
	put_scancode(SDL_SCANCODE_KP_7, "KP_7");
	put_scancode(SDL_SCANCODE_KP_8, "KP_8");
	put_scancode(SDL_SCANCODE_KP_9, "KP_9");
	put_scancode(SDL_SCANCODE_KP_0, "KP_0");
	put_scancode(SDL_SCANCODE_KP_PERIOD, "KP_PERIOD");
	put_scancode(SDL_SCANCODE_NONUSBACKSLASH, "NONUSBACKSLASH");
	put_scancode(SDL_SCANCODE_LCTRL, "LCTRL");
	put_scancode(SDL_SCANCODE_LSHIFT, "LSHIFT");
	put_scancode(SDL_SCANCODE_LALT, "LALT");
	put_scancode(SDL_SCANCODE_RCTRL, "RCTRL");
	put_scancode(SDL_SCANCODE_RSHIFT, "RSHIFT");
	put_scancode(SDL_SCANCODE_RALT, "RALT");
}

/*
on playstation controllers:
SDL A = PS cross
SDL B = PS circle
SDL X = PS square
SDL Y = PS triangle
*/
void init_game_controller_buttons() {
	memset(game_controller_button_to_str_array, 0, sizeof game_controller_button_to_str_array);
	put_game_controller_button(SDL_CONTROLLER_BUTTON_A, "A");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_B, "B");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_X, "X");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_Y, "Y");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_BACK, "BACK");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_GUIDE, "GUIDE");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_START, "START");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_LEFTSTICK, "LEFTSTICK");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_RIGHTSTICK, "RIGHTSTICK");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_LEFTSHOULDER, "LEFTSHOULDER");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER, "RIGHTSHOULDER");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_DPAD_UP, "DPAD_UP");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_DPAD_DOWN, "DPAD_DOWN");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_DPAD_LEFT, "DPAD_LEFT");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_DPAD_RIGHT, "DPAD_RIGHT");
#if SDL_VERSION_ATLEAST(2, 0, 14)
	put_game_controller_button(SDL_CONTROLLER_BUTTON_MISC1, "MISC1");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_PADDLE1, "PADDLE1");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_PADDLE2, "PADDLE2");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_PADDLE3, "PADDLE3");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_PADDLE4, "PADDLE4");
	put_game_controller_button(SDL_CONTROLLER_BUTTON_TOUCHPAD, "TOUCHPAD");
#endif
}

void init_game_controller_axes() {
	memset(game_controller_axis_to_str_array, 0, sizeof game_controller_axis_to_str_array);
	put_game_controller_axis(SDL_CONTROLLER_AXIS_LEFTX, "LEFTX");
	put_game_controller_axis(SDL_CONTROLLER_AXIS_LEFTY, "LEFTY");
	put_game_controller_axis(SDL_CONTROLLER_AXIS_RIGHTX, "RIGHTX");
	put_game_controller_axis(SDL_CONTROLLER_AXIS_RIGHTY, "RIGHTY");
	put_game_controller_axis(SDL_CONTROLLER_AXIS_TRIGGERLEFT, "TRIGGERLEFT");
	put_game_controller_axis(SDL_CONTROLLER_AXIS_TRIGGERRIGHT, "TRIGGERRIGHT");
}

void init_input_constants() {
	init_scancodes();
	init_game_controller_buttons();
	init_game_controller_axes();
}

const char *scancode_to_str(SDL_Scancode sc) {
	return scancode_to_str_array[sc];
}
SDL_Scancode str_to_scancode(std::string_view strview) {
	auto it = str_to_scancode_map.find(strview);
	if (it != str_to_scancode_map.end()) {
		return it->second;
	}
	return SDL_SCANCODE_UNKNOWN;
}

const char *game_controller_button_to_str(SDL_GameControllerButton button) {
	return game_controller_button_to_str_array[button];
}
SDL_GameControllerButton str_to_game_controller_button(std::string_view strview) {
	auto it = str_to_game_controller_button_map.find(strview);
	if (it != str_to_game_controller_button_map.end()) {
		return it->second;
	}
	return SDL_CONTROLLER_BUTTON_INVALID;
}

const char *game_controller_axis_to_str(SDL_GameControllerAxis axis) {
	return game_controller_axis_to_str_array[axis];
}
SDL_GameControllerAxis str_to_game_controller_axis(std::string_view strview) {
	auto it = str_to_game_controller_axis_map.find(strview);
	if (it != str_to_game_controller_axis_map.end()) {
		return it->second;
	}
	return SDL_CONTROLLER_AXIS_INVALID;
}

#if SDL_VERSION_ATLEAST(2, 0, 12)
const char *game_controller_type_str(SDL_GameControllerType type) {
	switch (type) {
		default:
		case SDL_CONTROLLER_TYPE_UNKNOWN: return "Unknown";
		case SDL_CONTROLLER_TYPE_XBOX360: return "Xbox 360";
		case SDL_CONTROLLER_TYPE_XBOXONE: return "Xbox One";
		case SDL_CONTROLLER_TYPE_PS3: return "PlayStation 3";
		case SDL_CONTROLLER_TYPE_PS4: return "PlayStation 4";
		case SDL_CONTROLLER_TYPE_NINTENDO_SWITCH_PRO: return "Nintendo Switch Pro";
#if SDL_VERSION_ATLEAST(2, 0, 14)
		case SDL_CONTROLLER_TYPE_VIRTUAL: return "Virtual";
		case SDL_CONTROLLER_TYPE_PS5: return "PlayStation 5";
#endif
#if SDL_VERSION_ATLEAST(2, 0, 16)
		case SDL_CONTROLLER_TYPE_AMAZON_LUNA: return "Amazon Luna";
		case SDL_CONTROLLER_TYPE_GOOGLE_STADIA: return "Google Stadia";
#endif
	}
}
#endif
