// map.hh

#pragma once

#include <map>
#include <string>
#include <vector>

#include <entt/entt.hpp>

#include "collision_parts.hh"
#include "gfx.hh"
#include "log.hh"

enum TileLayerRenderOrder {
	RIGHT_DOWN, RIGHT_UP, LEFT_DOWN, LEFT_UP
};
enum ObjectLayerDrawOrder {
	TOPDOWN, INDEX
};

struct ZrTile {
	struct AnimationFrame;

	ZrTexture *texture;
	int srcx, srcy;
	int srcw, srch;

	bool has_animation = false;
	unsigned int animation_duration;
	std::vector<ZrTile::AnimationFrame> animation_frames;

	std::vector<StaticCollisionPart> static_collision_absolute;
	std::vector<StaticCollisionPart> static_collision_normalized;

	std::vector<BodyCollisionPart> body_collision_absolute;
	std::vector<BodyCollisionPart> body_collision_normalized;
};

struct ZrTile::AnimationFrame {
	unsigned int gid;
	unsigned int end_at;
};

extern const ZrTile *get_tile(unsigned int gid);

extern void load_object_types();

extern void unload_level();
extern bool load_level(const char *filename, entt::registry &reg);

const unsigned int TMX_FLIP_HORZ = 0x80000000;
const unsigned int TMX_FLIP_VERT = 0x40000000;
const unsigned int TMX_FLIP_DIAG = 0x20000000;
const unsigned int TMX_GID_MASK = 0x1fffffff;
