// playerchar.cc

#include "playerchar.hh"

#include <cstdlib>
#include <string>
#include <vector>

#include <physfs.h>
#include <stb_image.h>

#include "configfile.hh"
#include "context.hh"
#include "fs.hh"
#include "gfx.hh"

void parse_frames(std::vector<unsigned int> &v, const char *str) {
	v.clear();
	for (;;) {
		unsigned int f = strtol(str, (char **)&str, 10);
		v.push_back(f);
		const char *sep_ptr = strchr(str, ',');
		if (sep_ptr == nullptr) return;
		str = sep_ptr + 1;
	}
}

int meta_handler(void *user, const char *section, const char *name, const char *value) {
	PlayerChar *pc = (PlayerChar *)user;
	if (!strcmp(section, "playerchar")) {
		if(0);
		else if (!strcmp(name, "width"))     pc->frame_width = atoi(value);
		else if (!strcmp(name, "vertical_offset")) pc->vertical_offset = atoi(value);
		else if (!strcmp(name, "animation_time")) pc->animation_time = atof(value);
		else if (!strcmp(name, "facing"))    pc->rightfacing = !strcasecmp(value, "right");
		else if (!strcmp(name, "idle"))      parse_frames(pc->idle, value);
		else if (!strcmp(name, "walk"))      parse_frames(pc->walk, value);
		else if (!strcmp(name, "idle_up"))   parse_frames(pc->idle_up, value);
		else if (!strcmp(name, "walk_up"))   parse_frames(pc->walk_up, value);
		else if (!strcmp(name, "jump"))      parse_frames(pc->jump, value);
		else if (!strcmp(name, "jump_up"))   parse_frames(pc->jump_up, value);
		else if (!strcmp(name, "jump_down")) parse_frames(pc->jump_down, value);
		else if (!strcmp(name, "fall"))      parse_frames(pc->fall, value);
		else if (!strcmp(name, "fall_up"))   parse_frames(pc->fall_up, value);
		else if (!strcmp(name, "fall_down")) parse_frames(pc->fall_down, value);
		else if (!strcmp(name, "back"))      pc->back = atoi(value);
	}
	return 1;
}

bool load_playerchar(PlayerChar *pc, const char *path) {
	new (pc) PlayerChar;

	PHYSFS_File *handle = PHYSFS_openRead(path);
	if (handle == nullptr) {
		zr_log_error("%s: could not open file", path);
		return false;
	}

	PHYSFS_sint64 png_len = PHYSFS_fileLength(handle);
	void *png = SDL_malloc(png_len);
	PHYSFS_readBytes(handle, png, png_len);
	PHYSFS_close(handle);

	void *bitmap = stbi_load_from_memory((const stbi_uc *)png, png_len, &pc->image_width, &pc->image_height, nullptr, 4);
	SDL_free(png);
	if (bitmap == nullptr) {
		zr_log_error("%s: failed to load image: %s", path, stbi_failure_reason());
		return false;
	}

	size_t sz = pc->image_width * pc->image_height * 4;
	pc->bitmap.resize(sz);
	memcpy(pc->bitmap.data(), bitmap, sz);

	std::string meta_path;
	meta_path.reserve(256);
	meta_path += path;
	meta_path += ".meta";
	fs_load_ini(meta_path.c_str(), meta_handler, pc);

	return true;
}

void set_playerchar(const PlayerChar &pc) {
	gfx_free_texture(g_game_context.playerchar_texture);

	g_game_context.playerchar = pc;
	g_game_context.playerchar_texture = gfx_create_texture(pc.image_width, pc.image_height, (void *)pc.bitmap.data());
}

void load_and_set_playerchar(const char *path) {
	PlayerChar pc;
	load_playerchar(&pc, path);
	set_playerchar(pc);

	g_game_context.playerchar_path = path; // std::string constructor, copies
	config_mark_dirty();
}
