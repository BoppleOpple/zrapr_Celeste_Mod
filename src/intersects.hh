// intersects.hh

#pragma once

#include "geom.hh"

extern bool intersect_rect_and_line(const NEWRECT rect, float *X1, float *Y1, float *X2, float *Y2);

extern bool intersect_rects(const NEWRECT rectA, const NEWRECT rectB);
