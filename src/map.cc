// map.cc

#include "map.hh"

#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <entt/entt.hpp>

#include "comp.hh"
#include "configfile.hh"
#include "context.hh"
#include "entities.hh"
#include "log.hh"
#include "tmx.hh"

// indexed by gid
std::vector<ZrTile> tiles_global;
const ZrTile *tiles_global_ptr;
unsigned int tiles_global_size;

ZrTile empty_tile;

const ZrTile *get_tile(unsigned int gid) {
	gid &= TMX_GID_MASK;
	if (gid < tiles_global_size) {
		return &tiles_global_ptr[gid];
	} else {
		return &empty_tile;
	}
}

struct LoadMapContext {
	const TmxTile *get_tile(unsigned int gid) const;
	unsigned int get_firstgid(unsigned int gid) const;

	const TmxMap *tmxmap;
	std::unordered_map<unsigned int, const TmxObject *> objects_by_id;
};

unsigned int LoadMapContext::get_firstgid(unsigned int gid) const {
	for (const TmxTileset &tileset : this->tmxmap->tilesets) {
		if (gid >= tileset.firstgid && gid < tileset.firstgid + tileset.tilecount) {
			return tileset.firstgid;
		}
	}
	return 0;
}

const TmxTile *LoadMapContext::get_tile(unsigned int gid) const {
	for (const TmxTileset &tileset : this->tmxmap->tilesets) {
		if (gid >= tileset.firstgid && gid < tileset.firstgid + tileset.tilecount) {
			return &tileset.tiles[gid - tileset.firstgid];
		}
	}
	return nullptr;
}

void prepass_layers(LoadMapContext *load_map_context, const std::vector<std::unique_ptr<TmxLayer>> &layers) {
	for (const std::unique_ptr<TmxLayer> &layer : layers) {
		if (layer->type == TmxLayer::OBJECT_LAYER) {
			TmxObjectLayer *objectlayer = static_cast<TmxObjectLayer *>(layer.get());
			for (const TmxObject &object : objectlayer->objects) {
				load_map_context->objects_by_id.emplace(object.id, &object);
			}

		} else if (layer->type == TmxLayer::GROUP_LAYER) {
			TmxGroupLayer *grouplayer = static_cast<TmxGroupLayer *>(layer.get());
			prepass_layers(load_map_context, grouplayer->layers);

		}
	}
}

void add_tile_layer(const LoadMapContext *load_map_context, const TmxTileLayer *tilelayer, float parent_offset_x, float parent_offset_y, unsigned int *index, entt::registry &reg) {
	if (tilelayer->tile_gids.size() < (tilelayer->width * tilelayer->height)) {
		zr_log_error("layer id %u: failed to load tiles for some reason", tilelayer->id);
		return;
	}
	if (tilelayer->width == 0 || tilelayer->height == 0) return;
	entt::entity e = reg.create();
	reg.emplace<CompOnLayer>(e, (*index)++);
	float x = parent_offset_x + tilelayer->tile_offset_x * load_map_context->tmxmap->tilewidth + tilelayer->offset_x;
	float y = parent_offset_y + tilelayer->tile_offset_y * load_map_context->tmxmap->tileheight + tilelayer->offset_y;
	reg.emplace<CompPosition>(e, x, y);
	CompTileLayer &comptilelayer = reg.emplace<CompTileLayer>(e);
	comptilelayer.width = tilelayer->width;
	comptilelayer.height = tilelayer->height;
	comptilelayer.tile_width = load_map_context->tmxmap->tilewidth;
	comptilelayer.tile_height = load_map_context->tmxmap->tileheight;
	comptilelayer.tile_gids = tilelayer->tile_gids;
	comptilelayer.render_order = load_map_context->tmxmap->renderorder;
	entity_update_tile_layer_static_collision(reg, e);
	reg.emplace<CompRender>(e);
}

// Quack
template <typename It>
void do_warp_points(It begin, It end, std::vector<TransitPoint> &points, float bx, float by) {
	int num_points = 0;
	float last_x, last_y;
	float total_time = 0.0f;

	for (It it = begin; it != end; it++) {
		const auto &[x, y] = *it;
		TransitPoint &point = points.emplace_back();
		point.x = bx + x;
		point.y = by + y;
		if (num_points > 0) {
			float dx = point.x - last_x;
			float dy = point.y - last_y;
			total_time += sqrtf((dy * dy) + (dx * dx));
		}
		point.end_time = total_time;
		num_points++;
		last_x = point.x;
		last_y = point.y;
	}
}

void add_object_layer(const LoadMapContext *load_map_context, const TmxObjectLayer *objectlayer, float parent_offset_x, float parent_offset_y, unsigned int *index, entt::registry &reg) {
	unsigned int object_layer_index = 0;
	for (const TmxObject &object : objectlayer->objects) {
		float x = parent_offset_x + objectlayer->offset_x + object.x;
		float y = parent_offset_y + objectlayer->offset_y + object.y;

		if (object.type == TmxObject::RECTANGLE && object.has_gid) {
			TmxProperties object_tile_properties = object.properties;
			const TmxTile *tile = load_map_context->get_tile(object.gid);
			if (tile != nullptr) {
				object_tile_properties.merge(tile->properties);
			}

			entt::entity e = reg.create();
			reg.emplace<CompPosition>(e, x + object.width / 2.0f, y - object.height / 2.0f);
			reg.emplace<CompOnLayer>(e, *index);
			reg.emplace<CompObjectLayerObject>(e, objectlayer->draworder, object_layer_index++);
			reg.emplace<CompRender>(e);

			reg.emplace<CompTile>(e, object.gid, NEWRECT::centered_of_size(object.width, object.height));
			entity_update_tile_static_collision(reg, e);
			entity_update_tile_body_collision(reg, e);

			std::string init = object_tile_properties.get_string("init");
			if (!init.compare("coin")) {
				reg.emplace<CompCoinTile>(e);
				g_game_context.total_coins++;

			} else if (!init.compare("key")) {
				reg.emplace<CompKeyTile>(e);

			} else if (!init.compare("warp")) {
				CompWarpTile &warptile = reg.emplace<CompWarpTile>(e);

				unsigned int target_id = object_tile_properties.get_object("warp_target");
				if (target_id && load_map_context->objects_by_id.contains(target_id)) {
					bool reversed = object_tile_properties.get_bool("warp_reverse");

					const TmxObject &target_object = *load_map_context->objects_by_id.at(target_id);
					if (target_object.type == TmxObject::POLYLINE) {
						warptile.points.reserve(target_object.polypoints.points.size());

						if (reversed) {
							do_warp_points(target_object.polypoints.points.crbegin(), target_object.polypoints.points.crend(),
									warptile.points, target_object.x, target_object.y);
						} else {
							do_warp_points(target_object.polypoints.points.cbegin(), target_object.polypoints.points.cend(),
									warptile.points, target_object.x, target_object.y);
						}
					} else {
						TransitPoint &point = warptile.points.emplace_back();
						point.x = target_object.x + target_object.width / 2.0f;
						point.y = target_object.y - target_object.height / 2.0f;
						point.end_time = 0.0f;
					}

					warptile.warp_to_spawn = false;
				} else {
					// warp to spawn if there's no target set
					warptile.warp_to_spawn = true;
				}

				warptile.on_touch = object_tile_properties.get_bool("warp_on_touch");
				warptile.speed_multiplier = object_tile_properties.get_float("warp_speed_multiplier");

				warptile.set_velocity = object_tile_properties.get_bool("warp_set_velocity");
				if (warptile.set_velocity) {
					warptile.vel_dx = object_tile_properties.get_float("warp_velocity_x");
					warptile.vel_dy = object_tile_properties.get_float("warp_velocity_y");
				}

				unsigned int key_id = object_tile_properties.get_object("warp_key");
				if (key_id && load_map_context->objects_by_id.contains(key_id)) {
					const TmxObject &target_object = *load_map_context->objects_by_id.at(key_id);

					if (target_object.type == TmxObject::RECTANGLE && target_object.has_gid) {
						warptile.key_gid = target_object.gid;

						warptile.unlock_tile_gid = load_map_context->get_firstgid(object.gid) + object_tile_properties.get_int("warp_unlock_tile_id");
					}
				}
			}

			continue;
		}

		if (object.type == TmxObject::POINT) {
			std::string init = object.properties.get_string("init");
			if (!init.compare("spawnpoint")) {
				g_game_context.spawnpoint_x = x;
				g_game_context.spawnpoint_y = y;
			}
		}
	}
	(*index)++;
}

void add_group_layer(const LoadMapContext *load_map_context, const TmxGroupLayer *grouplayer, float parent_offset_x, float parent_offset_y, unsigned int *index, entt::registry &reg) {
	parent_offset_x += grouplayer->offset_x;
	parent_offset_y += grouplayer->offset_y;
	for (const std::unique_ptr<TmxLayer> &layer : grouplayer->layers) {
		if (layer->type == TmxLayer::TILE_LAYER) {
			add_tile_layer(load_map_context, static_cast<TmxTileLayer *>(layer.get()), parent_offset_x, parent_offset_y, index, reg);
		} else if (layer->type == TmxLayer::OBJECT_LAYER) {
			add_object_layer(load_map_context, static_cast<TmxObjectLayer *>(layer.get()), parent_offset_x, parent_offset_y, index, reg);
		} else if (layer->type == TmxLayer::GROUP_LAYER) {
			add_group_layer(load_map_context, static_cast<TmxGroupLayer *>(layer.get()), parent_offset_x, parent_offset_y, index, reg);
		}
	}
}

void unload_level(entt::registry &reg) {
	entt::entity player = reg.view<CompPosition, CompPlayer>().front();
	if (player != entt::null) {
		CompPosition &pos = reg.get<CompPosition>(player);
		g_game_context.preserved_player_x = pos.x;
		g_game_context.preserved_player_y = pos.y;
		g_game_context.has_preserved_player_position = true;
	}

	reg.clear();

	for (ZrTexture *texture : g_game_context.loaded_textures) {
		gfx_free_texture(texture);
	}
	g_game_context.loaded_textures.clear();

	g_game_context.level_is_loaded = false;
}

bool load_level_body(const char *filename, entt::registry &reg) {
	unload_level(reg);

	bool is_same_level = !g_game_context.level_path.compare(filename);

	g_game_context.level_path = filename;
	config_mark_dirty();

	g_game_context.spawnpoint_x = 0.0f;
	g_game_context.spawnpoint_y = 0.0f;

	entt::entity demo_player = make_demo_player(reg);

	load_object_types();

	TmxMap tmxmap;
	bool success = parse_map(&tmxmap, filename);
	if (!success) return false;

	LoadMapContext load_map_context;
	load_map_context.tmxmap = &tmxmap;

	prepass_layers(&load_map_context, tmxmap.layers);

	g_game_context.total_coins = 0;
	g_game_context.coin_count = 0;

	g_game_context.map_background_color = tmxmap.background_color;

	for (const TmxTileset &tileset : tmxmap.tilesets) {
		ZrTexture *texture;
		if (tileset.image.empty()) {
			texture = nullptr;
		} else {
			texture = gfx_load_texture_from_file(tileset.image.c_str());
			g_game_context.loaded_textures.push_back(texture);
		}

		size_t needed_size = tileset.firstgid + tileset.tilecount;
		if (tiles_global.size() < needed_size) {
			tiles_global.resize(needed_size);
		}
		for (const TmxTile &tmxtile : tileset.tiles) {
			ZrTile zrtile;

			zrtile.texture = texture;
			zrtile.srcx = tmxtile.src_x;
			zrtile.srcy = tmxtile.src_y;
			zrtile.srcw = tmxtile.width;
			zrtile.srch = tmxtile.height;

			// animation
			zrtile.animation_duration = 0;
			for (TmxAnimationFrame tmxframe : tmxtile.animation) {
				ZrTile::AnimationFrame &zrframe = zrtile.animation_frames.emplace_back();
				zrtile.animation_duration += tmxframe.duration;
				zrframe.gid = tileset.firstgid + tmxframe.lid;
				zrframe.end_at = zrtile.animation_duration;
			}
			zrtile.has_animation = zrtile.animation_duration > 0;

			// static and body collision, absolute
			for (const TmxObject &object : tmxtile.collision.objects) {
				if (object.type == TmxObject::POLYLINE) {
					if (object.properties.has_string("static_collision_type")) {
						std::string static_collision_type = object.properties.get_string("static_collision_type");
						StaticCollisionPart::Type sc_type;

						if (!static_collision_type.compare("floor")) {
							sc_type = StaticCollisionPart::FLOOR;
						} else if (!static_collision_type.compare("leftwall")) {
							sc_type = StaticCollisionPart::WALL_LEFT;
						} else if (!static_collision_type.compare("rightwall")) {
							sc_type = StaticCollisionPart::WALL_RIGHT;
						} else if (!static_collision_type.compare("ceiling")) {
							sc_type = StaticCollisionPart::CEILING;
						} else {
							zr_log_warn("invalid polyline static_collision_type '%s'", static_collision_type.c_str());
							continue;
						}

						float last_x, last_y;
						int num_points = 0;
						for (const auto &[px, py] : object.polypoints.points) {
							float x = object.x + px;
							float y = object.y + py;
							if (num_points > 0) {
								StaticCollisionPart &statpart = zrtile.static_collision_absolute.emplace_back();
								statpart.type = sc_type;
								statpart.x1 = last_x;
								statpart.y1 = last_y;
								statpart.x2 = x;
								statpart.y2 = y;
							}
							num_points++;
							last_x = x;
							last_y = y;
						}
					}
				} else if (object.type == TmxObject::RECTANGLE) {
					if (object.properties.has_string("static_collision_type")) {
						std::string static_collision_type = object.properties.get_string("static_collision_type");
						if (!static_collision_type.compare("rectangle")) {
							float w = tmxtile.width;
							float h = tmxtile.height;
							zrtile.static_collision_absolute.push_back({ StaticCollisionPart::FLOOR, 0.0f, 0.0f, w, 0.0f });
							zrtile.static_collision_absolute.push_back({ StaticCollisionPart::WALL_LEFT, 0.0f, 0.0f, 0.0f, h });
							zrtile.static_collision_absolute.push_back({ StaticCollisionPart::WALL_RIGHT, w, 0.0f, w, h });
							zrtile.static_collision_absolute.push_back({ StaticCollisionPart::CEILING, 0.0f, h, w, h });
						} else {
							zr_log_warn("invalid rectangle static_collision_type '%s'", static_collision_type.c_str());
						}

					} else if (object.properties.has_string("body_collision_type")) {
						std::string body_collision_type = object.properties.get_string("body_collision_type");
						bool solid;
						if (!body_collision_type.compare("nonsolid")) {
							solid = false;
						} else if (!body_collision_type.compare("solid")) {
							solid = true;
						} else {
							zr_log_warn("invalid rectangle body_collision_type '%s'", body_collision_type.c_str());
							continue;
						}

						BodyCollisionPart &bodypart = zrtile.body_collision_absolute.emplace_back();
						bodypart.rect = { .x1 = object.x, .y1 = object.y, .x2 = object.x + object.width, .y2 = object.y + object.height };
						bodypart.solid = solid;
					}
				}
			}

			// static collision, normalized copy
			zrtile.static_collision_normalized.reserve(zrtile.static_collision_absolute.size());
			for (const StaticCollisionPart &statpart : zrtile.static_collision_absolute) {
				StaticCollisionPart &statpart_normalized = zrtile.static_collision_normalized.emplace_back(statpart);
				statpart_normalized.x1 /= (float)tmxtile.width;
				statpart_normalized.y1 /= (float)tmxtile.height;
				statpart_normalized.x2 /= (float)tmxtile.width;
				statpart_normalized.y2 /= (float)tmxtile.height;
			}
			// body collision, normalized copy
			zrtile.body_collision_normalized.reserve(zrtile.body_collision_absolute.size());
			for (const BodyCollisionPart &bodypart : zrtile.body_collision_absolute) {
				BodyCollisionPart &bodypart_normalized = zrtile.body_collision_normalized.emplace_back(bodypart);
				bodypart_normalized.rect.x1 /= (float)tmxtile.width;
				bodypart_normalized.rect.y1 /= (float)tmxtile.height;
				bodypart_normalized.rect.x2 /= (float)tmxtile.width;
				bodypart_normalized.rect.y2 /= (float)tmxtile.height;
			}

			tiles_global[tileset.firstgid + tmxtile.lid] = zrtile;
		}

		tiles_global_size = tiles_global.size();
		tiles_global_ptr = tiles_global.data();
	}

	unsigned int index = 0;
	for (const std::unique_ptr<TmxLayer> &layer : tmxmap.layers) {
		if (layer->type == TmxLayer::TILE_LAYER) {
			add_tile_layer(&load_map_context, static_cast<TmxTileLayer *>(layer.get()), 0.0f, 0.0f, &index, reg);
		} else if (layer->type == TmxLayer::OBJECT_LAYER) {
			add_object_layer(&load_map_context, static_cast<TmxObjectLayer *>(layer.get()), 0.0f, 0.0f, &index, reg);
		} else if (layer->type == TmxLayer::GROUP_LAYER) {
			add_group_layer(&load_map_context, static_cast<TmxGroupLayer *>(layer.get()), 0.0f, 0.0f, &index, reg);
		}
	}

	g_game_context.speedrun_mode = g_game_context.user_speedrun_mode;
	if (g_game_context.speedrun_mode) {
		g_game_context.speedrun_time = 0.0;
		g_game_context.speedrun_finished = false;
	}

	if (!g_game_context.speedrun_mode && is_same_level && g_game_context.preserve_position_on_level_load && g_game_context.has_preserved_player_position) {
		reg.replace<CompPosition>(demo_player, g_game_context.preserved_player_x, g_game_context.preserved_player_y);
	} else {
		reg.replace<CompPosition>(demo_player, g_game_context.spawnpoint_x, g_game_context.spawnpoint_y);
	}

	g_game_context.level_is_loaded = true;

	return true;
}

bool load_level(const char *filename, entt::registry &reg) {
	zr_log_cat_push("load_level");
	bool r = load_level_body(filename, reg);
	zr_log_cat_pop();
	return r;
}
