// gfx_tile.cc

#include "gfx_tile.hh"

#include <entt/entt.hpp>

#include "geom.hh"
#include "gfx.hh"
#include "map.hh"

void gfx_draw_tile(unsigned int gid, const NEWRECT dstrect) {
	if (gid == 0) return;

	const ZrTile *tile = get_tile(gid);

	if (tile->has_animation) {
		unsigned int dur = SDL_GetTicks() % tile->animation_duration;
		for (ZrTile::AnimationFrame frame : tile->animation_frames) {
			if (dur < frame.end_at) {
				tile = get_tile(frame.gid);
				break;
			}
		}
	}

	NEWRECT srcrect;
	srcrect.x1 = tile->srcx;
	srcrect.y1 = tile->srcy;
	srcrect.x2 = tile->srcx + tile->srcw;
	srcrect.y2 = tile->srcy + tile->srch;

	gfx_draw_texture(tile->texture, srcrect, dstrect);
}
