// comp.hh

#pragma once

#include <vector>

#include "collision_parts.hh"
#include "geom.hh"
#include "map.hh"

struct CompPosition {
	float x, y;
};
struct CompPhysics {
	float dx = 0.0f;
	float dy = 0.0f;
	bool is_on_ground = false;
	bool was_on_ground = false;
	float gravity_multiplier = 1.0f;
};
struct CompPlayer {
	struct Key;

	enum FacingH { RIGHT, LEFT };
	enum FacingV { NEUTRAL, UP, DOWN };
	float grace_jump_timer;
	float grace_bhop_timer;
	float grace_jump_base_height;
	FacingH hfacing;
	FacingV vfacing;
	float walking_time = 0.0f;
	float idle_time = 0.0f;
	float air_time = 0.0f;
	int input_index = 0;
	bool looking_back = false;
	bool leaving_door = false;
	bool can_dash = true;
	std::vector<unsigned int> keys_gids;
	bool input_left;
	bool input_right;
	bool input_jump;
	bool input_jump_ff;
	bool input_dash;
	bool input_dash_ff;
	bool input_aircontrol;
	bool input_up;
	bool input_down;
	bool input_down_ff;
	bool input_toggle_free_move;
	bool input_respawn;
};

struct CompDash {
	float dash_time = 0.0f;
	float vel_dx, vel_dy;
};

struct CompFreeMove {};

struct TransitPoint {
	float end_time;
	float x, y;
};
struct CompTransit {
	float time;
	std::vector<TransitPoint> points;
	float speed_multiplier;
	bool set_velocity;
	float vel_dx, vel_dy;
};

struct CompBodyCollision {
	std::vector<BodyCollisionPart> parts;
};
struct CompStaticCollision {
	std::vector<StaticCollisionPart> parts;
};

struct CompTileLayer {
	int width, height;
	int tile_width, tile_height;
	std::vector<unsigned int> tile_gids;
	TileLayerRenderOrder render_order;
};

struct CompTile {
	unsigned int gid;
	NEWRECT rect;
};

struct CompCoinTile {};
struct CompCoinCollector {};

struct CompKeyTile {};

struct CompOnLayer {
	unsigned int index; // index of the layer the entity is on
};
struct CompObjectLayerObject {
	ObjectLayerDrawOrder draworder;
	unsigned int index; // index within an object layer the entity is on
};
struct CompRender {};

struct CompWarpTile {
	std::vector<TransitPoint> points;
	bool on_touch;
	float speed_multiplier;
	bool set_velocity;
	float vel_dx, vel_dy;
	bool warp_to_spawn;
	unsigned int key_gid = 0;
	unsigned int unlock_tile_gid;
};
