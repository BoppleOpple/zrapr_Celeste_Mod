// geom.hh

#pragma once

#include <cmath>
#include <cstdint>

struct NEWRECT {
	float x1, y1, x2, y2;

	static inline NEWRECT of_points(float _x1, float _y1, float _x2, float _y2) {
		NEWRECT r;
		r.x1 = _x1;
		r.y1 = _y1;
		r.x2 = _x2;
		r.y2 = _y2;
		return r;
	}

	static inline NEWRECT xflip(NEWRECT r) {
		std::swap(r.x1, r.x2);
		return r;
	}
	static inline NEWRECT yflip(NEWRECT r) {
		std::swap(r.y1, r.y2);
		return r;
	}

	static inline float width(const NEWRECT r) {
		return fabsf(r.x1 - r.x2);
	}
	static inline float height(const NEWRECT r) {
		return fabsf(r.y1 - r.y2);
	}

	/* create a new rect, centered at (0, 0) of size (w, h) */
	static inline const NEWRECT centered_of_size(float w, float h) {
		NEWRECT r;
		r.x1 = w / -2.0f;
		r.y1 = h / -2.0f;
		r.x2 = w / 2.0f;
		r.y2 = h / 2.0f;
		return r;
	}

	static inline const NEWRECT offset(NEWRECT r, float x, float y) {
		r.x1 += x;
		r.y1 += y;
		r.x2 += x;
		r.y2 += y;
		return r;
	}
};

/*struct VEC2 {
	float x, y;

	// binary equals ==
	inline bool operator==(const VEC2 &rhs) const {
		return this->x == rhs.x && this->y == rhs.y;
	}
	// binary not equals !=
	inline bool operator!=(const VEC2 &rhs) const {
		return this->x != rhs.x || this->y != rhs.y;
	}
	// binary plus equals +=
	inline VEC2& operator+=(const VEC2& rhs) {
		this->x += rhs.x;
		this->y += rhs.y;
		return *this;
	}
	// binary plus +
	friend VEC2 operator+(const VEC2 &lhs, const VEC2 &rhs) {
		VEC2 v = lhs;
		v += rhs;
		return v;
	}
	// binary minus equals -=
	inline VEC2& operator-=(const VEC2& rhs) {
		this->x -= rhs.x;
		this->y -= rhs.y;
		return *this;
	}
	// binary minus -
	friend VEC2 operator-(const VEC2 &lhs, const VEC2 &rhs) {
		VEC2 v = lhs;
		v -= rhs;
		return v;
	}
	// unary minus -
	friend VEC2 operator-(const VEC2 &rhs) {
		VEC2 v;
		v.x = -rhs.x;
		v.y = -rhs.y;
		return v;
	}
};*/
