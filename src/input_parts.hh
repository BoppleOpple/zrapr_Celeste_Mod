// input_parts.hh

#pragma once

#include <vector>

struct InputSpecEntry {
	enum Type {
		UNBOUND,
		SCANCODE,
		GAME_CONTROLLER_BUTTON,
		GAME_CONTROLLER_AXIS_POSITIVE,
		GAME_CONTROLLER_AXIS_NEGATIVE
	};

	Type type = UNBOUND;
	union {
		unsigned int code;
		SDL_Scancode scancode;
		SDL_GameControllerButton button;
		SDL_GameControllerAxis axis;
	};
	bool waiting_for_bind = false;
};

struct InputSpec {
	std::vector<InputSpecEntry> entries;
};

struct InputProfile {
	InputSpec left;
	InputSpec right;
	InputSpec up;
	InputSpec down;
	InputSpec jump;
	InputSpec dash;
	InputSpec aircontrol;
	InputSpec freemove;
	InputSpec respawn;
};

struct InputControl {
	InputSpec frame_advance;
	InputSpec pause;
	InputSpec reload_level;
	InputSpec reload_playerchar;
	InputSpec zoom_in;
	InputSpec zoom_out;
	InputSpec toggle_collision_view;
	std::vector<InputProfile> profiles;
};
