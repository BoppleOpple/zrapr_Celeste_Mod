// context.hh

#pragma once

#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <entt/entt.hpp>
#include <imgui/imgui.h>
#include <SDL.h>

#include "gfx.hh"
#include "input_parts.hh"
#include "log.hh"
#include "playerchar.hh"

struct CollisionViewControl {
	bool show_static_floor;
	bool show_static_wall_left;
	bool show_static_wall_right;
	bool show_static_ceiling;
	bool show_body_main;
	bool show_body_floor_snap_up;
	bool show_body_floor_snap_down;
	bool show_body_wall_snap_left;
	bool show_body_wall_snap_right;
	bool show_body_ceiling_snap_down;
	bool setting_extra_body_collision;
};

struct CollisionViewColors {
	SDL_Color static_floor;
	SDL_Color static_wall_left;
	SDL_Color static_wall_right;
	SDL_Color static_ceiling;
	SDL_Color body_main;
	SDL_Color body_main_bg;
	SDL_Color body_floor_snap_up;
	SDL_Color body_floor_snap_up_bg;
	SDL_Color body_floor_snap_down;
	SDL_Color body_floor_snap_down_bg;
	SDL_Color body_wall_snap_left;
	SDL_Color body_wall_snap_left_bg;
	SDL_Color body_wall_snap_right;
	SDL_Color body_wall_snap_right_bg;
	SDL_Color body_ceiling_snap_down;
	SDL_Color body_ceiling_snap_down_bg;
};

struct ClientContext {
	SDL_Window *window;
	int window_width;
	int window_height;
	bool window_fullscreen;
	bool window_maximized;
	bool window_vsync;
	ImGuiContext *imgui_context;
	ImGuiIO *imgui_io;
	float frame_duration_seconds;
	bool show_imgui_demo;
	bool show_ecs_view;
	bool show_collision_view_control;
	bool show_input_control;
	bool caption_entity_ids;
	CollisionViewControl collision_view_control;
	CollisionViewColors collision_view_colors;
	InputControl input_control;
	float view_x;
	float view_y;
	float view_scale;
	bool show_level_select;
	std::vector<ZrLogEntry> log_entries;
	unsigned int log_max_entries;
	bool show_log;
	bool log_to_stdout;
	bool show_playerchar_select;
	unsigned int hide_cursor_time;
	entt::entity camera_entity;
	ZrTexture *folders_tex;
	std::set<uint32_t> playerchar_select_expanded_folders;
};

struct GameContext {
	float target_frames_per_second;
	float time_factor;
	float time_factor_multiplier;
	int coin_count;
	int total_coins;
	bool is_game_frame;
	bool paused;
	std::string level_path;
	std::vector<ZrTexture *> loaded_textures;
	std::string playerchar_path;
	std::vector<std::string> playerchar_search_paths;
	std::map<std::string, ZrTexture *> playerchar_select_previews;
	PlayerChar playerchar;
	ZrTexture *playerchar_texture;
	SDL_Color map_background_color;
	float spawnpoint_x;
	float spawnpoint_y;
	bool user_speedrun_mode;
	bool speedrun_mode;
	double speedrun_time;
	bool speedrun_finished;
	bool preserve_position_on_level_load;
	bool level_is_loaded;
	bool has_preserved_player_position;
	float preserved_player_x;
	float preserved_player_y;
};

extern ClientContext g_client_context;
extern GameContext g_game_context;
