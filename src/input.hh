// input.hh

#pragma once

#include <SDL_gamecontroller.h>
#include <SDL_scancode.h>

#include <string_view>
#include <string>
#include <unordered_map>
#include <vector>

#include "context.hh"
#include "input_parts.hh"

void input_init();
void input_uninit();
void input_controller_open(int joystick_device_index);
void input_controller_close(int joystick_instance_id);

const std::unordered_map<SDL_JoystickID, SDL_GameController *> &input_get_controllers();
int input_get_device_index(int joystick_instance_id);

const std::unordered_map<std::string, InputSpec *> &input_get_specs();
const std::vector<std::pair<std::string, InputSpec *>> &input_get_specs_ordered();
const std::unordered_map<std::string, InputSpec InputProfile::*> &input_get_specs_profiled();
const std::vector<std::pair<std::string, InputSpec InputProfile::*>> &input_get_specs_profiled_ordered();

void input_event_key_down(SDL_Scancode scancode);
void input_event_key_up(SDL_Scancode scancode);
void input_event_button_down(unsigned int joystick_instance_id, SDL_GameControllerButton button);
void input_event_button_up(unsigned int joystick_instance_id, SDL_GameControllerButton button);
void input_event_axis_update(unsigned int joystick_instance_id, SDL_GameControllerAxis axis, Sint16 v);
void input_event_frame_end();

void input_set_waiting_for_bind(InputSpecEntry &entry);

/*
- game_input_* functions update only game frames, i.e., they freeze when paused
- global_input_* functions update every frame
*/

bool global_input_is_pressed(const InputSpec &spec);
bool global_input_is_held(const InputSpec &spec);
bool global_input_is_released(const InputSpec &spec);
bool game_input_is_pressed(const InputSpec &spec);
bool game_input_is_held(const InputSpec &spec);
bool game_input_is_released(const InputSpec &spec);

float input_get_analog(const InputSpec &spec);

std::string inputspecentry_to_string(const InputSpecEntry &spec);
bool inputspecentry_from_string(const std::string_view strview, InputSpecEntry &entry);

std::string inputspec_to_string(const InputSpec &spec);
InputSpec inputspec_from_string(const char *str);
