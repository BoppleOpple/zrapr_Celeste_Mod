// configfile.cc

#include "configfile.hh"

#include <cstdarg>
#include <cstdlib>
#include <sstream>
#include <string>

#include <inih/ini.h>
#include <physfs.h>
#include <SDL.h>

#include "anno.hh"
#include "context.hh"
#include "fs.hh"
#include "input.hh"
#include "log.hh"
#include "util/sleep.hh"

#define GAME_CONFIG_FILE "game.cfg"

bool config_is_dirty = false;
unsigned int config_dirty_time;

static inline bool str2bool(const char *str) { return !strcasecmp(str, "true"); }
static inline const char *bool2str(const bool b) { return b ? "true" : "false"; }

unsigned int sdl_color_hex_format(SDL_Color color) {
	return (color.r << 24) | (color.g << 16) | (color.b << 8) | color.a;
}

SDL_Color sdl_color_hex_parse(const char *str) {
	unsigned int hex = strtoul(str, nullptr, 16);
	SDL_Color color;
	color.r = (hex >> 24) & 0xff;
	color.g = (hex >> 16) & 0xff;
	color.b = (hex >>  8) & 0xff;
	color.a = (hex      ) & 0xff;
	return color;
}

void write_line(PHYSFS_File *handle, PRINTF_FORMAT_STRING const char *fmt, ...) PRINTF_VARARG_FN(2);

void write_line(PHYSFS_File *handle, const char *fmt, ...) {
	static char buf[256];
	va_list v;
	va_start(v, fmt);
	int written = vsnprintf(buf, sizeof buf, fmt, v);
	PHYSFS_writeBytes(handle, buf, written);
	va_end(v);
}

void save_config_to_disk() {
	PHYSFS_File *handle = PHYSFS_openWrite(GAME_CONFIG_FILE);
	if (handle == nullptr) {
		zr_log_error("failed to open config file '%s' for writing", GAME_CONFIG_FILE);
		return;
	}

	write_line(handle, "# " GAME_CONFIG_FILE "\n");
	write_line(handle, "# this file will be overwritten without mercy\n");

	write_line(handle, "\n[game]\n");
	write_line(handle, "window_width=%d\n",             g_client_context.window_width                );
	write_line(handle, "window_height=%d\n",            g_client_context.window_height               );
	write_line(handle, "window_fullscreen=%s\n",        bool2str(g_client_context.window_fullscreen) );
	write_line(handle, "window_maximized=%s\n",         bool2str(g_client_context.window_maximized)  );
	write_line(handle, "window_vsync=%s\n",             bool2str(g_client_context.window_vsync)      );
	write_line(handle, "target_frames_per_second=%f\n", g_game_context.target_frames_per_second      );
	write_line(handle, "sleep_method=%s\n",             sleepmethod_to_string(g_sleep_method)        );
	write_line(handle, "sleep_correction=%s\n",         bool2str(g_sleep_correction)                 );
	write_line(handle, "sleep_threshold=%f\n",          g_sleep_threshold                            );
	write_line(handle, "level=%s\n",                    g_game_context.level_path.c_str()            );
	write_line(handle, "playerchar=%s\n",               g_game_context.playerchar_path.c_str()       );
	write_line(handle, "log_to_stdout=%s\n",            bool2str(g_client_context.log_to_stdout)     );
	write_line(handle, "view_scale=%f\n",               g_client_context.view_scale                  );
	write_line(handle, "speedrun_mode=%s\n",            bool2str(g_game_context.user_speedrun_mode)  );
	write_line(handle, "preserve_position_on_level_load=%s\n", bool2str(g_game_context.preserve_position_on_level_load));
	write_line(handle, "caption_entity_ids=%s\n",       bool2str(g_client_context.caption_entity_ids ));

	if (!g_game_context.playerchar_search_paths.empty()) {
		std::string base_paths_str;
		base_paths_str.reserve(256);
		for (const std::string &str : g_game_context.playerchar_search_paths) {
			base_paths_str += ':';
			base_paths_str += str;
		}
		write_line(handle, "playerchar_search_paths=%s\n", base_paths_str.c_str() + 1);
	}

	if (!g_client_context.playerchar_select_expanded_folders.empty()) {
		std::string folders_str;
		folders_str.reserve(256);
		for (uint32_t hash : g_client_context.playerchar_select_expanded_folders) {
			char buf[16];
			snprintf(buf, sizeof buf, ":%08x", hash);
			folders_str += buf;
		}
		write_line(handle, "playerchar_select_expanded_folders=%s\n", folders_str.c_str() + 1);
	}

	write_line(handle, "\n[tools]\n");
	write_line(handle, "show_imgui_demo=%s\n",             bool2str(g_client_context.show_imgui_demo)             );
	write_line(handle, "show_ecs_view=%s\n",               bool2str(g_client_context.show_ecs_view)               );
	write_line(handle, "show_collision_view_control=%s\n", bool2str(g_client_context.show_collision_view_control) );
	write_line(handle, "show_input_control=%s\n",          bool2str(g_client_context.show_input_control)          );
	write_line(handle, "show_level_select=%s\n",           bool2str(g_client_context.show_level_select)           );
	write_line(handle, "show_playerchar_select=%s\n",      bool2str(g_client_context.show_playerchar_select)      );
	write_line(handle, "show_log=%s\n",                    bool2str(g_client_context.show_log)                    );

	write_line(handle, "\n[collision_view_control]\n");
	write_line(handle, "show_static_floor=%s\n",           bool2str(g_client_context.collision_view_control.show_static_floor)           );
	write_line(handle, "show_static_wall_left=%s\n",       bool2str(g_client_context.collision_view_control.show_static_wall_left)       );
	write_line(handle, "show_static_wall_right=%s\n",      bool2str(g_client_context.collision_view_control.show_static_wall_right)      );
	write_line(handle, "show_static_ceiling=%s\n",         bool2str(g_client_context.collision_view_control.show_static_ceiling)         );
	write_line(handle, "show_body_main=%s\n",              bool2str(g_client_context.collision_view_control.show_body_main)              );
	write_line(handle, "show_body_floor_snap_up=%s\n",     bool2str(g_client_context.collision_view_control.show_body_floor_snap_up)     );
	write_line(handle, "show_body_floor_snap_down=%s\n",   bool2str(g_client_context.collision_view_control.show_body_floor_snap_down)   );
	write_line(handle, "show_body_wall_snap_left=%s\n",    bool2str(g_client_context.collision_view_control.show_body_wall_snap_left)    );
	write_line(handle, "show_body_wall_snap_right=%s\n",   bool2str(g_client_context.collision_view_control.show_body_wall_snap_right)   );
	write_line(handle, "show_body_ceiling_snap_down=%s\n", bool2str(g_client_context.collision_view_control.show_body_ceiling_snap_down) );
	write_line(handle, "setting_extra_body_collision=%s\n", bool2str(g_client_context.collision_view_control.setting_extra_body_collision) );

	write_line(handle, "\n[collision_view_colors]\n");
	write_line(handle, "static_floor=%08x\n",              sdl_color_hex_format(g_client_context.collision_view_colors.static_floor)              );
	write_line(handle, "static_wall_left=%08x\n",          sdl_color_hex_format(g_client_context.collision_view_colors.static_wall_left)          );
	write_line(handle, "static_wall_right=%08x\n",         sdl_color_hex_format(g_client_context.collision_view_colors.static_wall_right)         );
	write_line(handle, "static_ceiling=%08x\n",            sdl_color_hex_format(g_client_context.collision_view_colors.static_ceiling)            );
	write_line(handle, "body_main=%08x\n",                 sdl_color_hex_format(g_client_context.collision_view_colors.body_main)                 );
	write_line(handle, "body_main_bg=%08x\n",              sdl_color_hex_format(g_client_context.collision_view_colors.body_main_bg)              );
	write_line(handle, "body_floor_snap_up=%08x\n",        sdl_color_hex_format(g_client_context.collision_view_colors.body_floor_snap_up)        );
	write_line(handle, "body_floor_snap_up_bg=%08x\n",     sdl_color_hex_format(g_client_context.collision_view_colors.body_floor_snap_up_bg)     );
	write_line(handle, "body_floor_snap_down=%08x\n",      sdl_color_hex_format(g_client_context.collision_view_colors.body_floor_snap_down)      );
	write_line(handle, "body_floor_snap_down_bg=%08x\n",   sdl_color_hex_format(g_client_context.collision_view_colors.body_floor_snap_down_bg)   );
	write_line(handle, "body_wall_snap_left=%08x\n",       sdl_color_hex_format(g_client_context.collision_view_colors.body_wall_snap_left)       );
	write_line(handle, "body_wall_snap_left_bg=%08x\n",    sdl_color_hex_format(g_client_context.collision_view_colors.body_wall_snap_left_bg)    );
	write_line(handle, "body_wall_snap_right=%08x\n",      sdl_color_hex_format(g_client_context.collision_view_colors.body_wall_snap_right)      );
	write_line(handle, "body_wall_snap_right_bg=%08x\n",   sdl_color_hex_format(g_client_context.collision_view_colors.body_wall_snap_right_bg)   );
	write_line(handle, "body_ceiling_snap_down=%08x\n",    sdl_color_hex_format(g_client_context.collision_view_colors.body_ceiling_snap_down)    );
	write_line(handle, "body_ceiling_snap_down_bg=%08x\n", sdl_color_hex_format(g_client_context.collision_view_colors.body_ceiling_snap_down_bg) );

	write_line(handle, "\n[input_control]\n");
	const InputProfile &profile = g_client_context.input_control.profiles[0];
	for (const auto &[name, spec] : input_get_specs_profiled_ordered()) {
		std::string s = inputspec_to_string(&profile->*spec);
		write_line(handle, "%s=%s\n", name.c_str(), s.c_str());
	}
	for (const auto &[name, spec] : input_get_specs_ordered()) {
		std::string s = inputspec_to_string(*spec);
		write_line(handle, "%s=%s\n", name.c_str(), s.c_str());
	}

	PHYSFS_close(handle);

	config_is_dirty = false;
}

int config_loader([[maybe_unused]] void *user, const char *section, const char *key, const char *value) {
	if (!strcmp(section, "game")) {
		if(0);
		else if (!strcmp(key, "window_width"))             g_client_context.window_width = atoi(value);
		else if (!strcmp(key, "window_height"))            g_client_context.window_height = atoi(value);
		else if (!strcmp(key, "window_fullscreen"))        g_client_context.window_fullscreen = str2bool(value);
		else if (!strcmp(key, "window_maximized"))         g_client_context.window_maximized = str2bool(value);
		else if (!strcmp(key, "window_vsync"))             g_client_context.window_vsync = str2bool(value);
		else if (!strcmp(key, "target_frames_per_second")) g_game_context.target_frames_per_second = strtof(value, nullptr);
		else if (!strcmp(key, "sleep_method"))             sleepmethod_from_string(value, &g_sleep_method);
		else if (!strcmp(key, "sleep_correction"))         g_sleep_correction = str2bool(value);
		else if (!strcmp(key, "sleep_threshold"))          g_sleep_threshold = strtof(value, nullptr);
		else if (!strcmp(key, "level"))                    g_game_context.level_path = value;
		else if (!strcmp(key, "playerchar"))               g_game_context.playerchar_path = value;
		else if (!strcmp(key, "log_to_stdout"))            g_client_context.log_to_stdout = str2bool(value);
		else if (!strcmp(key, "view_scale"))               g_client_context.view_scale = atoi(value);
		else if (!strcmp(key, "speedrun_mode"))            g_game_context.user_speedrun_mode = str2bool(value);
		else if (!strcmp(key, "preserve_position_on_level_load")) g_game_context.preserve_position_on_level_load = str2bool(value);
		else if (!strcmp(key, "caption_entity_ids"))       g_client_context.caption_entity_ids = str2bool(value);
		else if (!strcmp(key, "playerchar_search_paths")) {
			g_game_context.playerchar_search_paths.clear();
			std::istringstream iss(value);
			std::string token;
			while (std::getline(iss, token, ':')) {
				if (!token.empty()) {
					g_game_context.playerchar_search_paths.emplace_back(token);
				}
			}
		}
		else if (!strcmp(key, "playerchar_select_expanded_folders")) {
			g_client_context.playerchar_select_expanded_folders.clear();
			std::istringstream iss(value);
			std::string token;
			while (std::getline(iss, token, ':')) {
				if (!token.empty()) {
					uint32_t hash = strtoul(token.c_str(), nullptr, 16);
					g_client_context.playerchar_select_expanded_folders.emplace(hash);
				}
			}
		}
	} else if (!strcmp(section, "tools")) {
		if(0);
		else if (!strcmp(key, "show_imgui_demo"))             g_client_context.show_imgui_demo             = str2bool(value);
		else if (!strcmp(key, "show_ecs_view"))               g_client_context.show_ecs_view               = str2bool(value);
		else if (!strcmp(key, "show_collision_view_control")) g_client_context.show_collision_view_control = str2bool(value);
		else if (!strcmp(key, "show_input_control"))          g_client_context.show_input_control          = str2bool(value);
		else if (!strcmp(key, "show_level_select"))           g_client_context.show_level_select           = str2bool(value);
		else if (!strcmp(key, "show_playerchar_select"))      g_client_context.show_playerchar_select      = str2bool(value);
		else if (!strcmp(key, "show_log"))                    g_client_context.show_log                    = str2bool(value);
	} else if (!strcmp(section, "collision_view_control")) {
		if(0);
		else if (!strcmp(key, "show_static_floor"))           g_client_context.collision_view_control.show_static_floor           = str2bool(value);
		else if (!strcmp(key, "show_static_wall_left"))       g_client_context.collision_view_control.show_static_wall_left       = str2bool(value);
		else if (!strcmp(key, "show_static_wall_right"))      g_client_context.collision_view_control.show_static_wall_right      = str2bool(value);
		else if (!strcmp(key, "show_static_ceiling"))         g_client_context.collision_view_control.show_static_ceiling         = str2bool(value);
		else if (!strcmp(key, "show_body_main"))              g_client_context.collision_view_control.show_body_main              = str2bool(value);
		else if (!strcmp(key, "show_body_floor_snap_up"))     g_client_context.collision_view_control.show_body_floor_snap_up     = str2bool(value);
		else if (!strcmp(key, "show_body_floor_snap_down"))   g_client_context.collision_view_control.show_body_floor_snap_down   = str2bool(value);
		else if (!strcmp(key, "show_body_wall_snap_left"))    g_client_context.collision_view_control.show_body_wall_snap_left    = str2bool(value);
		else if (!strcmp(key, "show_body_wall_snap_right"))   g_client_context.collision_view_control.show_body_wall_snap_right   = str2bool(value);
		else if (!strcmp(key, "show_body_ceiling_snap_down")) g_client_context.collision_view_control.show_body_ceiling_snap_down = str2bool(value);
		else if (!strcmp(key, "setting_extra_body_collision")) g_client_context.collision_view_control.setting_extra_body_collision = str2bool(value);
	} else if (!strcmp(section, "collision_view_colors")) {
		if(0);
		else if (!strcmp(key, "static_floor"))              g_client_context.collision_view_colors.static_floor              = sdl_color_hex_parse(value);
		else if (!strcmp(key, "static_wall_left"))          g_client_context.collision_view_colors.static_wall_left          = sdl_color_hex_parse(value);
		else if (!strcmp(key, "static_wall_right"))         g_client_context.collision_view_colors.static_wall_right         = sdl_color_hex_parse(value);
		else if (!strcmp(key, "static_ceiling"))            g_client_context.collision_view_colors.static_ceiling            = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_main"))                 g_client_context.collision_view_colors.body_main                 = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_main_bg"))              g_client_context.collision_view_colors.body_main_bg              = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_floor_snap_up"))        g_client_context.collision_view_colors.body_floor_snap_up        = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_floor_snap_up_bg"))     g_client_context.collision_view_colors.body_floor_snap_up_bg     = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_floor_snap_down"))      g_client_context.collision_view_colors.body_floor_snap_down      = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_floor_snap_down_bg"))   g_client_context.collision_view_colors.body_floor_snap_down_bg   = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_wall_snap_left"))       g_client_context.collision_view_colors.body_wall_snap_left       = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_wall_snap_left_bg"))    g_client_context.collision_view_colors.body_wall_snap_left_bg    = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_wall_snap_right"))      g_client_context.collision_view_colors.body_wall_snap_right      = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_wall_snap_right_bg"))   g_client_context.collision_view_colors.body_wall_snap_right_bg   = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_ceiling_snap_down"))    g_client_context.collision_view_colors.body_ceiling_snap_down    = sdl_color_hex_parse(value);
		else if (!strcmp(key, "body_ceiling_snap_down_bg")) g_client_context.collision_view_colors.body_ceiling_snap_down_bg = sdl_color_hex_parse(value);
	} else if (!strcmp(section, "input_control")) {
		std::unordered_map<std::string, InputSpec InputProfile::*> specs_profiled = input_get_specs_profiled();
		std::unordered_map<std::string, InputSpec *> specs = input_get_specs();

		InputProfile &profile = g_client_context.input_control.profiles[0];
		if (specs_profiled.find(key) != specs_profiled.end()) {
			profile.*specs_profiled[key] = inputspec_from_string(value);
		} else if (specs.find(key) != specs.end()) {
			*specs[key] = inputspec_from_string(value);
		}
	}
	return 1;
}

void load_config_from_disk() {
	fs_load_ini(GAME_CONFIG_FILE, (ini_handler)config_loader);
	g_game_context.speedrun_mode = g_game_context.user_speedrun_mode;
	save_config_to_disk();
}

void config_mark_dirty() {
	config_is_dirty = true;
	config_dirty_time = SDL_GetTicks();
}

void config_tick() {
	if (config_is_dirty && SDL_GetTicks() - config_dirty_time >= 4000) {
		config_is_dirty = false;
		save_config_to_disk();
	}
}
