// entities.hh

#pragma once

#include <entt/entt.hpp>

#include "collision_parts.hh"

void entity_add_body_collision_part(entt::registry &reg, entt::entity e, BodyCollisionPart part);
void entity_add_static_collision_part(entt::registry &reg, entt::entity e, StaticCollisionPart part);

entt::entity make_demo_player(entt::registry &reg);

void entity_update_tile_layer_static_collision(entt::registry &reg, entt::entity e);
void entity_update_tile_static_collision(entt::registry &reg, entt::entity e);
void entity_update_tile_body_collision(entt::registry &reg, entt::entity e);
